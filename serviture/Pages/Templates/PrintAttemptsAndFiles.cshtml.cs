using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Serviture.Models;

namespace Serviture.Pages.Templates
{
    public class PrintAttemptsAndFilesModel : PageModel
    {
        [BindProperty]
        public List<AttemptMapped> Attempts { get; set; }

        [BindProperty]
        public List<FileUpload> Files { get; set; }

        public PrintAttemptsAndFilesModel(AttemptReport attemptReport)
        {
            Attempts = attemptReport.Attempts;
            Files = attemptReport.Files;
        }
    }
}
