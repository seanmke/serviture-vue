﻿namespace Serviture
{
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Serviture.Models;

    public class AffidavitTemplateModel : PageModel
    {
        [BindProperty]
        public AffidavitMapped Affidavit { get; set; }

        [BindProperty]
        public string EntityServedLabel { get; set; }

        [BindProperty]
        public string CareOfLabel { get; set; }

        [BindProperty]
        public string AddressServedLabel { get; set; }

        [BindProperty]
        public string AddressesString { get; set; }

        [BindProperty]
        public string DocumentsString { get; set; }

        [BindProperty]
        public string ReasonsString { get; set; }

        [BindProperty]
        public string AffidavitTitle { get; set; }

        [BindProperty]
        public bool ShowAttempts { get; set; }

        [BindProperty]
        public AttemptMapped SuccessfulAttempt { get; set; }

        [BindProperty]
        public bool IsPrintVersion { get; set; }

        [BindProperty]
        public string ProcessServerName { get; set; }

        public AffidavitTemplateModel(AffidavitMapped affidavitMapped)
        {
            Affidavit = affidavitMapped;

            AddressServedLabel = "Address Where Served:";

            switch (Affidavit.AffidavitType)
            {
                case AffidavitType.Personal:
                    AffidavitTitle = "Affidavit of Personal Service";
                    EntityServedLabel = "Person Served:";
                    break;
                case AffidavitType.Substitute:
                    AffidavitTitle = "Affidavit of Substitute Service";
                    EntityServedLabel = "Person to be Served:";
                    break;
                case AffidavitType.Company:
                    AffidavitTitle = "Affidavit of Personal Service on Corporation or Limited Liability Company";
                    EntityServedLabel = "Corporation or LLC  Served:";
                    break;
                case AffidavitType.Reasonable:
                    AffidavitTitle = "Affidavit of Reasonable Diligence";
                    EntityServedLabel = "Person/Business to be Served:";
                    AddressServedLabel = "Address(es) Where Service Attempted:";
                    break;
                case AffidavitType.Mailing:
                    AffidavitTitle = "Affidavit of Posting/Mailing";
                    EntityServedLabel = "Person/Business to be Served:";
                    AddressServedLabel = "Address(es) Where Service Attempted:";
                    break;
            }

            switch (Affidavit.Relationship?.Value)
            {
                case "Officer":
                    CareOfLabel = "Served officer, director, a managing agent or registered agent name:";
                    break;
                case "Person":
                    CareOfLabel = "Served person apparently in charge of office or officer, director, or managing agent name:";
                    break;
                default:
                    CareOfLabel = "Substitute Service Upon:";
                    break;
            }

            AddressesString = string.Empty;

            if(Affidavit.Addresses != null)
            {
                foreach (var address in Affidavit.Addresses)
                {
                    var comma = Affidavit.Addresses.IndexOf(address) == Affidavit.Addresses.Count - 1 ? string.Empty : "; ";
                    AddressesString += $"{address.StreetAddress}{comma}";
                }
            }            

            if(Affidavit.Attempts != null)
            {
                SuccessfulAttempt = Affidavit.Attempts.Where(x => x.IsSuccess).FirstOrDefault() ?? new AttemptMapped();
            } 
            else
            {
                SuccessfulAttempt = new AttemptMapped();
            }

            ShowAttempts = Affidavit.AffidavitType == AffidavitType.Reasonable ||
                Affidavit.AffidavitType == AffidavitType.Mailing ||
                Affidavit.AffidavitType == AffidavitType.Substitute ||
                Affidavit.Client.ShowUnsuccessful.Value;

            if (Affidavit.Documents != null)
            {
                foreach (var doc in Affidavit.Documents)
                {
                    var comma = Affidavit.Documents.IndexOf(doc) == Affidavit.Documents.Count - 1 ? string.Empty : ", ";
                    DocumentsString += $"{doc.DocumentString}{comma}";
                }
            }

            if (Affidavit.Reasons != null)
            {
                foreach (var reason in Affidavit.Reasons)
                {
                    var comma = Affidavit.Reasons.IndexOf(reason) == Affidavit.Reasons.Count - 1 ? string.Empty : ", ";
                    ReasonsString += $"{reason.ReasonText}{comma}";
                }
            }

            if (Affidavit.Server.UserType == (int)UserType.Contractor)
            {
                ProcessServerName = Affidavit.ContractorUser == null ? string.Empty : Affidavit.ContractorUser.Name;
            } else
            {
                ProcessServerName = $"{Affidavit.Server.FirstName} {Affidavit.Server.LastName}"; 
            }
        }
    }
}