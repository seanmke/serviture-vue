use pre-serviture

SET IDENTITY_INSERT dbo.reason ON 

INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (15,'Address provided by Attorney/Plaintiff',0,0);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (16,'Accurint',0,1);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (17,'Argali',0,2);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (18,'CCAP',0,3);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (19,'CPE',0,4);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (20,'Debtor confirmed',0,5);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (21,'No Mail Return',0,6);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (22,'Post Office',0,7);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (23,'Tax Assessor',0,8);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (24,'Third Party call',0,9);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (25,'Third Party letter',0,10);
INSERT INTO reason(Id,Value,IsDeleted,SortOrder) VALUES (26,'DFI',0,11);

SET IDENTITY_INSERT dbo.reason OFF