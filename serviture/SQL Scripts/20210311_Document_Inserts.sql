SET IDENTITY_INSERT dbo.document ON

INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (66,'Authenticated Copy of Summons and Complaint- Small Claims Claim for Money, Electronic Filing Notice',0,0,12,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (67,'Authenticated Copy of Summons and Complaint, Metro Milwaukee Foreclosure Mediation Program, Electronic Filing Notice',0,0,1,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (68,'Authenticated Copy of Summons and Complaint',0,0,10,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (69,'Foreclosure Mediation Program',1,1,3,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (70,'Summons and Complaint, Important Notice',0,0,7,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (71,'',0,1,6,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (72,'Authenticated Copy of Summons and Complaint, Electronic Filing Notice',0,0,2,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (73,'Authenticated Copy of Amended Summons and Complaint, Electronic Filing Notice',0,0,5,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (74,'Plaintiff''s First Set of Written Interrogatories and Request for Production of Documents to Defendant',0,1,11,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (75,'Authenticated Copy of Summons and Complaint- Small Claims Eviction, Electronic Filing Notice',0,0,13,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (76,'Authenticated Copy of Summons and Complaint, Winnebago County Foreclosure Mediation Program, Electronic Filing Notice',0,0,14,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (77,'Authenticated Copy of Summons and Complaint, Rock County Foreclosure Mediation Program, Electronic Filing Notice',0,0,15,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (78,'Authenticated Copy of Summons and Complaint, Sheboygan County Foreclosure Mediation Program, Electronic Filing Notice',0,0,16,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (79,'Authenticated Copy of Summons and Complaint, Electronic Filing Notice',1,0,14,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (80,'Order to Raze and Remove Building, Important Notice',0,0,9,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (81,'Foreclosure Mediation Program',0,1,3,1);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (82,'Order and Notice of Hearing, Petition for Protective Placement, Petition for Permanent Guardianship Due to Incompetency, Examining Physician''s or Psychologist''s Report',0,0,17,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (83,'Earnings Garnishment Notice, Earnings Garnishment, Earnings Garnishment-Exemption Notice, Earnings Garnishment-Debtor''s Answer, Check for Garnishment',0,0,18,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (84,'Authenticated Copy of Summons and Complaint, Electronic Filing Notice, Ashland County Foreclosure Mediation Program',0,0,19,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (85,'Authenticated Copy of Amended Summons and Complaint, Electronic Filing Notice, Ashland County Foreclosure Mediation Program',0,0,20,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (86,'Authenticated Copy of Summons and Complaint- Small Claims Claim for Money and Eviction, Electronic Filing Notice',0,0,21,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (87,'Authenticated Copy of Summons and Complaint- Small Claims Claim for Money and Eviction, Electronic Filing Notice',0,0,22,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (88,'Order to Appear Before Court Commissioner, Affidavit',0,0,23,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (89,'Authenticated copy of Small Claims Non-Earnings Garnishment Summons and Complaint, Small Claims Garnishment Complaint for Non-Earnings',0,0,24,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (90,'Authenticated Copy of Non-Earnings Garnishment Summons and Complaint, Non-Earnings Garnishment Garnishee Answer, Garnishment Fee Check',0,0,25,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (91,'Authenticated Copy of Non-Earnings Garnishment Summons and Complaint, Creditor''s Claim',0,0,27,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (92,'Creditor''s Notice of Motion and Motion for Contempt, Affidavit',0,0,28,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (93,'Authenticated Copy of Small Claims Non-Earnings Garnishment Summons and Complaint, Creditor''s Claim, Non-Earnings Garnishment Garnishee Answer, Garnishment Fee Check',0,0,26,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (94,'Authenticated Copy of Small Claims Non-Earnings Garnishment Summons and Complaint, Creditor''s Claim',0,0,28,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (95,'Milwaukee Municipal Citation',0,0,8,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (96,'Authenticated Copy of Notice of Motion and Motion for Judgment Against Garnishee, Affidavit In Support of Motion For Judgment Against Garnishee',0,0,29,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (97,'Authenticated Copy of Amended Summons and Complaint, Authenticated Copy of Summons and Complaint, Electronic Filing Notice',0,0,30,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (98,'Authenticated Copy of Amended Summons and Complaint- Small Claims Claim for Money, Electronic Filing Notice',0,0,31,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (99,'Authenticated Copy of Summons and Complaint, La Crosse County Foreclosure Mediation Program, Electronic Filing Notice',0,0,0,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (100,'Authenticated Copy of Non-Earnings Garnishment Summons and Complaint, Creditor''s Claim, Non-Earnings Garnishment Garnishee Answer, Garnishment Fee Check',0,0,32,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (101,'Notice Regarding Mediation',0,0,33,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (102,'Authenticated Copy of Summons and Complaint- Small Claims Tort/Personal Injury, Electronic Filing Notice',0,0,34,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (103,'Authenticated Copy of Amended Summons and Complaint- Small Claims Tort/Personal Injury, Electronic Filing Notice',0,0,35,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (104,'Order to Appear Before Court Commissioner, Affidavit, Instructions to Appear Remotely',0,0,36,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (105,'Conference Order and Notice of Scheduling Conference',0,0,37,0);
INSERT INTO document(Id,Name,IsDeleted,HasText,SortOrder,TextBefore) VALUES (106,'Instructions for How to Appear Remotely',0,0,38,0);

SET IDENTITY_INSERT dbo.document OFF