USE [serviture]
GO
/****** Object:  Table [dbo].[address]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServeId] [int] NOT NULL,
	[StreetAddress] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_Address_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[affidavit]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[affidavit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNumber] [varchar](50) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[AffidavitType] [int] NOT NULL,
	[ServerId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsArchived] [bit] NOT NULL,
	[IsPaid] [bit] NOT NULL,
	[IsNotMilitary] [bit] NOT NULL,
	[IsNotMarried] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ServeId] [int] NULL,
	[EntityId] [int] NULL,
	[CareOfId] [int] NULL,
	[RelationshipId] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[ContractorUserId] [int] NULL,
	[ClientId] [int] NULL,
	[ContactId] [int] NULL,
	[CaseNameId] [int] NULL,
	[DefendantId] [int] NULL,
	[OtherLocationId] [int] NULL,
	[CaseNumId] [int] NULL,
	[FileNumId] [int] NULL,
	[Charge] [decimal](10, 2) NULL,
	[CourtDate] [datetime] NULL,
	[State] [nvarchar](50) NULL,
	[County] [nvarchar](50) NULL,
	[NotaryState] [nvarchar](50) NULL,
	[NotaryCounty] [nvarchar](50) NULL,
	[SigState] [nvarchar](50) NULL,
 CONSTRAINT [PK_Affidavit_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[attempt]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attempt](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServeId] [int] NOT NULL,
	[IsSuccess] [bit] NOT NULL,
	[DateServed] [datetime] NULL,
	[OtherInfo] [nvarchar](500) NULL,
 CONSTRAINT [PK_Attempt_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[case]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[case](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsArchived] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ClientId] [int] NULL,
	[ContactId] [int] NULL,
	[CaseNameId] [int] NULL,
	[DefendantId] [int] NULL,
	[OtherLocationId] [int] NULL,
	[CaseNumId] [int] NULL,
	[FileNumId] [int] NULL,
	[CourtDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[State] [nvarchar](50) NULL,
	[County] [nvarchar](50) NULL,
 CONSTRAINT [PK_Case_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[client]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[client](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[DefaultFee] [decimal](8, 2) NULL,
	[IsDeleted] [bit] NOT NULL,
	[ShowUnsuccessful] [bit] NOT NULL,
	[ClientName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Client_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[company]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[company](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Company_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[contractoruser]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contractoruser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ContractorId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_ContractorUser_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[document]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[document](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[HasText] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[TextBefore] [bit] NOT NULL,
 CONSTRAINT [PK_Document_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[documentbyserve]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[documentbyserve](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServeId] [int] NOT NULL,
	[DocumentId] [int] NOT NULL,
	[DocumentText] [nvarchar](500) NULL,
 CONSTRAINT [PK_DocumentByServe_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[documentbyuser]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[documentbyuser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DocumentId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_DocumentByUser_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[field]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[field](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](500) NOT NULL,
	[FieldType] [tinyint] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CompanyId] [tinyint] NOT NULL,
	[IsArchived] [tinyint] NOT NULL,
 CONSTRAINT [PK_Field_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[file]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[file](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServeId] [int] NOT NULL,
	[Filename] [nvarchar](500) NOT NULL,
	[Size] [int] NOT NULL,
	[UploadedBy] [int] NOT NULL,
	[UploadedOn] [datetime] NOT NULL,
	[FileType] [varchar](50) NOT NULL,
	[Metadata] [nvarchar](500) NULL,
	[FileData] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_File_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[quickattempt]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[quickattempt](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Value] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_QuickAttempt_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reason]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reason](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Value] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Reason_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reasonbyaffidavit]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reasonbyaffidavit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ReasonId] [int] NOT NULL,
	[AffidavitId] [int] NOT NULL,
 CONSTRAINT [PK_ReasonByAffidavit_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[serve]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[serve](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CaseId] [int] NOT NULL,
	[ServeStatus] [int] NOT NULL,
	[IsDeleted] [int] NOT NULL,
	[IsArchived] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ServerId] [int] NULL,
	[EntityId] [int] NULL,
	[ContractorUserId] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[DueDate] [datetime] NULL,
	[Charge] [decimal](10, 2) NULL,
	[NotaryCounty] [nvarchar](50) NULL,
	[NotaryState] [nvarchar](50) NULL,
	[SigState] [nvarchar](50) NULL,
	[Notes] [nvarchar](200) NULL,
 CONSTRAINT [PK_Serve_Id] PRIMARY KEY CLUSTERED 
(
	[Id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[UserType] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[CanEdit] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[EntityName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Users_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [serviture]
GO

/****** Object:  Table [dbo].[usersettings]    Script Date: 8/15/2021 4:22:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[usersettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[NotaryState] [nvarchar](50) NULL,
	[NotaryCounty] [nvarchar](50) NULL,
	[SigState] [nvarchar](50) NULL,
 CONSTRAINT [PK_usersettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Index [IX_Address_ServeId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_Address_ServeId] ON [dbo].[address]
(
	[ServeId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Affidavit_ServeId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_Affidavit_ServeId] ON [dbo].[affidavit]
(
	[ServeId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Attempt_ServeId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_Attempt_ServeId] ON [dbo].[attempt]
(
	[ServeId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DocumentByServe_ServeId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_DocumentByServe_ServeId] ON [dbo].[documentbyserve]
(
	[ServeId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DocumentByUser_UserId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_DocumentByUser_UserId] ON [dbo].[documentbyuser]
(
	[UserId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Field_FieldType]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_Field_FieldType] ON [dbo].[field]
(
	[FieldType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_File_ServeId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_File_ServeId] ON [dbo].[file]
(
	[ServeId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ReasonByAffidavit_ServeId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_ReasonByAffidavit_ServeId] ON [dbo].[reasonbyaffidavit]
(
	[AffidavitId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Serve_CaseId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_Serve_CaseId] ON [dbo].[serve]
(
	[CaseId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Serve_ServerId]    Script Date: 3/10/2021 3:31:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_Serve_ServerId] ON [dbo].[serve]
(
	[ServerId] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[affidavit] ADD  CONSTRAINT [DF_affidavit_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[affidavit] ADD  CONSTRAINT [DF_affidavit_IsArchived]  DEFAULT ((0)) FOR [IsArchived]
GO
ALTER TABLE [dbo].[affidavit] ADD  CONSTRAINT [DF_affidavit_IsPaid]  DEFAULT ((0)) FOR [IsPaid]
GO
ALTER TABLE [dbo].[affidavit] ADD  CONSTRAINT [DF_affidavit_IsNotMilitary]  DEFAULT ((1)) FOR [IsNotMilitary]
GO
ALTER TABLE [dbo].[affidavit] ADD  CONSTRAINT [DF_affidavit_IsNotMarried]  DEFAULT ((1)) FOR [IsNotMarried]
GO
ALTER TABLE [dbo].[attempt] ADD  CONSTRAINT [DF_attempt_IsSuccess]  DEFAULT ((0)) FOR [IsSuccess]
GO
ALTER TABLE [dbo].[case] ADD  CONSTRAINT [DF_case_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[case] ADD  CONSTRAINT [DF_case_IsArchived]  DEFAULT ((0)) FOR [IsArchived]
GO
ALTER TABLE [dbo].[client] ADD  CONSTRAINT [DF_client_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[client] ADD  CONSTRAINT [DF_client_ShowUnsuccessful]  DEFAULT ((0)) FOR [ShowUnsuccessful]
GO
ALTER TABLE [dbo].[company] ADD  CONSTRAINT [DF_company_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[file] ADD  CONSTRAINT [DF_file_ServeId]  DEFAULT ((1)) FOR [ServeId]
GO
ALTER TABLE [dbo].[serve] ADD  CONSTRAINT [DF_serve_ServeStatus]  DEFAULT ((0)) FOR [ServeStatus]
GO
ALTER TABLE [dbo].[serve] ADD  CONSTRAINT [DF_serve_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[serve] ADD  CONSTRAINT [DF_serve_IsArchived]  DEFAULT ((0)) FOR [IsArchived]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_user_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_user_IsAdmin]  DEFAULT ((0)) FOR [IsAdmin]
GO
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_user_CanEdit]  DEFAULT ((0)) FOR [CanEdit]
GO

/****** Object:  StoredProcedure [dbo].[BillingReport]    Script Date: 8/6/2021 11:19:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BillingReport] 
	-- Add the parameters for the stored procedure here
	@ClientId int = null, 
	@ServerId int = null,
	@StartDate datetime = null,
	@EndDate datetime = null,
	@State varchar(20) = null,
	@County varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 
		SUM(CASE WHEN a.AffidavitType = 0 THEN 1 ELSE 0 END) AS PersonalCount,
		SUM(CASE WHEN a.AffidavitType = 1 THEN 1 ELSE 0 END) AS SubstituteCount,
		SUM(CASE WHEN a.AffidavitType = 2 THEN 1 ELSE 0 END) AS ReasonableCount,
		SUM(CASE WHEN a.AffidavitType = 3 THEN 1 ELSE 0 END) AS CorporationCount,
		SUM(CASE WHEN a.AffidavitType = 4 THEN 1 ELSE 0 END) AS OtherCount,
		SUM(CASE WHEN s.ServeStatus = 1 THEN s.Charge ELSE a.Charge END) AS TotalCharge,
		SUM(CASE WHEN s.ServeStatus = 1 THEN 1 ELSE 0 END) AS OpenServes,
		COUNT(a.Id) AS TotalServes
	FROM dbo.affidavit a
	LEFT JOIN dbo.serve s on a.ServeId = s.Id
	LEFT JOIN dbo.[case] c ON c.Id = s.CaseId
	WHERE s.IsDeleted = 0
	AND (@ClientId IS NULL OR c.ClientId = @ClientId)
	AND (@ServerId IS NULL OR s.ServerId = @ServerId)
	AND (@StartDate IS NULL OR a.CreatedOn >= @StartDate)
	AND (@EndDate IS NULL OR a.CreatedOn <= @EndDate)
	AND (@State IS NULL OR a.[State] = @State)
	AND (@County IS NULL OR a.County = @County)
END

/****** Object:  StoredProcedure [dbo].[CleanUpTestData]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CleanUpTestData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.address WHERE 1 = 1;
	DELETE FROM dbo.affidavit WHERE 1 = 1;	
	DELETE FROM dbo.attempt WHERE 1 = 1;	
	DELETE FROM dbo.[case] WHERE 1 = 1;
	DELETE FROM dbo.documentbyserve WHERE 1 = 1;
	DELETE FROM dbo.reasonbyaffidavit WHERE 1 = 1;
	DELETE FROM dbo.serve WHERE 1 = 1;
END
GO
/****** Object:  StoredProcedure [dbo].[searchaffidavit]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Murrihy, Sean
-- Create date: 2021-01-23
-- Description:	Searching Affidavit Table
-- =============================================
CREATE PROCEDURE [dbo].[searchaffidavit] 
	@IsDeleted int = 0,
	@PageIndex int = 0,
	@PageSize int = 10,
	@SearchTerm nvarchar(200) = NULL,
	@SearchDate DATETIME = NULL,
	@SearchUser int = NULL
	
AS
BEGIN

	SELECT a.*
	FROM [dbo].affidavit a 
	LEFT JOIN [dbo].client client ON a.ClientId = client.Id
	LEFT JOIN [dbo].field f1 ON a.ContactId = f1.Id
	LEFT JOIN [dbo].field f2 ON a.FileNumId = f2.Id
	LEFT JOIN [dbo].field f3 ON a.CaseNameId = f3.Id
	LEFT JOIN [dbo].field f4 ON a.DefendantId = f4.Id
	LEFT JOIN [dbo].field f5 ON a.CaseNumId = f5.Id
	LEFT JOIN [dbo].field f6 ON a.OtherLocationId = f6.Id
	LEFT JOIN [dbo].field f7 ON a.EntityId = f7.Id

	WHERE a.IsDeleted = 0
	AND ( f1.Value like @SearchTerm + '%' 
	OR f2.Value like @SearchTerm + '%' 
	OR f3.Value like '%' + @SearchTerm + '%'
	OR f4.Value like '%' + @SearchTerm + '%'
	OR f5.Value like @SearchTerm + '%'	
	OR f6.Value like @SearchTerm + '%'
	OR f7.Value like '%' + @SearchTerm + '%'
	OR a.InvoiceNumber like @SearchTerm + '%')
	AND (@SearchDate is null OR datediff(day, a.CreatedOn, @SearchDate) = 0)
	AND (@SearchUser is null OR a.ServerId = @SearchUser)
		
	ORDER BY a.CreatedOn desc OFFSET @PageSize * @PageIndex ROWS FETCH NEXT @PageSize ROWS ONLY

	RETURN @@RowCount
END
GO
/****** Object:  StoredProcedure [dbo].[searchcase]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Murrihy, Sean
-- Create date: 2021-01-23
-- Description:	Searching Case Table
-- =============================================
CREATE PROCEDURE [dbo].[searchcase] 
	@IsDeleted int = 0,
	@PageIndex int = 0,
	@PageSize int = 10,
	@SearchTerm nvarchar(200) = NULL,
	@SearchDate DATETIME = NULL
	
AS
BEGIN

	SELECT c.*
	FROM [dbo].[case] c 
	LEFT JOIN [dbo].client client ON c.ClientId = client.Id
	LEFT JOIN [dbo].field f1 ON c.ContactId = f1.Id
	LEFT JOIN [dbo].field f2 ON c.FileNumId = f2.Id
	LEFT JOIN [dbo].field f3 ON c.CaseNameId = f3.Id
	LEFT JOIN [dbo].field f4 ON c.DefendantId = f4.Id
	LEFT JOIN [dbo].field f5 ON c.CaseNumId = f5.Id
	LEFT JOIN [dbo].field f6 ON c.OtherLocationId = f6.Id

	WHERE c.IsDeleted = 0
	AND ( client.ClientName like @SearchTerm + '%'
	OR f1.Value like @SearchTerm + '%' 
	OR f2.Value like @SearchTerm + '%' 
	OR f3.Value like '%' + @SearchTerm + '%'
	OR f4.Value like '%' + @SearchTerm + '%'
	OR f5.Value like @SearchTerm + '%'	
	OR f6.Value like @SearchTerm + '%')
	AND (@SearchDate is null OR datediff(day, c.CreatedOn, @SearchDate) = 0)
		
	ORDER BY c.CreatedOn desc OFFSET @PageSize * @PageIndex ROWS FETCH NEXT @PageSize ROWS ONLY

	RETURN @@RowCount
END
GO
/****** Object:  StoredProcedure [dbo].[searchclient]    Script Date: 8/6/2021 11:23:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Murrihy, Sean
-- Create date: 4/25/2021
-- Description:	The screen for displaying client view
-- =============================================
CREATE PROCEDURE [dbo].[searchclient]
	-- Add the parameters for the stored procedure here
	@ClientId int,
	@IsDeleted int = 0,
	@IsArchived int = 0,
	@PageIndex int = 0,
	@PageSize int = 10,
	@SearchTerm nvarchar(200) = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
	CASE WHEN s.ServeStatus = 1 THEN 'In Progress' WHEN s.ServeStatus = 2 THEN 'Complete' END AS Status,
	CASE WHEN a.AffidavitType = NULL THEN '' WHEN a.AffidavitType = 0 THEN 'Personal' WHEN a.AffidavitType = 1 THEN 'Substitute' WHEN a.AffidavitType = 2 THEN 'Reasonable' WHEN a.AffidavitType = 3 THEN 'Corporate' END AS AffidavitType,
	f1.Value As ClientFileNumber,
	f2.Value As CaseNumber,
	f3.Value AS Defendant,
	f4.Value As EntityServed,
	(SELECT COUNT(*) FROM dbo.attempt att WHERE att.ServeId = s.Id) AS AttemptCount,
	(SELECT MAX(att2.DateServed) FROM dbo.attempt att2 WHERE att2.ServeId = s.Id) AS LatestAttempt,
	(SELECT COUNT(*) FROM dbo.[file] f WHERE f.ServeId = s.Id) AS FileCount,
	a.InvoiceNumber,
	u.FirstName + ' ' + u.LastName AS [Server],
	a.CreatedOn AS ServeCompleted,
	s.CreatedOn,
	a.Id AS AffidavitId,
	s.Id AS ServeId,
	c.Id AS CaseId
FROM dbo.serve s
LEFT JOIN dbo.[case] c ON c.Id = s.CaseId
LEFT JOIN dbo.affidavit a ON s.Id = a.ServeId
LEFT JOIN dbo.field f1 ON ISNULL(a.FileNumId, c.FileNumId) = f1.Id
LEFT JOIN dbo.field f2 ON ISNULL(a.CaseNumId, c.CaseNumId) = f2.Id
LEFT JOIN dbo.field f3 ON ISNULL(a.DefendantId, c.DefendantId) = f3.Id
LEFT JOIN dbo.field f4 ON ISNULL(a.EntityId, s.EntityId) = f4.Id
LEFT JOIN dbo.users u ON s.ServerId = u.Id 
WHERE c.ClientId = @ClientId
AND c.IsDeleted = @IsDeleted AND c.IsArchived = @IsArchived 
AND s.IsDeleted = @IsDeleted AND s.IsArchived = @IsArchived
AND (a.IsPaid IS NULL OR a.IsPaid = 0)

AND (@SearchTerm IS NULL OR 
	( f1.Value like @SearchTerm + '%' 
	OR f2.Value like @SearchTerm + '%' 
	OR f3.Value like '%' + @SearchTerm + '%'
	OR f4.Value like '%' + @SearchTerm + '%'
	OR a.InvoiceNumber like @SearchTerm + '%'))

	ORDER BY s.CreatedOn desc OFFSET @PageSize * @PageIndex ROWS FETCH NEXT @PageSize ROWS ONLY

	RETURN @@RowCount

END
GO
/****** Object:  StoredProcedure [dbo].[searchserve]    Script Date: 3/10/2021 3:31:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[searchserve] 
	-- Add the parameters for the stored procedure here
	@IsDeleted int = 0,
	@PageIndex int = 0,
	@PageSize int = 10,
	@SearchTerm nvarchar(200) = NULL,
	@SearchDate DATETIME = NULL,
	@SearchUser int = NULL
	
AS
BEGIN
	SELECT s.*
    FROM [dbo].[serve] s
    LEFT JOIN [dbo].[case] c ON s.CaseId = c.Id
    LEFT JOIN [dbo].client client ON c.ClientId = client.Id
    LEFT JOIN [dbo].field f1 ON s.EntityId = f1.Id
    LEFT JOIN [dbo].field f3 ON c.FileNumId = f3.Id
    LEFT JOIN [dbo].field f4 ON c.CaseNameId = f4.Id
    LEFT JOIN [dbo].field f5 ON c.DefendantId = f5.Id
    LEFT JOIN [dbo].field f6 ON c.CaseNumId = f6.Id

	WHERE s.IsDeleted = 0
	AND s.ServeStatus = 1
	AND ( f1.Value like '%' + @SearchTerm + '%' 
	OR f3.Value like @SearchTerm + '%'
	OR f4.Value like @SearchTerm + '%'
	OR f5.Value like '%' + @SearchTerm + '%'	
	OR f6.Value like @SearchTerm + '%')
	AND (@SearchDate is null OR datediff(day, s.CreatedOn, @SearchDate) = 0)
	AND (@SearchUser is null OR s.ServerId = @SearchUser)
		
	ORDER BY s.CreatedOn desc OFFSET @PageSize * @PageIndex ROWS FETCH NEXT @PageSize ROWS ONLY

	RETURN @@RowCount
END