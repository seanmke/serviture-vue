﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ClientData : DataBase<Client, ClientSearchRequest>, IClientData
    {

        public ClientData(IConfiguration configuration, ILogger<Client> log) : base(configuration, log)
        {
            Columns = Client.Columns.ToList();
            SearchColumns = ClientSearchRequest.Columns.ToList();
            TableName = DataConstants.Client;
        }

        public async Task<int> CreateClient(Client item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<Client> GetClient(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<Client>> GetClients()
        {
            var items = await Get();
            var sortedList = items.OrderBy(x => x.ClientName).ToList();
            return sortedList;
        }

        public async Task<int> UpdateClient(int id, Client item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteClient(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteClient(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Client>> SearchClients(ClientSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }

        public async Task<Client> GetRandomRecord()
        {
            var constraint = string.Empty;
            var result = await GetRandomRecord(constraint);
            return result;
        }
    }
}
