﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AffidavitData : DataBase<Affidavit, AffidavitSearchRequest>, IAffidavitData
    {
        public AffidavitData(IConfiguration configuration, ILogger<Affidavit> log) : base(configuration, log)
        {
            Columns = Affidavit.Columns.ToList();
            SearchColumns = AffidavitSearchRequest.Columns.ToList();
            TableName = DataConstants.Affidavit;
        }

        public async Task<int> CreateAffidavit(Affidavit item)
        {
            item.InvoiceNumber = "0";
            var newId = await Create(item);
            var affidavit = await GetAffidavit(newId);
            affidavit.InvoiceNumber = newId.ToString();
            await UpdateAffidavit(newId, affidavit);
            return newId;
        }

        public async Task<Affidavit> GetAffidavit(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<Affidavit>> GetAffidavits()
        {
            var items = await Get();
            return items;
        }

        public async Task<int> UpdateAffidavit(int id, Affidavit item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteAffidavit(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteAffidavit(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Affidavit>> SearchAffidavits(AffidavitSearchRequest searchRequest)
        {
            Log.LogInformation(searchRequest.ToString());
            var results = await Search(searchRequest);
            return results;
        }

        public async Task<SearchResponse<Affidavit>> SearchAffidavitTable(AffidavitSearchRequest searchRequest)
        {
            Log.LogInformation(searchRequest.ToString());
            var results = await SearchTable(searchRequest);
            return results;
        }
    }
}