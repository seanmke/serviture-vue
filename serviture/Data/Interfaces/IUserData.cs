﻿namespace Serviture.Data
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Serviture.Models;

    public interface IUserData
    {
        Task<User> Authenticate(AuthenticateModel authenticateModel);

        Task<int> CreateUser(User user);

        Task<User> GetUser(int id);

        Task<List<User>> GetUsers();

        Task<int> UpdateUser(int id, User user);

        Task<int> DeleteUser(int id);

        Task<int> HardDeleteUser(int id);

        Task<SearchResponse<User>> SearchUsers(UserSearchRequest searchRequest);
    }
}
