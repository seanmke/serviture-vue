﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface ICaseData
    {
        Task<int> CreateCase(Case item);

        Task<Case> GetCase(int id);

        Task<int> UpdateCase(int id, Case item);

        Task<int> DeleteCase(int id);

        Task<int> HardDeleteCase(int id);

        Task<SearchResponse<Case>> SearchCases(CaseSearchRequest searchRequest);

        Task<SearchResponse<Case>> SearchCaseTable(CaseSearchRequest searchRequest);
    }
}
