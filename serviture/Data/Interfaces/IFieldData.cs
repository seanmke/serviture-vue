﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IFieldData
    {
        Task<int> CreateField(Field item);

        Task<Field> GetField(int id);

        Task<int> UpdateField(int id, Field item);

        Task<int> DeleteField(int id);

        Task<int> HardDeleteField(int id);

        Task<SearchResponse<Field>> SearchFields(FieldSearchRequest searchRequest);

        Task<Field> GetRandomRecord(FieldType fieldType);
    }
}
