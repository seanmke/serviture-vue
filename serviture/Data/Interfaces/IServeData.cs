﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IServeData
    {
        Task<int> CreateServe(Serve item);

        Task<Serve> GetServe(int id);

        Task<int> UpdateServe(int id, Serve item);

        Task<int> DeleteServe(int id);

        Task<int> HardDeleteServe(int id);

        Task<SearchResponse<Serve>> SearchServes(ServeSearchRequest searchRequest);

        Task<SearchResponse<Serve>> SearchServeTable(ServeSearchRequest searchRequest);
    }
}