﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IAddressData
    {
        Task<int> CreateAddress(Address address);

        Task<Address> GetAddress(int id);

        Task<int> UpdateAddress(int id, Address address);

        Task<int> HardDeleteAddress(int id);

        Task<SearchResponse<Address>> SearchAddresses(AddressSearchRequest searchRequest);
    }
}
