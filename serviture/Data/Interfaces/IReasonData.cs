﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IReasonData
    {
        Task<int> CreateReason(Reason item);

        Task<Reason> GetReason(int id);

        Task<List<Reason>> GetReasons();

        Task<int> UpdateReason(int id, Reason item);

        Task<int> DeleteReason(int id);

        Task<int> HardDeleteReason(int id);

        Task<SearchResponse<Reason>> SearchReasons(ReasonSearchRequest searchRequest);
    }
}