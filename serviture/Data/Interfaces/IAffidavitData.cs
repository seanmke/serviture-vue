﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IAffidavitData
    {
        Task<int> CreateAffidavit(Affidavit item);

        Task<Affidavit> GetAffidavit(int id);

        Task<int> UpdateAffidavit(int id, Affidavit item);

        Task<int> DeleteAffidavit(int id);

        Task<int> HardDeleteAffidavit(int id);

        Task<SearchResponse<Affidavit>> SearchAffidavits(AffidavitSearchRequest searchRequest);

        Task<SearchResponse<Affidavit>> SearchAffidavitTable(AffidavitSearchRequest searchRequest);
    }
}