﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IDocumentByServeData
    {
        Task<int> CreateDocumentByServe(DocumentByServe item);

        Task<DocumentByServe> GetDocumentByServe(int id);

        Task<int> HardDeleteDocumentByServe(int id);

        Task<SearchResponse<DocumentByServe>> SearchDocumentByServes(DocumentByServeSearchRequest searchRequest);
    }
}
