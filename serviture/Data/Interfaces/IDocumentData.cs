﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IDocumentData
    {
        Task<int> CreateDocument(Document item);

        Task<Document> GetDocument(int id);

        Task<List<Document>> GetDocuments();

        Task<List<Document>> GetAllDocuments();

        Task<int> UpdateDocument(int id, Document item);

        Task<int> DeleteDocument(int id);

        Task<int> HardDeleteDocument(int id);

        Task<SearchResponse<Document>> SearchDocuments(DocumentSearchRequest searchRequest);
    }
}
