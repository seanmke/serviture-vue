﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IReportData
    {
        Task<BillingReport> RunBillingReport(BillingReportSearchRequest searchRequest);

        Task<List<ReportServeAttemptSummary>> RunServeAttemptSummaryReport(ReportServeAttemptSummaryRequest searchRequest);
    }
}
