﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IUserSettingsData
    {
        Task<int> CreateUserSettings(UserSettings item);

        Task<UserSettings> GetUserSettings(int id);

        Task<int> UpdateUserSettings(int id, UserSettings item);

        Task<int> HardDeleteUserSettings(int id);

        Task<SearchResponse<UserSettings>> SearchUserSettingss(UserSettingsSearchRequest searchRequest);
    }
}
