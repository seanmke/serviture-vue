﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IQuickAttemptData
    {
        Task<int> CreateQuickAttempt(QuickAttempt item);

        Task<QuickAttempt> GetQuickAttempt(int id);

        Task<List<QuickAttempt>> GetQuickAttempts();

        Task<int> UpdateQuickAttempt(int id, QuickAttempt item);

        Task<int> DeleteQuickAttempt(int id);

        Task<int> HardDeleteQuickAttempt(int id);

        Task<SearchResponse<QuickAttempt>> SearchQuickAttempts(QuickAttemptSearchRequest searchRequest);
    }
}