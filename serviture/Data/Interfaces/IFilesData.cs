﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IFilesData
    {
        Task<int> CreateFiles(FileUpload item);

        Task<FileUpload> GetFile(int id);

        Task<int> UpdateFile(int id, FileUpload item);

        Task<int> HardDeleteFiles(int id);

        Task<SearchResponse<FileUpload>> SearchFiles(FilesSearchRequest searchRequest);
    }
}
