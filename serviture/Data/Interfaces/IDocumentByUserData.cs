﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IDocumentByUserData
    {
        Task<int> CreateDocumentByUser(DocumentByUser item);

        Task<DocumentByUser> GetDocumentByUser(int id);

        Task<int> HardDeleteDocumentByUser(int id);

        Task<SearchResponse<DocumentByUser>> SearchDocumentByUsers(DocumentByUserSearchRequest searchRequest);
    }
}
