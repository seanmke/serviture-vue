﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IAttemptData
    {
        Task<int> CreateAttempt(Attempt item);

        Task<Attempt> GetAttempt(int id);

        Task<int> HardDeleteAttempt(int id);

        Task<SearchResponse<Attempt>> SearchAttempts(AttemptSearchRequest searchRequest);
    }
}
