﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IClientPortalData
    {
        Task<SearchResponse<ClientPortalRow>> SearchClientPortal(ClientPortalSearchRequest searchRequest);
    }
}
