﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IReasonByAffidavitData
    {
        Task<int> CreateReasonByAffidavit(ReasonByAffidavit item);

        Task<ReasonByAffidavit> GetReasonByAffidavit(int id);

        Task<int> HardDeleteReasonByAffidavit(int id);

        Task<SearchResponse<ReasonByAffidavit>> SearchReasonByAffidavits(ReasonByAffidavitSearchRequest searchRequest);
    }
}
