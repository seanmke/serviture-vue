﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IContractorUserData
    {
        Task<int> CreateContractorUser(ContractorUser item);

        Task<ContractorUser> GetContractorUser(int id);

        Task<int> UpdateContractorUser(int id, ContractorUser item);

        Task<int> DeleteContractorUser(int id);

        Task<int> HardDeleteContractorUser(int id);

        Task<SearchResponse<ContractorUser>> SearchContractorUsers(ContractorUserSearchRequest searchRequest);
    }
}