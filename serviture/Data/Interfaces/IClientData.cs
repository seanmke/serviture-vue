﻿namespace Serviture.Data
{
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IClientData
    {
        Task<int> CreateClient(Client item);

        Task<Client> GetClient(int id);

        Task<List<Client>> GetClients();

        Task<int> UpdateClient(int id, Client item);

        Task<int> DeleteClient(int id);

        Task<int> HardDeleteClient(int id);

        Task<SearchResponse<Client>> SearchClients(ClientSearchRequest searchRequest);

        Task<Client> GetRandomRecord();
    }
}
