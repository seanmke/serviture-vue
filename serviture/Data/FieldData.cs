﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class FieldData : DataBase<Field, FieldSearchRequest>, IFieldData
    {
        public FieldData(IConfiguration configuration, ILogger<Field> log) : base(configuration, log)
        {
            Columns = Field.Columns.ToList();
            SearchColumns = FieldSearchRequest.Columns.ToList();
            TableName = DataConstants.Field;
        }

        public async Task<int> CreateField(Field item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<Field> GetField(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<Field>> GetFields()
        {
            var items = await Get();
            return items;
        }

        public async Task<int> UpdateField(int id, Field item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteField(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteField(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Field>> SearchFields(FieldSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }

        public async Task<Field> GetRandomRecord(FieldType fieldType)
        {
            var constraint = $" FieldType = {(int)fieldType} ";
            var result = await GetRandomRecord(constraint);
            return result;
        }
    }
}
