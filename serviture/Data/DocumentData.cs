﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class DocumentData : DataBase<Document, DocumentSearchRequest>, IDocumentData
    {
        public DocumentData(IConfiguration configuration, ILogger<Document> log) : base(configuration, log)
        {
            Columns = Document.Columns.ToList();
            SearchColumns = DocumentSearchRequest.Columns.ToList();
            TableName = DataConstants.Document;
        }

        public async Task<int> CreateDocument(Document item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<Document> GetDocument(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<Document>> GetDocuments()
        {
            OrderBy = DataConstants.OrderBySortOrder;

            var items = await Get();
            return items;
        }

        public async Task<List<Document>> GetAllDocuments()
        {
            OrderBy = DataConstants.OrderBySortOrder;

            var items = await GetAll();
            return items;
        }

        public async Task<int> UpdateDocument(int id, Document item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteDocument(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteDocument(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Document>> SearchDocuments(DocumentSearchRequest searchRequest)
        {
            searchRequest.OrderBy = DataConstants.OrderBySortOrder;
            var results = await Search(searchRequest);
            return results;
        }
    }
}
