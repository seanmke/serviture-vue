﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CaseData : DataBase<Case, CaseSearchRequest>, ICaseData
    {
        public CaseData(IConfiguration configuration, ILogger<Case> log) : base (configuration, log)
        {
            Columns = Case.Columns.ToList();
            SearchColumns = CaseSearchRequest.Columns.ToList();
            TableName = DataConstants.Case;
        }

        public async Task<int> CreateCase(Case item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<Case> GetCase(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<Case>> GetCases()
        {
            var items = await Get();
            return items;
        }

        public async Task<int> UpdateCase(int id, Case item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteCase(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteCase(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Case>> SearchCases(CaseSearchRequest searchRequest)
        {
            Log.LogInformation(searchRequest.ToString());
            var results = await Search(searchRequest);
            return results;
        }

        public async Task<SearchResponse<Case>> SearchCaseTable(CaseSearchRequest searchRequest)
        {
            Log.LogInformation(searchRequest.ToString());
            var results = await SearchTable(searchRequest);
            return results;
        }
    }
}
