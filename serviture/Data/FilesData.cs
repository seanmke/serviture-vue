﻿namespace Serviture.Data
{
    using Dapper;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;

    public class FilesData : DataBase<FileUpload, FilesSearchRequest>, IFilesData
    {
        public FilesData(IConfiguration configuration, ILogger<FileUpload> log) : base(configuration, log)
        {
            Columns = FileUpload.Columns.ToList();
            SearchColumns = FilesSearchRequest.Columns.ToList();
            TableName = DataConstants.File;
        }

        public async Task<int> CreateFiles(FileUpload item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<FileUpload> GetFile(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<int> UpdateFile(int id, FileUpload item)
        {
            var rowsAffected = await Update(id, item);
            return rowsAffected;
        }

        //TODO Remove from Azure Storage?
        public async Task<int> HardDeleteFiles(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<FileUpload>> SearchFiles(FilesSearchRequest searchItem)
        {
            try
            {
                var searchResponse = new SearchResponse<FileUpload>();

                string queryString = $@"SELECT [Id],[ServeId],[Filename],[Size],[UploadedBy],[UploadedOn],[FileType],[Metadata] 
                                        FROM dbo.[file] WHERE ServeId = @ServeId ";

                var parameters = new DynamicParameters();
                parameters.Add("@ServeId", searchItem.ServeId);

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var items = await connection.QueryAsync<FileUpload>(queryString, parameters);
                    searchResponse.Results = items;
                    searchResponse.TotalResults = items.Count();
                    searchResponse.HasMoreResults = false;
                }

                return searchResponse;
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return null;
            }
        }
    }
}
