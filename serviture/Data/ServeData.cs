﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ServeData : DataBase<Serve, ServeSearchRequest>, IServeData
    {
        public ServeData(IConfiguration configuration, ILogger<Serve> log) : base(configuration, log)
        {
            Columns = Serve.Columns.ToList();
            SearchColumns = ServeSearchRequest.Columns.ToList();
            TableName = DataConstants.Serve;
        }

        public async Task<int> CreateServe(Serve item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<Serve> GetServe(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<Serve>> GetServes()
        {
            var items = await Get();
            return items;
        }

        public async Task<int> UpdateServe(int id, Serve item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteServe(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteServe(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Serve>> SearchServes(ServeSearchRequest searchRequest)
        {
            Log.LogInformation(searchRequest.ToString());
            var results = await Search(searchRequest);
            return results;
        }

        public async Task<SearchResponse<Serve>> SearchServeTable(ServeSearchRequest searchRequest)
        {
            Log.LogInformation(searchRequest.ToString());
            var results = await SearchTable(searchRequest);
            return results;
        }
    }
}