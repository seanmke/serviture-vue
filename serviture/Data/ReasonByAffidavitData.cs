﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class ReasonByAffidavitData : DataBase<ReasonByAffidavit, ReasonByAffidavitSearchRequest>, IReasonByAffidavitData
    {
        public ReasonByAffidavitData(IConfiguration configuration, ILogger<ReasonByAffidavit> log) : base(configuration, log)
        {
            Columns = ReasonByAffidavit.Columns.ToList();
            SearchColumns = ReasonByAffidavitSearchRequest.Columns.ToList();
            TableName = DataConstants.ReasonByAffidavit;
        }

        public async Task<int> CreateReasonByAffidavit(ReasonByAffidavit item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<ReasonByAffidavit> GetReasonByAffidavit(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<int> HardDeleteReasonByAffidavit(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<ReasonByAffidavit>> SearchReasonByAffidavits(ReasonByAffidavitSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }
    }
}
