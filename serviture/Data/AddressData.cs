﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class AddressData : DataBase<Address, AddressSearchRequest>, IAddressData
    {
        public AddressData(IConfiguration configuration, ILogger<Address> log) : base(configuration, log)
        {
            Columns = Address.Columns.ToList();
            SearchColumns = AddressSearchRequest.Columns.ToList();
            TableName = DataConstants.Address;
        }

        public async Task<int> CreateAddress(Address address)
        {
            var newId = await Create(address);
            return newId;
        }

        public async Task<Address> GetAddress(int id)
        {
            var address = await Get(id);
            return address;
        }

        public async Task<int> UpdateAddress(int id, Address address)
        {
            var rowsUpdated = await Update(id, address);
            return rowsUpdated;
        }

        public async Task<int> HardDeleteAddress(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Address>> SearchAddresses(AddressSearchRequest address)
        {
            var results = await Search(address);
            return results;
        }
    }
}