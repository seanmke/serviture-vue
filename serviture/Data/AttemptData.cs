﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class AttemptData : DataBase<Attempt, AttemptSearchRequest>, IAttemptData
    {
        public AttemptData(IConfiguration configuration, ILogger<Attempt> log) : base(configuration, log)
        {
            Columns = Attempt.Columns.ToList();
            SearchColumns = AttemptSearchRequest.Columns.ToList();
            TableName = DataConstants.Attempt;
        }

        public async Task<int> CreateAttempt(Attempt item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<Attempt> GetAttempt(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<int> HardDeleteAttempt(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Attempt>> SearchAttempts(AttemptSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }
    }
}
