﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Serviture.Models;
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Serviture.Helpers;
    using System.IdentityModel.Tokens.Jwt;
    using System.Text;
    using Microsoft.IdentityModel.Tokens;
    using System.Security.Claims;
    using System.Collections.Generic;

    public class UserData :  DataBase<User, UserSearchRequest>, IUserData
    {
        public IPasswordHasher PasswordHasher { get; }

        public UserData(IConfiguration configuration, IPasswordHasher passwordHasher, ILogger<User> log) : base(configuration, log)
        {
            PasswordHasher = passwordHasher ?? throw new ArgumentNullException(nameof(passwordHasher));

            Columns = User.Columns.ToList();
            SearchColumns = UserSearchRequest.Columns.ToList();
            TableName = DataConstants.User;
        }

        public async Task<User> Authenticate(AuthenticateModel authenticateModel)
        {
            var userSearchRequest = new UserSearchRequest
            {
                UserName = authenticateModel.Username,
                PageIndex = 0,
                PageSize = 1
            };

            var userSearchResponse = await SearchUsers(userSearchRequest);

            if (userSearchResponse.TotalResults != 1)
            {
                Log.LogDebug("No users found.");
                return null;
            }

            var user = (User)userSearchResponse.Results.FirstOrDefault();

            var passCheck = PasswordHasher.Check(user.Password, authenticateModel.Password);

            if (!passCheck.Verified)
            {
                Log.LogDebug("Wrong password");
                return null;
            }

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Configuration["AppSettings:Secret"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return user;
        }

        public async Task<int> CreateUser(User user)
        {
            var hashPass = PasswordHasher.Hash(user.Password);
            user.Password = hashPass;

            var newId = await Create(user);
            return newId;
        }

        public async Task<User> GetUser(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<User>> GetUsers()
        {
            var items = await Get();
            return items;
        }

        public async Task<int> UpdateUser(int id, User user)
        {
            if (!string.IsNullOrWhiteSpace(user.Password))
            {
                var hashPass = PasswordHasher.Hash(user.Password);
                user.Password = hashPass;
            } 
            else
            {
                var original = await GetUser(id);
                user.Password = original.Password;
            }            

            var rowsUpdated = await Update(id, user);
            return rowsUpdated;
        }

        public async Task<int> DeleteUser(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteUser(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<User>> SearchUsers(UserSearchRequest searchRequest)
        {
            searchRequest.OrderBy = "FirstName";
            var results = await Search(searchRequest);
            return results;
        }
    }
}
