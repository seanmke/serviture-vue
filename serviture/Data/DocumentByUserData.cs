﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class DocumentByUserData : DataBase<DocumentByUser, DocumentByUserSearchRequest>, IDocumentByUserData
    {
        public DocumentByUserData(IConfiguration configuration, ILogger<DocumentByUser> log) : base(configuration, log)
        {
            Columns = DocumentByUser.Columns.ToList();
            SearchColumns = DocumentByUserSearchRequest.Columns.ToList();
            TableName = DataConstants.DocumentByUser;
            OrderBy = DataConstants.OrderBySortOrder;
        }

        public async Task<int> CreateDocumentByUser(DocumentByUser item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<DocumentByUser> GetDocumentByUser(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<int> HardDeleteDocumentByUser(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<DocumentByUser>> SearchDocumentByUsers(DocumentByUserSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }
    }
}
