﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ReasonData : DataBase<Reason, ReasonSearchRequest>, IReasonData
    {

        public ReasonData(IConfiguration configuration, ILogger<Reason> log) : base(configuration, log)
        {
            Columns = Reason.Columns.ToList();
            SearchColumns = ReasonSearchRequest.Columns.ToList();
            TableName = DataConstants.Reason;
        }

        public async Task<int> CreateReason(Reason item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<Reason> GetReason(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<Reason>> GetReasons()
        {
            OrderBy = DataConstants.OrderBySortOrder;

            var items = await Get();
            return items;
        }

        public async Task<int> UpdateReason(int id, Reason item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteReason(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteReason(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<Reason>> SearchReasons(ReasonSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }
    }
}
