﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class ContractorUserData : DataBase<ContractorUser, ContractorUserSearchRequest>, IContractorUserData
    {

        public ContractorUserData(IConfiguration configuration, ILogger<ContractorUser> log) : base(configuration, log)
        {
            Columns = ContractorUser.Columns.ToList();
            SearchColumns = ContractorUserSearchRequest.Columns.ToList();
            TableName = DataConstants.ContractorUser;
        }

        public async Task<int> CreateContractorUser(ContractorUser item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<ContractorUser> GetContractorUser(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<int> UpdateContractorUser(int id, ContractorUser item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteContractorUser(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteContractorUser(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<ContractorUser>> SearchContractorUsers(ContractorUserSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }
    }
}
