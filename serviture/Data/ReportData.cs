﻿namespace Serviture.Data
{
    using Dapper;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Threading.Tasks;

    public class ReportData : IReportData
    {
        public ILogger<ReportData> Log { get; }
        public IConfiguration Configuration { get; }

        public ReportData(IConfiguration configuration, ILogger<ReportData> log)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            Log = log ?? throw new ArgumentNullException(nameof(log));
        }

        public async Task<BillingReport> RunBillingReport(BillingReportSearchRequest searchRequest)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@ClientId", searchRequest.ClientId);
                parameters.Add("@ServerId", searchRequest.ServerId);
                parameters.Add("@StartDate", searchRequest.StartDate);
                parameters.Add("@EndDate", searchRequest.EndDate);
                parameters.Add("@State", searchRequest.State);
                parameters.Add("@County", searchRequest.County);

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var results = await connection.QueryAsync<BillingReport>($"[dbo].[BillingReport]", parameters, commandType: CommandType.StoredProcedure);
                    return results.AsList()[0];
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return null;
            }
        }

        public async Task<List<ReportServeAttemptSummary>> RunServeAttemptSummaryReport(ReportServeAttemptSummaryRequest searchRequest)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@ClientId", searchRequest.ClientId);
                parameters.Add("@StartDate", searchRequest.StartDate);
                parameters.Add("@EndDate", searchRequest.EndDate);
                parameters.Add("@IncludeUnsuccessful", searchRequest.IncludeUnsuccessful);

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var results = await connection.QueryAsync<ReportServeAttemptSummary>($"[dbo].[Report_ServeAttemptSummary]", parameters, commandType: CommandType.StoredProcedure);
                    return results.AsList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return null;
            }
        }
    }
}
