﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserSettingsData : DataBase<UserSettings, UserSettingsSearchRequest>, IUserSettingsData
    {
        public UserSettingsData(IConfiguration configuration, ILogger<UserSettings> log) : base(configuration, log)
        {
            Columns = UserSettings.Columns.ToList();
            SearchColumns = UserSettingsSearchRequest.Columns.ToList();
            TableName = DataConstants.UserSettings;
            OrderBy = DataConstants.OrderBySortOrder;
        }

        public async Task<int> CreateUserSettings(UserSettings item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<UserSettings> GetUserSettings(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<int> UpdateUserSettings(int id, UserSettings item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> HardDeleteUserSettings(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<UserSettings>> SearchUserSettingss(UserSettingsSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }
    }
}
