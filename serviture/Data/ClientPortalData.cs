﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class ClientPortalData : DataBase<ClientPortalRow, ClientPortalSearchRequest>, IClientPortalData
    {
        public ClientPortalData(IConfiguration configuration, ILogger<ClientPortalRow> log) : base(configuration, log)
        {
            Columns = Affidavit.Columns.ToList();
            SearchColumns = AffidavitSearchRequest.Columns.ToList();
            TableName = DataConstants.Client;
        }

        public async Task<SearchResponse<ClientPortalRow>> SearchClientPortal(ClientPortalSearchRequest searchRequest)
        {
            var results = await SearchTable(searchRequest);
            return results;
        }
    }
}
