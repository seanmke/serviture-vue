﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;

    public class DocumentByServeData : DataBase<DocumentByServe, DocumentByServeSearchRequest>, IDocumentByServeData
    {
        public DocumentByServeData(IConfiguration configuration, ILogger<DocumentByServe> log) : base(configuration, log)
        {
            Columns = DocumentByServe.Columns.ToList();
            SearchColumns = DocumentByServeSearchRequest.Columns.ToList();
            TableName = DataConstants.DocumentByServe;
        }

        public async Task<int> CreateDocumentByServe(DocumentByServe item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<DocumentByServe> GetDocumentByServe(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<int> HardDeleteDocumentByServe(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<DocumentByServe>> SearchDocumentByServes(DocumentByServeSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }
    }
}
