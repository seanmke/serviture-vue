﻿namespace Serviture.Data
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class QuickAttemptData : DataBase<QuickAttempt, QuickAttemptSearchRequest>, IQuickAttemptData
    {

        public QuickAttemptData(IConfiguration configuration, ILogger<QuickAttempt> log) : base(configuration, log)
        {
            Columns = QuickAttempt.Columns.ToList();
            SearchColumns = QuickAttemptSearchRequest.Columns.ToList();
            TableName = DataConstants.QuickAttempt;
        }

        public async Task<int> CreateQuickAttempt(QuickAttempt item)
        {
            var newId = await Create(item);
            return newId;
        }

        public async Task<QuickAttempt> GetQuickAttempt(int id)
        {
            var item = await Get(id);
            return item;
        }

        public async Task<List<QuickAttempt>> GetQuickAttempts()
        {
            OrderBy = DataConstants.OrderBySortOrder;

            var items = await Get();
            return items;
        }

        public async Task<int> UpdateQuickAttempt(int id, QuickAttempt item)
        {
            var rowsUpdated = await Update(id, item);
            return rowsUpdated;
        }

        public async Task<int> DeleteQuickAttempt(int id)
        {
            var didDelete = await Delete(id);
            return didDelete;
        }

        public async Task<int> HardDeleteQuickAttempt(int id)
        {
            var didDelete = await HardDelete(id);
            return didDelete;
        }

        public async Task<SearchResponse<QuickAttempt>> SearchQuickAttempts(QuickAttemptSearchRequest searchRequest)
        {
            var results = await Search(searchRequest);
            return results;
        }
    }
}
