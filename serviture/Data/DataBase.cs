﻿namespace Serviture.Data
{
    using Dapper;
    using System.Collections.Generic;
    using Microsoft.Extensions.Configuration;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Linq;
    using Serviture.Models;
    using System.Data;

    public class DataBase<T, X>
    {
        public ILogger<T> Log { get; }
        public IConfiguration Configuration { get; }
        public string TableName { get; set; }
        public string OrderBy { get; set; }
        public List<string> Columns { get; set; }
        public List<string> SearchColumns { get; set; }
        public List<string> SearchTermColumns { get; set; }

        public string SearchSelectString { get; set; }

        public DataBase(IConfiguration configuration, ILogger<T> log)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            Log = log ?? throw new ArgumentNullException(nameof(log));

            OrderBy = DataConstants.OrderByDefault;
        }

        public async Task<int> Create(T item)
        {
            try
            {
                var colNames = string.Empty;
                var colParams = string.Empty;

                foreach (var col in Columns)
                {
                    var comma = Columns.IndexOf(col) == Columns.Count - 1 ? string.Empty : ",";
                    colNames += $"{col}{comma}";
                    colParams += $"@{col}{comma}";
                }

                string queryString = $"INSERT INTO {TableName} ({colNames}) OUTPUT INSERTED.Id VALUES ({colParams})";

                var parameters = GetParameters(item);

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var newId = await connection.ExecuteScalarAsync<int>(queryString, parameters);
                    return newId;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return 0;
            }
        }

        public async Task<T> Get(int id)
        {
            try
            {
                string queryString = $"SELECT * FROM {TableName} WHERE Id = @Id ";

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var document = await connection.QueryFirstAsync<T>(queryString, new { Id = id });
                    return document;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return default;
            }
        }

        public async Task<List<T>> Get()
        {
            try
            {
                string queryString = $"SELECT * FROM {TableName} WHERE IsDeleted = 0 ORDER BY {OrderBy}";

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var queryResponse = await connection.QueryAsync<T>(queryString);
                    return queryResponse.ToList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return null;
            }
        }

        public async Task<List<T>> GetAll()
        {
            try
            {
                string queryString = $"SELECT * FROM {TableName} ORDER BY {OrderBy}";

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var queryResponse = await connection.QueryAsync<T>(queryString);
                    return queryResponse.ToList();
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return null;
            }
        }

        public async Task<int> Update(int id, T item)
        {
            try
            {
                var updateColsAndParams = string.Empty;

                foreach (var col in Columns)
                {
                    if (!(col.Equals("CreatedOn") || col.Equals("CreatedBy")))
                    {
                        var comma = Columns.IndexOf(col) == Columns.Count - 1 ? string.Empty : ",";
                        updateColsAndParams += $" {col} = @{col}{comma} ";
                    }                        
                }

                string queryString = $"UPDATE {TableName} SET {updateColsAndParams} WHERE Id = @Id";

                var parameters = GetUpdateParameters(item);
                parameters.Add("@Id", id);

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var affectedRows = await connection.ExecuteAsync(queryString, parameters);
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return 0;
            }
        }

        public async Task<int> Delete(int id)
        {
            try
            {
                string queryString = $"UPDATE {TableName} SET IsDeleted = 1 WHERE Id = @Id";

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var affectedRows = await connection.ExecuteAsync(queryString, new { Id = id });
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return 0;
            }
        }

        public async Task<int> HardDelete(int id)
        {
            try
            {
                string queryString = $"DELETE FROM {TableName} WHERE Id = @Id";

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var affectedRows = await connection.ExecuteAsync(queryString, new { Id = id });
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return 0;
            }
        }

        public async Task<SearchResponse<T>> Search(X searchItem)
        {
            try
            {
                var searchTerm = (string)typeof(X).GetProperty("SearchTerm").GetValue(searchItem);

                if (!string.IsNullOrWhiteSpace(searchTerm))
                {
                    return await SearchTable(searchItem);
                }

                var searchDate = (string)typeof(X).GetProperty("SearchDate").GetValue(searchItem);
                var daysBack = 0;

                if (typeof(X).GetProperty("DaysBack") != null && typeof(X).GetProperty("DaysBack").GetValue(searchItem) != null)
                {
                    daysBack = (int)typeof(X).GetProperty("DaysBack").GetValue(searchItem);
                }

                string queryString = $"SELECT * FROM {TableName} WHERE 1 = 1 ";
                string countQueryString = $"SELECT COUNT(*) FROM {TableName} WHERE 1 = 1 ";

                var parameters = new DynamicParameters();
                var searchResponse = new SearchResponse<T>();

                var conditionsString = string.Empty;

                foreach (var col in SearchColumns)
                {
                    var cond = "=";
                    var colObj = typeof(X).GetProperty(col).GetValue(searchItem);
                    var colType = typeof(X).GetProperty(col).PropertyType;

                    if (col.Equals("Id") && colObj != null)
                    {
                        cond = "IN";

                        conditionsString += $" AND {col} {cond} @{col}";
                        parameters.Add($"@{col}", colObj);
                    }
                    else if (col.Equals("InvoiceNumber") && colObj != null)
                    {
                        cond = "IN";

                        var stringInvoice = (string)colObj;
                        var intListInvocie = stringInvoice.Split(',').Select(Int32.Parse).ToList();

                        conditionsString += $" AND {col} {cond} @{col}";
                        parameters.Add($"@{col}", intListInvocie);
                    }
                    else if(col.Equals("CreatedOn") && (!string.IsNullOrWhiteSpace(searchDate) || daysBack > 0))
                    {
                        var tSearchDate = DateTime.Today.AddDays(-daysBack).ToString();

                        if (!string.IsNullOrWhiteSpace(searchDate))
                        {
                            tSearchDate = searchDate;
                        }
                        
                        if (daysBack > 0)
                        {
                            cond = ">=";
                        }

                        conditionsString += $" AND DATEDIFF(day, {col}, @{col}) {cond} 0 ";
                        parameters.Add($"@{col}", tSearchDate);
                    }
                    else if (colType == typeof(string) && !string.IsNullOrWhiteSpace((string)colObj))
                    {
                        cond = "like";
                        colObj += "%";

                        conditionsString += $" AND {col} {cond} @{col}";
                        parameters.Add($"@{col}", colObj);
                    }
                    else if (colType == typeof(Int32) && (int)colObj != 0)
                    {
                        conditionsString += $" AND {col} {cond} @{col}";
                        parameters.Add($"@{col}", colObj);
                    }
                    else if (colType != typeof(Int32) && colType != typeof(string) && colType != typeof(DateTime) && colType != typeof(DateTimeOffset) && colObj != null)
                    {
                        conditionsString += $" AND {col} {cond} @{col}";
                        parameters.Add($"@{col}", colObj);
                    }

                }

                queryString += conditionsString;
                countQueryString += conditionsString;

                var pageSize = (int)typeof(X).GetProperty("PageSize").GetValue(searchItem);
                var pageIndex = (int)typeof(X).GetProperty("PageIndex").GetValue(searchItem);
                var orderBy = (string)typeof(X).GetProperty("OrderBy").GetValue(searchItem);

                if (string.IsNullOrEmpty(orderBy))
                {
                    orderBy = DataConstants.OrderByDefault;
                }

                queryString += $" ORDER BY {orderBy} ";

                if (pageSize >= 0) {
                    queryString += $" OFFSET {pageSize * pageIndex} ROWS FETCH NEXT {pageSize} ROWS ONLY ";
                }

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var items = await connection.QueryAsync<T>(queryString, parameters);
                    searchResponse.Results = items;
                    var total = await connection.ExecuteScalarAsync<int>(countQueryString, parameters);
                    searchResponse.TotalResults = total;
                    searchResponse.HasMoreResults = total > (pageIndex + 1) * pageSize;
                }

                return searchResponse;
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// For searching a populated table.
        /// Used in Case, Serve, and Affidavit.
        /// </summary>
        /// <param name="searchItem"></param>
        /// <returns></returns>
        public async Task<SearchResponse<T>> SearchTable(X searchItem)
        {
            try
            {
                var parameters = new DynamicParameters();
                var searchResponse = new SearchResponse<T>();

                var searchDate = (string)typeof(X).GetProperty("SearchDate").GetValue(searchItem);
                var searchTerm = (string)typeof(X).GetProperty("SearchTerm").GetValue(searchItem);
                var pageSize = (int)typeof(X).GetProperty("PageSize").GetValue(searchItem);
                var pageIndex = (int)typeof(X).GetProperty("PageIndex").GetValue(searchItem);
                var isDeleted =  (bool?)typeof(X).GetProperty("IsDeleted").GetValue(searchItem);

                if (typeof(X).GetProperty("SearchUser").GetValue(searchItem) != null)
                {
                    var searchUser = (int)typeof(X).GetProperty("SearchUser").GetValue(searchItem);
                    if (searchUser > 0 && !TableName.Equals("[case]"))
                    {
                        parameters.Add("@SearchUser", searchUser);
                    }
                } 
                else if (typeof(X).GetProperty("ServerId") != null && typeof(X).GetProperty("ServerId").GetValue(searchItem) != null)
                {
                    var searchUser = (int)typeof(X).GetProperty("ServerId").GetValue(searchItem);
                    if (searchUser > 0 && !TableName.Equals("[case]"))
                    {
                        parameters.Add("@SearchUser", searchUser);
                    }
                }

                if (!string.IsNullOrWhiteSpace(searchDate))
                {
                    parameters.Add("@SearchDate", searchDate);
                }
                parameters.Add("@SearchTerm", searchTerm);
                parameters.Add("@PageSize", pageSize);
                parameters.Add("@PageIndex", pageIndex);
                parameters.Add("@IsDeleted", isDeleted);

                // For ClientPortal Search
                if (typeof(X).GetProperty("ClientId") != null)
                {
                    var clientId = (int?)typeof(X).GetProperty("ClientId").GetValue(searchItem);
                    if (clientId != null && clientId > 0)
                    {
                        parameters.Add("@ClientId", clientId);
                    }
                }

                parameters.Add( name: "@RetVal", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var table = TableName.Equals("[case]") ? "case" : TableName;
                    var results = await connection.QueryAsync<T>($"[dbo].[search{table}]", parameters, commandType: CommandType.StoredProcedure);
                    searchResponse.Results = results;
                    var a = await connection.ExecuteAsync($"[dbo].[search{table}]", parameters, commandType: CommandType.StoredProcedure);
                    var total = parameters.Get<int>("@RetVal");

                    searchResponse.TotalResults = total;
                    searchResponse.HasMoreResults = total > (pageIndex + 1) * pageSize;
                }

                return searchResponse;
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return null;
            }
        }

        public async Task<T> GetRandomRecord(string constraints)
        {
            try
            {
                var andClause = string.Empty;
                if (!string.IsNullOrEmpty(constraints))
                {
                    andClause = $" AND {constraints}";
                }
                string queryString = $"SELECT TOP 1 * FROM {TableName} WHERE IsDeleted = 0 {andClause} ORDER BY NEWID()";

                using (var connection = new SqlConnection(Configuration["ConnectionStrings:SQL"]))
                {
                    var queryResponse = await connection.QueryFirstAsync<T>(queryString);
                    return queryResponse;
                }
            }
            catch (Exception ex)
            {
                Log.LogError(ex.Message);
                return default;
            }
        }

        private DynamicParameters GetParameters(T item)
        {
            var parameters = new DynamicParameters();

            foreach (var col in Columns)
            {
                parameters.Add($"@{col}", typeof(T).GetProperty(col).GetValue(item));
            }

            return parameters;
        }

        private DynamicParameters GetUpdateParameters(T item)
        {
            var parameters = new DynamicParameters();

            foreach (var col in Columns)
            {
                if(!(col.Equals("CreatedOn") || col.Equals("CreatedBy")))
                {
                    parameters.Add($"@{col}", typeof(T).GetProperty(col).GetValue(item));
                }
            }

            return parameters;
        }
    }
}
