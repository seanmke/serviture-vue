namespace Serviture
{
    using DinkToPdf;
    using DinkToPdf.Contracts;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.IdentityModel.Tokens;
    using Serilog;
    using Serviture.Data;
    using Serviture.Helpers;
    using Serviture.Services;
    using System;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Loader;
    using System.Text;
    using VueCliMiddleware;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            try
            {
                CustomAssemblyLoadContext context = new CustomAssemblyLoadContext();
                var architectureFolder = (IntPtr.Size == 8) ? "64" : "32";
                context.LoadUnmanagedLibrary(Path.Combine(Directory.GetCurrentDirectory(), $"dinktopdf\\{architectureFolder}\\libwkhtmltox.dll"));
            } catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            

            services.AddCors();

            services.AddControllersWithViews();

            services.AddRazorPages();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["AppSettings:Secret"])),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddTransient<IAddressData, AddressData>();
            services.AddTransient<IAffidavitData, AffidavitData>();
            services.AddTransient<IAttemptData, AttemptData>();
            services.AddTransient<ICaseData, CaseData>();
            services.AddTransient<IClientData, ClientData>();
            services.AddTransient<IClientPortalData, ClientPortalData>();
            services.AddTransient<IContractorUserData, ContractorUserData>();
            services.AddTransient<ICSAHelper, CSAHelper>();
            services.AddTransient<IDocumentData, DocumentData>();
            services.AddTransient<IDocumentByUserData, DocumentByUserData>();
            services.AddTransient<IDocumentByServeData, DocumentByServeData>();
            services.AddTransient<IFilesData, FilesData>();
            services.AddTransient<IFieldData, FieldData>();
            services.AddTransient<IFieldHelper, FieldHelper>();
            services.AddTransient<IPasswordHasher, PasswordHasher>();
            services.AddTransient<IQuickAttemptData, QuickAttemptData>();
            services.AddTransient<IRazorToStringRenderer, RazorToStringRenderer>();
            services.AddTransient<IReasonData, ReasonData>();
            services.AddTransient<IReasonByAffidavitData, ReasonByAffidavitData>();
            services.AddTransient<IReportData, ReportData>();
            services.AddTransient<IServeData, ServeData>();
            services.AddTransient<IUserData, UserData>();
            services.AddTransient<IUserSettingsData, UserSettingsData>();

            services.AddTransient<IBlobService, BlobService>();

            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "My API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }            

            app.UseSerilogRequestLogging();
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            //Sean added.  HTTP PUT/OPTIONS wasnt working
            app.UseCors(builder =>
            {
                builder.WithOrigins("http://localhost:8080",
                    "http://localhost:8081",
                    "http://localhost:8082",
                    "http://localhost:8083", 
                    "http://localhost:51333", 
                    "https://localhost:51333",
                    "http://dev-servitureapp.azurewebsites.net/",
                    "https://dev-servitureapp.azurewebsites.net/",
                    "https://servitureapp.com",
                    "http://servitureapp.com",
                    "https://serviture-app.com",
                    "http://serviture-app.com")
                .AllowCredentials()
                .AllowAnyHeader()
                .AllowAnyMethod();
            });

            app.UseAuthorization();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseVueCli(npmScript: "serve", port: 8080);
                }
            });
        }
    }

    public class CustomAssemblyLoadContext : AssemblyLoadContext
    {
        public IntPtr LoadUnmanagedLibrary(string absolutePath)
        {
            return LoadUnmanagedDll(absolutePath);
        }
        protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
        {
            return LoadUnmanagedDllFromPath(unmanagedDllName);
        }
        protected override Assembly Load(AssemblyName assemblyName)
        {
            throw new NotImplementedException();
        }
    }
}
