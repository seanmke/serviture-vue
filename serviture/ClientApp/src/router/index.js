import Vue from 'vue'
import VueRouter from 'vue-router'
import { routes } from './router'
import store from '../store'
import { AUTH_LOGOUT } from "../store/actions/auth";
import userService from "../services/user"

Vue.use(VueRouter);

let router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    var expDate = parseInt(store.getters.getExpirationDate);

    if (to.name !== 'login' && Date.now() > expDate) {
        store.dispatch(AUTH_LOGOUT).then(() => {
            next({ name: 'login' });
        });
    }
    else if (to.name !== 'login' && !store.getters.isAuthenticated) {
        next({ name: 'login' });
    }
    else {
        userService.updateLastAction(store.getters.getUserId);
        next();
    }
});

export default router