﻿import Home from '../components/home'
import Files from '../components/files/files'
import Camera from '../components/files/camera'
import ServeFiles from '../components/files/serve-files'
import Case from '../components/case/case'
import Serve from '../components/serve/serve'
import QuickAttempt from '../components/serve/quick-attempts'
import Affidavit from '../components/affidavit/affidavit'
import AffidavitPdf from '../components/affidavit/affidavitPdf'
import Editor from '../components/admin/editors'
import Billing from '../components/billing/billing-shell'
import Reports from '../components/reports/reports'
import UserSettings from '../components/settings/userSettings'
import UserManagement from '../components/users/user-management'
import UserAddEdit from '../components/users/user-add-edit'
import Login from '../components/login'
import Logout from '../components/logout'

export const routes = [
    { name: 'home', path: '/', component: Home, display: 'Home', icon: 'mdi-home', meta: { hidden: false, admin: false, client: true }  },
    { name: 'files', path: '/files/:id', component: Files, display: 'Files', icon: 'mdi-file-upload', meta: { hidden: true, admin: false, client: false } },
    { name: 'serve-files', path: '/serve-files/:id', component: ServeFiles, display: 'Serve Files', meta: { hidden: true, admin: false, client: false } },
    { name: 'camera', path: '/camera/:id', component: Camera, display: 'Camera', icon: 'mdi-camera', meta: { hidden: true, admin: false, client: false }  },
    { name: 'case', path: '/case/:id', component: Case, display: 'Create Case', icon: 'mdi-briefcase', meta: { hidden: false, admin: false, client: false } },
    { name: 'serve', path: '/serve/:id', component: Serve, display: 'Serve', icon: 'mdi-email-send-outline', meta: { hidden: true, admin: false, client: false } },
    { name: 'quickattempt', path: '/quickattempt/:id', component: QuickAttempt, display: 'Quick Attempt', meta: { hidden: true, admin: false, client: false } },
    { name: 'affidavit', path: '/affidavit/:id', component: Affidavit, display: 'Affidavit', icon: 'mdi-alpha-a-circle', meta: { hidden: true, admin: false, client: false } },
    { name: 'affidavitPdf', path: '/affidavitPdf/:id', component: AffidavitPdf, meta: { hidden: true, admin: false, client: false } },
    { name: 'editor', path: '/editor', component: Editor, display: 'Editor', icon: 'mdi-square-edit-outline', meta: { hidden: false, admin: true, client: false } },
    { name: 'billing', path: '/billing', component: Billing, display: 'Billing', icon: 'mdi-cash-multiple', meta: { hidden: false, admin: true, client: false } },
    { name: 'reports', path: '/reports', component: Reports, display: 'Reports', icon: 'mdi-file-chart-outline', meta: { hidden: false, admin: true, client: false } },
    { name: 'usersettings', path: '/usersettings', component: UserSettings, display: 'User Settings', icon: 'mdi-account-details', meta: { hidden: false, admin: false, client: false } },
    { name: 'user-management', path: '/user-management', component: UserManagement, display: 'Users', icon: 'mdi-account-group', meta: { hidden: false, admin: true, client: false } },
    { name: 'user-add-edit', path: '/user-add-edit/:userId', component: UserAddEdit, display: 'Users', meta: { hidden: true, admin: false, client: false } },
    { name: "logout", path: "/logout", component: Logout, display: 'Logout', icon: 'mdi-logout', meta: { hidden: false, admin: false, client: true } },
    { name: "login", path: "/login", component: Login, meta: { hidden: true, admin: false, client: true } },

    //Need to include the reports twice since the actual routing happens here?
    { name: 'serve-summary', path: '/reports/serve-summary', component: ServeSummary, display: 'Serve Summary Report', meta: { hidden: true, admin: false, client: true } }
]

import ServeSummary from '../components/reports/serve-summary'

export const reports = [
    { name: 'serve-summary', path: '/reports/serve-summary', component: ServeSummary, display: 'Serve Summary Report' },
]