﻿import States from "../constants/states"

export const fieldMixins = {
    methods: {
        newFieldValue(newValue, ft) {
            let field = {
                companyId: parseInt(this.$store.getters.getCompanyId),
                fieldType: ft,
                value: newValue,
                isArchived: false,
                isDeleted: false,
                id: 0
            };
            return field;
        },
        newClientValue(newValue) {
            let client = {
                companyId: parseInt(this.$store.getters.getCompanyId),
                clientName: newValue,
                isDeleted: false,
                showUnsuccessful: false
            };
            return client;
        },
        getCountiesForState(state) {
            switch (state) {
                case 'Alabama': return this.ALABAMA_COUNTIES;
                case 'Alaska': return this.ALASKA_COUNTIES;
                case 'Arizona': return this.ARIZONA_COUNTIES;
                case 'Arkansas': return this.ARKANSAS_COUNTIES;
                case 'California': return this.CALIFORNIA_COUNTIES;
                case 'Colorado': return this.COLORODO_COUNTIES;
                case 'Connecticut': return this.CONNECTICUT_COUNTIES;
                case 'Delaware': return this.DELAWARE_COUNTIES;
                case 'District of Columbia': return this.DISTRICT_OF_COLOMBIA_COUNTIES;
                case 'Florida': return this.FLORIDA_COUNTIES;
                case 'Georgia': return this.GEORGIA_COUNTIES;
                case 'Hawaii': return this.HAWAII_COUNTIES;
                case 'Idaho': return this.IDAHO_COUNTIES;
                case 'Illinois': return this.ILLINOIS_COUNTIES;
                case 'Indiana': return this.INDIANA_COUNTIES;
                case 'Iowa': return this.IOWA_COUNTIES;
                case 'Kansas': return this.KANSAS_COUNTIES;
                case 'Kentucky': return this.KENTUCKY_COUNTIES;
                case 'Louisiana': return this.LOUSIANA_COUNTIES;
                case 'Maine': return this.MAINE_COUNTIES;
                case 'Maryland': return this.MARYLAND_COUNTIES;
                case 'Massachusetts': return this.MASSACHUSETTS_COUNTIES;
                case 'Michigan': return this.MICHIGAN_COUNTIES;
                case 'Minnesota': return this.MINNESOTA_COUNTIES;
                case 'Mississippi': return this.MISSISSIPPI_COUNTIES;
                case 'Missouri': return this.MISSOURI_COUNTIES;
                case 'Montana': return this.MONTANA_COUNTIES;
                case 'Nebraska': return this.NEBRASKA_COUNTIES;
                case 'Nevada': return this.NEVADA_COUNTIES;
                case 'New Hampshire': return this.NEW_HAMPSIRE_COUNTIES;
                case 'New Jersey': return this.NEW_JERSEY_COUNTIES;
                case 'New Mexico': return this.NEW_MEXICO_COUNTIES;
                case 'New York': return this.NEW_YORK_COUNTIES;
                case 'North Carolina': return this.NORTH_CAROLINA_COUNTIES;
                case 'North Dakota': return this.NORTH_DAKOTA_COUNTIES;
                case 'Ohio': return this.OHIO_COUNTIES;
                case 'Oklahoma': return this.OKLAHOMA_COUNTIES;
                case 'Oregon': return this.OREGON_COUNTIES;
                case 'Pennsylvania': return this.PENNSYLVANIA_COUNTIES;
                case 'Rhode Island': return this.RHODE_ISLAND_COUNTIES;
                case 'South Carolina': return this.SOUTH_CAROLINA_COUNTIES;
                case 'South Dakota': return this.SOUTH_DAKOTA_COUNTIES;
                case 'Tennessee': return this.TENNESSE_COUNTIES;
                case 'Texas': return this.TEXAS_COUNTIES;
                case 'Utah': return this.UTAH_COUNTIES;
                case 'Vermont': return this.VERMONT_COUNTIES;
                case 'Virginia': return this.VIRGINA_COUNTIES;
                case 'Washington': return this.WASHINGTON_COUNTIES;
                case 'West Virginia': return this.WEST_VIRGINA_COUNTIES;
                case 'Wisconsin': return this.WISCONSIN_COUNTIES;
                case 'Wyoming': return this.WYOMING_COUNTIES;
            }
        }
    },
    data() {
        return {
            ALABAMA_COUNTIES: States.ALABAMA_COUNTIES,
            ALASKA_COUNTIES: States.ALASKA_COUNTIES,
            ARIZONA_COUNTIES: States.ARIZONA_COUNTIES,
            ARKANSAS_COUNTIES: States.ARKANSAS_COUNTIES,
            CALIFORNIA_COUNTIES: States.CALIFORNIA_COUNTIES,
            COLORODO_COUNTIES: States.COLORODO_COUNTIES,
            CONNECTICUT_COUNTIES: States.CONNECTICUT_COUNTIES,
            DELAWARE_COUNTIES: States.DELAWARE_COUNTIES,
            DISTRICT_OF_COLOMBIA_COUNTIES: States.DISTRICT_OF_COLOMBIA_COUNTIES,
            FLORIDA_COUNTIES: States.FLORIDA_COUNTIES,
            GEORGIA_COUNTIES: States.GEORGIA_COUNTIES,
            HAWAII_COUNTIES: States.HAWAII_COUNTIES,
            IDAHO_COUNTIES: States.IDAHO_COUNTIES,
            ILLINOIS_COUNTIES: States.ILLINOIS_COUNTIES,
            INDIANA_COUNTIES: States.INDIANA_COUNTIES,
            IOWA_COUNTIES: States.IOWA_COUNTIES,
            KANSAS_COUNTIES: States.KANSAS_COUNTIES,
            KENTUCKY_COUNTIES: States.KENTUCKY_COUNTIES,
            LOUSIANA_COUNTIES: States.LOUSIANA_COUNTIES,
            MAINE_COUNTIES: States.MAINE_COUNTIES,
            MARYLAND_COUNTIES: States.MARYLAND_COUNTIES,
            MASSACHUSETTS_COUNTIES: States.MASSACHUSETTS_COUNTIES,
            MICHIGAN_COUNTIES: States.MICHIGAN_COUNTIES,
            MINNESOTA_COUNTIES: States.MINNESOTA_COUNTIES,
            MISSISSIPPI_COUNTIES: States.MISSISSIPPI_COUNTIES,
            MISSOURI_COUNTIES: States.MISSOURI_COUNTIES,
            MONTANA_COUNTIES: States.MONTANA_COUNTIES,
            NEBRASKA_COUNTIES: States.NEBRASKA_COUNTIES,
            NEVADA_COUNTIES: States.NEVADA_COUNTIES,
            NEW_HAMPSIRE_COUNTIES: States.NEW_HAMPSIRE_COUNTIES,
            NEW_JERSEY_COUNTIES: States.NEW_JERSEY_COUNTIES,
            NEW_MEXICO_COUNTIES: States.NEW_MEXICO_COUNTIES,
            NEW_YORK_COUNTIES: States.NEW_YORK_COUNTIES,
            NORTH_CAROLINA_COUNTIES: States.NORTH_CAROLINA_COUNTIES,
            NORTH_DAKOTA_COUNTIES: States.NORTH_DAKOTA_COUNTIES,
            OHIO_COUNTIES: States.OHIO_COUNTIES,
            OKLAHOMA_COUNTIES: States.OKLAHOMA_COUNTIES,
            OREGON_COUNTIES: States.OREGON_COUNTIES,
            PENNSYLVANIA_COUNTIES: States.PENNSYLVANIA_COUNTIES,
            RHODE_ISLAND_COUNTIES: States.RHODE_ISLAND_COUNTIES,
            SOUTH_CAROLINA_COUNTIES: States.SOUTH_CAROLINA_COUNTIES,
            SOUTH_DAKOTA_COUNTIES: States.SOUTH_DAKOTA_COUNTIES,
            TENNESSE_COUNTIES: States.TENNESSE_COUNTIES,
            TEXAS_COUNTIES: States.TEXAS_COUNTIES,
            UTAH_COUNTIES: States.UTAH_COUNTIES,
            VERMONT_COUNTIES: States.VERMONT_COUNTIES,
            VIRGINA_COUNTIES: States.VIRGINA_COUNTIES,
            WASHINGTON_COUNTIES: States.WASHINGTON_COUNTIES,
            WEST_VIRGINA_COUNTIES: States.WEST_VIRGINA_COUNTIES,
            WISCONSIN_COUNTIES: States.WISCONSIN_COUNTIES,
            WYOMING_COUNTIES: States.WYOMING_COUNTIES
        }
    },
}