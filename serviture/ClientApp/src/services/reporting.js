﻿import httpClient from 'axios';

export default {
    async getAffidavitPdf(affidavitId, isPrintVersion) {
        try {
            let response = await httpClient.get('affidavit/report/' + affidavitId + '?isPrintVersion=' + isPrintVersion, { responseType: 'arraybuffer' });
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async getClientCopyPdf(affidavitId) {
        try {
            let response = await httpClient.get('affidavit/clientcopy/' + affidavitId, { responseType: 'arraybuffer' });
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async getAttemptReport(serveId) {
        try {
            let response = await httpClient.get('report/attemptreport/' + serveId, { responseType: 'arraybuffer' });
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async runBillingReport(searchRequest) {
        try {
            let response = await httpClient.post('report/billingreport/', searchRequest);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async runServeAttemptSummaryReport(searchRequest) {
        try {
            let response = await httpClient.post('report/serveattemptsummary/', searchRequest);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    }
}