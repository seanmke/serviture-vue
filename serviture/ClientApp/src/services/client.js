﻿import httpClient from 'axios'

export default {
    createClient() {

    },
    async getClient(id) {
        try {
            let response = await httpClient.get('Client/' + id);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async searchClients(searchRequest) {
        try {
            let response = await httpClient.post('Client/search', searchRequest);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    }
}