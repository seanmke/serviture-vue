﻿const ADDRESS = "Address"
const ADDRESS_BY_SERVE = "AddressByServe"
const AFFIDAVIT = "Affidavit"
const ATTEMPT = "Attempt"
const CASE = "Case"
const CLIENT = "Client"
const CONTACT = "Contact"
const CONTRACTOR_USER = "ContractorUser"
const DOCUMENT = "Document"
const DOCUMENT_BY_USER = "DocumentByUser"
const FIELD = "Field"
const QUICKATTEMPT = "QuickAttempt"
const REASON = "Reason"
const SERVEATTEMPT = "ServeAttempt"
const SERVE = "Serve"
const USER = "User"
const USER_SETTINGS = "UserSettings"

export default {
    ADDRESS: ADDRESS,
    ADDRESS_BY_SERVE: ADDRESS_BY_SERVE,
    AFFIDAVIT: AFFIDAVIT,
    ATTEMPT: ATTEMPT,
    CASE: CASE,
    CLIENT: CLIENT,
    CONTACT: CONTACT,
    CONTRACTOR_USER: CONTRACTOR_USER,
    DOCUMENT: DOCUMENT,
    DOCUMENT_BY_USER: DOCUMENT_BY_USER,
    FIELD: FIELD,
    QUICKATTEMPT: QUICKATTEMPT,
    REASON: REASON,
    SERVEATTEMPT: SERVEATTEMPT,
    SERVE: SERVE,
    USER: USER,
    USER_SETTINGS: USER_SETTINGS
}