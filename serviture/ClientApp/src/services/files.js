﻿import httpClient from 'axios'

export default {
    async uploadFiles(serveId, uploadedFiles) {
        try {
            let config = {
                header: {
                    'Content-Type': 'multipart/form-data'
                }
            }

            let response = await httpClient.post('Files/upload/' + serveId, uploadedFiles, config);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async uploadPicture(serveId, formData) {
        try {
            let config = {
                header: {
                    'Content-Type': 'multipart/form-data'
                }
            }

            let response = await httpClient.post('Files/picture/' + serveId, formData, config);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async search(searchRequest) {
        try {
            let response = await httpClient.post('Files/search', searchRequest);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async searchblob(searchRequest) {
        try {
            let response = await httpClient.post('Files/searchblobs', searchRequest);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async delete(id) {
        try {
            let response = await httpClient.delete('Files/' + id);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
}