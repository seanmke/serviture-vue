﻿import httpClient from 'axios'
import logger from "../services/logger"

export default {
    createUser() {

    },
    async authenticate(authenticateModel) {
        try {
            let response = await httpClient.post('User/authenticate', authenticateModel);
            return response.data;
        } catch (ex) {
            console.log(ex.message);
            throw ex;
        }
    },
    async getUser(id) {
        try {
            let response = await httpClient.get('User/' + id);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async searchUsers(searchRequest) {
        try {
            let response = await httpClient.post('User/search', searchRequest);
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    async updateLastAction(id) {
        try {
            let response = await httpClient.put('user/lastaction/' + id);
            return response.data;
        } catch (ex) {
            console.log(ex);
            logger.log(ex);
            if (ex.message) {
                console.log(ex.message);
                logger.log(ex.message);
            }
        }
    },
}