﻿import httpClient from 'axios'
import logger from "../services/logger"

export default {
    async create(controller, createItem) {
        try {
            let response = await httpClient.post(controller + '/', createItem);
            return response.data;
        } catch (ex) {
            console.log(ex);
            logger.log(ex);
            if (ex.message) {
                console.log(ex.message);
                logger.log(ex.message);
            }
        }
    },
    async createMapped(controller, createItem) {
        try {
            let response = await httpClient.post(controller + '/mapped/', createItem);
            return response.data;
        } catch (ex) {
            console.log(ex);
            logger.log(ex);
            if (ex.message) {
                console.log(ex.message);
                logger.log(ex.message);
            }
        }
    },
    async get(controller, id) {
        try {
            let response = await httpClient.get(controller + '/' + id);
            return response.data;
        } catch (ex) {
            console.log(ex);
            if (ex.message) {
                console.log(ex.message);
            }
        }
    },
    async getMapped(controller, id) {
        try {
            let response = await httpClient.get(controller + '/mapped/' + id);
            return response.data;
        } catch (ex) {
            console.log(ex);
            if (ex.message) {
                console.log(ex.message);
            }
        }
    },
    async getAll(controller) {
        try {
            let response = await httpClient.get(controller + '/');
            return response.data;
        } catch (ex) {
            console.log(ex);
            if (ex.message) {
                console.log(ex.message);
            }
        }
    },
    async update(controller, id, updateItem) {
        try {
            let response = await httpClient.put(controller + '/' + id, updateItem);
            return response.data;
        } catch (ex) {
            console.log(ex);
            logger.log(ex);
            if (ex.message) {
                console.log(ex.message);
                logger.log(ex.message);
            }
        }
    },
    async updateMapped(controller, id, updateItem) {
        try {
            let response = await httpClient.put(controller + '/mapped/' + id, updateItem);
            console.log(response);
            return response.data;
        } catch (ex) {
            console.log(ex);
            logger.log(ex);
            if (ex.message) {
                console.log(ex.message);
                logger.log(ex.message);
            }
        }
    },
    async delete(controller, id) {
        try {
            let response = await httpClient.post(controller + '/delete', { Id: id });
            return response.data;
        } catch (ex) {
            console.log(ex);
            if (ex.message) {
                console.log(ex.message);
            }
        }
    },
    async hardDelete(controller, id) {
        try {
            let response = await httpClient.delete(controller + '/' + id);
            return response.data;
        } catch (ex) {
            console.log(ex);
            if (ex.message) {
                console.log(ex.message);
            }
        }
    },
    async search(controller, searchRequest) {
        try {
            let response = await httpClient.post(controller + '/search', searchRequest);
            return response.data;
        } catch (ex) {
            console.log(ex);
            logger.log(ex);
            if (ex.message) {
                console.log(ex.message);
                logger.log(ex.message);
            }
        }
    },
    //User in the By Controllers
    async upsert(controller, items) {
        try {
            let response = await httpClient.post(controller + '/upsert', items);
            return response.data;
        } catch (ex) {
            console.log(ex);
            logger.log(ex);
            if (ex.message) {
                console.log(ex.message);
                logger.log(ex.message);
            }
        }
    },
    async upsertById(controller, id, items) {
        try {
            let response = await httpClient.post(controller + '/upsert/' + id, items);
            return response.data;
        } catch (ex) {
            console.log(ex);
            logger.log(ex);
            if (ex.message) {
                console.log(ex.message);
                logger.log(ex.message);
            }
        }
    },
    //User in the ByUser Controllers
    async getByUser(controller, userId) {
        try {
            let response = await httpClient.get(controller + '/userid/' + userId);
            return response.data;
        } catch (ex) {
            console.log(ex);
            if (ex.message) {
                console.log(ex.message);
            }
        }
    }
}