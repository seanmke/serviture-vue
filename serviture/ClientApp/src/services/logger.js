﻿import httpClient from 'axios'

export default {
    log(message) {
        try {
            httpClient.post('Log', { Message: message });
        } catch (ex) {
            console.log(ex);
        }
    }
}