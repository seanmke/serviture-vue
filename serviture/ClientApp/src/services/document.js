﻿import httpClient from 'axios'

export default {
    createField() {

    },
    async get() {
        try {
            let response = await httpClient.get('Document/');
            return response.data;
        } catch (ex) {
            console.log(ex);
        }
    },
    update(document) {
        try {
            return httpClient.put('Document/' + document.id, document);
        } catch (ex) {
            console.log(ex);
        }
    },
    delete(document) {
        try {
            return httpClient.post('Document/delete/', document);
        } catch (ex) {
            console.log(ex);
        }
    }

}