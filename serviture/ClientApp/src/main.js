import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import JsonCSV from 'vue-json-csv'

Vue.config.productionTip = false

Vue.component('downloadCsv', JsonCSV)

Vue.prototype.$http = axios
axios.defaults.baseURL = process.env.VUE_APP_BASEURL

new Vue({
    router,
    axios,
    store,
    vuetify,
    JsonCSV,
    render: h => h(App)
}).$mount('#app')
