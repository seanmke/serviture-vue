﻿import { COPY_SERVE } from "../actions/shared";

const state = {
    copyServeId: localStorage.getItem("copy-serve-id") || 0
};

const getters = {
    getCopyServeId: state => state.copyServeId
};

const actions = {
    [COPY_SERVE]: (context, id) => {
        context.commit(COPY_SERVE, id);
    }
};

const mutations = {
    [COPY_SERVE]: (state, id) => {
        state.copyServeId = id;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};