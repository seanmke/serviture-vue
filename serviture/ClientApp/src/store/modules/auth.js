﻿import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT } from "../actions/auth";
import { USER_REQUEST } from "../actions/user";
import UserService from "../../services/user"

//inside module actions, context.state will expose the local state, and root state will be exposed as context.rootState
const state = {
    token: localStorage.getItem("user-token") || "",
    status: "",
    expDate: localStorage.getItem("login-expiration") || "",
    hasLoadedOnce: false
};

//Getters will receive the state as their 1st argument:
//Getters will also receive other getters as the 2nd argument:
//Also, inside module getters, the root state will be exposed as their 3rd argument:
const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
    getExpirationDate: state => state.expDate
};

//To handle asynchronous operations, let's introduce Actions.
//Actions are triggered with the store.dispatch method:

const actions = {
    [AUTH_REQUEST]: ({ commit, dispatch }, user) => {
        return new Promise((resolve, reject) => {
            var expDate = new Date();
            expDate = expDate.setHours(expDate.getHours() + 24);
            //expDate = expDate.setSeconds(expDate.getSeconds() + 24);

            commit(AUTH_REQUEST);
            UserService.authenticate(user)
                .then(resp => {
                    localStorage.setItem("user-token", resp.token);
                    localStorage.setItem('login-expiration', expDate);
                    resp.expDate = expDate;

                    // Here set the header of your ajax library to the token value.
                    // example with axios
                    // axios.defaults.headers.common['Authorization'] = resp.token
                    commit(AUTH_SUCCESS, resp);
                    dispatch(USER_REQUEST, resp);
                    resolve(resp);
                })
                .catch(err => {
                    commit(AUTH_ERROR, err);
                    localStorage.removeItem("user-token");
                    localStorage.removeItem("login-expiration");
                    reject(err);
                });
        });
    },
    [AUTH_LOGOUT]: ({ commit }) => {
        return new Promise(resolve => {
            commit(AUTH_LOGOUT);
            localStorage.removeItem("user-token");
            localStorage.removeItem("login-expiration");
            resolve();
        });
    }
};

//To invoke a mutation handler, you need to call store.commit with its type
//You can pass an additional argument to store.commit, which is called the payload for the mutation

//It is a commonly seen pattern to use constants for mutation types in various Flux implementations. This allows the code to take advantage of tooling like linters,
//and putting all constants in a single file allows your collaborators to get an at - a - glance view of what mutations are possible in the entire application

//mutation handler functions must be synchronous.
const mutations = {
    [AUTH_REQUEST]: state => {
        state.status = "loading";
    },
    //payload should be an object so that it can contain multiple fields
    [AUTH_SUCCESS]: (state, resp) => {
        state.status = "success";
        state.token = resp.token;
        state.expDate = resp.expDate;
        state.hasLoadedOnce = true;
    },
    [AUTH_ERROR]: state => {
        state.status = "error";
        state.hasLoadedOnce = true;
    },
    [AUTH_LOGOUT]: state => {
        state.token = "";
        state.expDate = "";
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};