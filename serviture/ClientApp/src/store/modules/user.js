﻿import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from "../actions/user";
import { AUTH_LOGOUT } from "../actions/auth";
import { routes } from '../../router/router'
//import UserService from "../../services/user"

const state = {
    status: "",
    profile: localStorage.getItem("profile") || {},
    userRoutes: localStorage.getItem("user-routes") || [],
    userId: localStorage.getItem("user-id") || 0,
    isAdmin: localStorage.getItem("admin") == 'true' || false,
    companyId: localStorage.getItem("company-id") || 0,
    userType: localStorage.getItem("user-type") || 0,
    displayName: localStorage.getItem("display-name") || ''
};

const getters = {
    //TODO first two used?
    getProfile: state => state.profile,
    getUserRoutes: state => state.userRoutes,
    isProfileLoaded: state => !!state.profile.name,
    getUserId: state => state.userId,
    getCompanyId: state => state.companyId,
    checkAdmin: state => state.isAdmin,
    getUserType: state => state.userType,
    getDisplayName: state => state.displayName
};

// Instead of mutating the state, actions commit mutations.
// Actions can contain arbitrary asynchronous operations.

// ction handlers receive a context object which exposes the same set of methods/properties on the store instance, 
// so you can call context.commit to commit a mutation, or access the state and getters via context.state and 
// context.getters.  We can even call other actions with context.dispatch.

const actions = {
    //USER_REQUEST is dispatched from AUTH_REQUEST

    // Can also be definted like so
    // In practice, we often use ES2015 argument destructuring (opens new window)
    // to simplify the code a bit(especially when we need to call commit multiple times):
    // [USER_REQUEST]: ({commit}, user) => {

    [USER_REQUEST]: (context, user) => {
        //Commits a mutation
        context.commit(USER_REQUEST);

        var displayName = '';
        if (user.userType === 1) {
            displayName = user.firstName + ' ' + user.lastName;
        } else {
            displayName = user.entityName;
        }

        localStorage.setItem("profile", JSON.stringify(user));
        localStorage.setItem("admin", user.isAdmin);
        localStorage.setItem("user-id", user.id);
        localStorage.setItem("company-id", user.companyId);
        localStorage.setItem("user-type", user.userType);
        localStorage.setItem("display-name", displayName);

        //Duplicated here to keep menu when clearing cache and refreshing
        var newArray = [];
        routes.forEach((route) => {
            var isAdminRoute = route.meta.admin;
            var isHiddenRoute = route.meta.hidden;
            var isClientRoute = route.meta.client;

            var isAdminUser = user.isAdmin;
            console.log('is admin user ' + isAdminUser)
            var isClientUser = user.userType == 2;

            if (!isHiddenRoute && ((isClientRoute && isClientUser) || (isAdminRoute && isAdminUser) || (!isClientUser && !isAdminRoute))) {
                newArray.push(route);
            }
        })

        console.log('nav routes1');
        console.log(newArray);

        localStorage.setItem("user-routes", JSON.stringify(newArray));
        console.log(localStorage)

        context.commit(USER_SUCCESS, user);
        //    })
        //    .catch(() => {
        //        commit(USER_ERROR);
        //        // if resp is unauthorized, logout, to
        //        dispatch(AUTH_LOGOUT);
        //    });
    }
};

// The only way to actually change state in a Vuex store is by committing a mutation.
const mutations = {
    [USER_REQUEST]: state => {
        console.log('we loading');
        state.status = "loading";
    },
    [USER_SUCCESS]: (state, resp) => {
        state.status = "success";
        state.profile = resp;
        state.userId = resp.id;
        state.companyId = resp.companyId;
        state.isAdmin = resp.isAdmin;
        state.userType = resp.userType;

        var displayName = '';
        if (resp.userType === 1) {
            displayName = resp.firstName + ' ' + resp.lastName;
        } else {
            displayName = resp.entityName;
        }

        state.displayName = displayName;

        var newArray = [];
        routes.forEach((route) => {
            var isAdminRoute = route.meta.admin;
            var isHiddenRoute = route.meta.hidden;
            var isClientRoute = route.meta.client;

            var isAdminUser = resp.isAdmin;
            var isClientUser = resp.userType == 2;

            if (!isHiddenRoute && ((isClientRoute && isClientUser) || (isAdminRoute && isAdminUser) || (!isClientUser && !isAdminRoute))) {
                newArray.push(route);
            }
        });

        state.userRoutes = newArray;
    },
    [USER_ERROR]: state => {
        state.status = "error";
    },
    [AUTH_LOGOUT]: state => {
        state.profile = {};
        state.userId = 0;
        state.companyId = 0;
        state.isAdmin = false;
        state.userType = 0;
        state.displayName = '';
        state.userRoutes = [];
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};