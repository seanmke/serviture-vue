﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class FieldTests : TestBase<Field, object, FieldSearchRequest>, IClassFixture<TestFixture>
    {
        public FieldTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Field.Columns.ToList();
            SearchColumns = FieldSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var id = await CreateItem();
            var deleteItem = new Field { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        //[Fact]
        //public async Task TestSearchFieldAsync()
        //{
        //    var id = await CreateItem();
        //    await TestSearchAsync(id);
        //}

        private async Task<int> CreateItem()
        {
            var user = new Field
            {
                Value = "Case Name Test",
                IsArchived = false,
                CompanyId = 1,
                IsDeleted = false,
                FieldType = (int)FieldType.CaseName,
            };

            return await Create(user);
        }
    }
}
