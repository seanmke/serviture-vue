﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class AttemptTests : TestBase<Attempt, AttemptMapped, AttemptSearchRequest>, IClassFixture<TestFixture>
    {
        public AttemptTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Attempt.Columns.ToList();
            SearchColumns = AttemptSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchDocumentAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var item = new Attempt
            {
                ServeId = 1,
                DateServed = DateTime.Now,
                IsSuccess = true,
                OtherInfo = "Some info"
            };

            return await Create(item);
        }
    }
}
