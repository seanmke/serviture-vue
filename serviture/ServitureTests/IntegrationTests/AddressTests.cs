﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class AddressTests : TestBase<Address, object, AddressSearchRequest>, IClassFixture<TestFixture>
    {
        public AddressTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Address.Columns.ToList();
            SearchColumns = AddressSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchAddressAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var address = new Address
            {
                ServeId = 1,
                StreetAddress = "123 Main St Oak Forest IL 60452"
            };

            return await Create(address);
        }
    }
}
