﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class ClientTests : TestBase<Client, object, ClientSearchRequest>, IClassFixture<TestFixture>
    {
        public ClientTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Serviture.Models.Client.Columns.ToList();
            SearchColumns = ClientSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var id = await CreateItem();
            var deleteItem = new Client { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchClientAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var item = new Client
            {
                ClientName = "CodeMke Test",
                CompanyId = 1,
                DefaultFee = 10,
                ShowUnsuccessful = true,
                IsDeleted = false
            };

            return await Create(item);
        }
    }
}
