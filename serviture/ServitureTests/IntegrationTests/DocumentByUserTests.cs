﻿namespace ServitureTests.IntegrationTests
{
    using Newtonsoft.Json;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class DocumentByUsersTests : TestBase<DocumentByUser, object, DocumentByUserSearchRequest>, IClassFixture<TestFixture>
    {
        public DocumentByUsersTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = DocumentByUser.Columns.ToList();
            SearchColumns = DocumentByUserSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestGetByUser()
        {
            var id = await CreateItem();
            var id2 = await CreateItem2();
            var id3 = await CreateItem3();

            var request = $"/api/DocumentByUser/userid/5";

            var response = await Client.GetAsync(request);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var item = JsonConvert.DeserializeObject<List<Document>>(jsonResponse);

            var itemId = item.Select(x => x.Id).FirstOrDefault();
            Assert.Equal(4, itemId);

            var rowsAffected = await HardDelete(id);
            Assert.Equal(1, rowsAffected);

            var rowsAffected2 = await HardDelete(id2);
            Assert.Equal(1, rowsAffected2);

            var rowsAffected3 = await HardDelete(id3);
            Assert.Equal(1, rowsAffected3);
        }

        [Fact]
        public async Task TestUpsert()
        {
            var id = await CreateItem();

            var list = new List<DocumentByUser>() { new DocumentByUser { DocumentId = 3, UserId = 4 } };
            var requestUrl = $"api/DocumentByUser/upsert";

            var stringContent = TestHelpers.GetStringContent(list);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var ids = JsonConvert.DeserializeObject<List<int>>(jsonResponse);
            Assert.True(ids.Count > 0);

            var rowsAffected = await HardDelete(ids.FirstOrDefault());
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchDocumentAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var item = new DocumentByUser
            {
                DocumentId = 4,
                UserId = 4
            };

            return await Create(item);
        }

        private async Task<int> CreateItem2()
        {
            var item = new DocumentByUser
            {
                DocumentId = 47,
                UserId = 4
            };

            return await Create(item);
        }

        private async Task<int> CreateItem3()
        {
            var item = new DocumentByUser
            {
                DocumentId = 4,
                UserId = 5
            };

            return await Create(item);
        }
    }
}
