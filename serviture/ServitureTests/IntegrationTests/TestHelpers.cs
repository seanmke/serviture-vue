﻿namespace ServitureTests.IntegrationTests
{
    using Newtonsoft.Json;
    using Serviture.Models;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Text;

    public static class TestHelpers
    {
        public static StringContent GetStringContent(object content)
        {
            var json = JsonConvert.SerializeObject(content);
            var stringContent = new StringContent(json, Encoding.Default, "application/json");
            return stringContent;
        }

        public static Client GetNewClient()
        {
            var fee = new Random().NextDouble() * 100;
            fee = Math.Round(fee);
            fee = fee / 100;

            var client = new Client()
            {
                CompanyId = 1,
                IsDeleted = false,
                ClientName = GetRandomString(),
                DefaultFee = (decimal)fee,
                ShowUnsuccessful = false
            };

            return client;
        }

        public static Field GetNewField(FieldType fieldType)
        {
            var field = new Field()
            {
                CompanyId = 1,
                FieldType = (int)fieldType,
                IsArchived = false,
                IsDeleted = false,
                Value = GetRandomString()
            };

            return field;
        }

        public static string GetRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", "");
            return path;
        }

        public static Case GetRandomCase()
        {
            var item = new Case
            {
                CaseNameId = 1,
                CaseNumId = 1,
                ClientId = 1,
                ContactId = 1,
                County = "Milwaukee",
                CourtDate = DateTime.Now,
                DefendantId = 1,
                FileNumId = 1,
                OtherLocationId = null,
                State = "Wisconsin",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            return item;
        }        

        public static Serve GetRandomServe(int caseId)
        {
            var item = new Serve()
            {
                CaseId = caseId,
                Charge = 10,
                ContractorUserId = null,
                EntityId = 1,
                NotaryCounty = "Milwaukee",
                NotaryState = "Wisconsin",
                Notes = "Some test notes",
                ServerId = 1,
                ServeStatus = (int)ServeStatus.InProcess,
                SigState = "Wisconsin",
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                IsDeleted = false,
            };

            return item;
        }

        public static ServeMapped GetRandomServeMapped(int caseId)
        {
            var item = new ServeMapped()
            {
                ServeStatus = (int)ServeStatus.InProcess,
                CaseId = caseId,
                Charge = 10,
                ContractorUser = null,
                Entity = GetNewField(FieldType.PersonServed),
                NotaryCounty = "Milwaukee",
                NotaryState = "Wisconsin",
                Notes = TestHelpers.GetRandomString(),
                ProcessServer = new User { Id = 1 },
                SigState = "Wisconsin",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var addresses = new List<Address>();

            for(int i = 0; i < new Random().Next(3) + 1; i++)
            {
                var address = new Address
                {
                    StreetAddress = GetRandomString()
                };

                addresses.Add(address);
            }

            item.Addresses = addresses;

            var attempts = new List<AttemptMapped>();

            for (int i = 0; i < new Random().Next(5) + 1; i++)
            {
                var attempt = new AttemptMapped
                {
                    DateServed = DateTime.Now.ToShortDateString(),
                    IsSuccess = false,
                    OtherInfo = GetNewField(FieldType.AttemptOther)
                };

                attempts.Add(attempt);
            }

            attempts[0].IsSuccess = true;
            item.Attempts = attempts;

            return item;
        }

        public static Affidavit GetRandomAffidavit(int serveId)
        {
            var item = new Affidavit()
            {
                ServeId = serveId,
                AffidavitType = 1,
                CareOfId = 1,
                CompanyId = 1,
                InvoiceNumber = "123",
                IsNotMarried = true,
                IsNotMilitary = false,
                IsPaid = false,
                RelationshipId = 1,
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                IsDeleted = false,
            };

            return item;
        }

        public static AffidavitMapped GetRandomAffidavitMapped(int serveId)
        {
            var item = new AffidavitMapped()
            {
                ServeId = serveId,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CompanyId = 1,
                InvoiceNumber = GetRandomString(),
                IsNotMarried = Convert.ToBoolean(new Random().Next(1)),
                IsNotMilitary = Convert.ToBoolean(new Random().Next(1)),
                IsPaid = false,
                CareOf = GetNewField(FieldType.PersonServed),
                Relationship = GetNewField(FieldType.Relationship),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            return item;
        }

        public static AffidavitMapped GetRandomPersonalAffidavitMapped(int serveId)
        {
            var item = new AffidavitMapped()
            {
                ServeId = serveId,
                AffidavitType = AffidavitType.Personal,
                CompanyId = 1,
                InvoiceNumber = GetRandomString(),
                IsNotMarried = Convert.ToBoolean(new Random().Next(1)),
                IsNotMilitary = Convert.ToBoolean(new Random().Next(1)),
                IsPaid = false,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            return item;
        }

        public static AffidavitMapped GetRandomSubstituteAffidavitMapped(int serveId)
        {
            var item = new AffidavitMapped()
            {
                ServeId = serveId,
                AffidavitType = AffidavitType.Substitute,
                CompanyId = 1,
                InvoiceNumber = GetRandomString(),
                IsNotMarried = Convert.ToBoolean(new Random().Next(1)),
                IsNotMilitary = Convert.ToBoolean(new Random().Next(1)),
                IsPaid = false,
                CareOf = GetNewField(FieldType.PersonServed),
                Relationship = GetNewField(FieldType.Relationship),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            return item;
        }

        public static AffidavitMapped GetRandomReasonableAffidavitMapped(int serveId)
        {
            var item = new AffidavitMapped()
            {
                ServeId = serveId,
                AffidavitType = AffidavitType.Reasonable,
                CompanyId = 1,
                InvoiceNumber = GetRandomString(),
                IsNotMarried = Convert.ToBoolean(new Random().Next(1)),
                IsNotMilitary = Convert.ToBoolean(new Random().Next(1)),
                IsPaid = false,
                CareOf = GetNewField(FieldType.PersonServed),
                Relationship = GetNewField(FieldType.Relationship),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            return item;
        }

        public static AffidavitMapped GetRandomCorpAffidavitMapped(int serveId)
        {
            var item = new AffidavitMapped()
            {
                ServeId = serveId,
                AffidavitType = AffidavitType.Company,
                CompanyId = 1,
                InvoiceNumber = GetRandomString(),
                IsNotMarried = Convert.ToBoolean(new Random().Next(1)),
                IsNotMilitary = Convert.ToBoolean(new Random().Next(1)),
                IsPaid = false,
                CareOf = GetNewField(FieldType.PersonServed),
                Relationship = GetNewField(FieldType.Relationship),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            return item;
        }
    }
}
