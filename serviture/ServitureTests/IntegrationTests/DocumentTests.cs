﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class DocumentTests : TestBase<Document, object, DocumentSearchRequest>, IClassFixture<TestFixture>
    {
        public DocumentTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Document.Columns.ToList();
            SearchColumns = DocumentSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var id = await CreateItem();
            var deleteItem = new Document { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchDocumentAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var item = new Document
            {
                Name = "Sean Test Doc",
                HasText = false,
                SortOrder = 0,
                TextBefore = false,
                IsDeleted = false
            };

            return await Create(item);
        }
    }
}
