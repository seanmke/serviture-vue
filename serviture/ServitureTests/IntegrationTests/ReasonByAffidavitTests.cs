﻿namespace ServitureTests.IntegrationTests
{
    using Newtonsoft.Json;
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class ReasonByAffidavitsTests : TestBase<ReasonByAffidavit, object, ReasonByAffidavitSearchRequest>, IClassFixture<TestFixture>
    {
        public ReasonByAffidavitsTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = ReasonByAffidavit.Columns.ToList();
            SearchColumns = ReasonByAffidavitSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestGetByAffidavit()
        {
            var id = await CreateItem();
            var id2 = await CreateItem2();
            var id3 = await CreateItem3();

            var request = $"/api/ReasonByAffidavit/affidavitid/2";

            var response = await Client.GetAsync(request);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var item = JsonConvert.DeserializeObject<List<Reason>>(jsonResponse);

            var itemId = item.Select(x => x.Id).FirstOrDefault();
            Assert.Equal(15, itemId);

            var rowsAffected = await HardDelete(id);
            Assert.Equal(1, rowsAffected);

            var rowsAffected2 = await HardDelete(id2);
            Assert.Equal(1, rowsAffected2);

            var rowsAffected3 = await HardDelete(id3);
            Assert.Equal(1, rowsAffected3);
        }

        [Fact]
        public async Task TestUpsert()
        {
            var id = await CreateItem();

            var list = new List<ReasonByAffidavit>() { new ReasonByAffidavit { ReasonId = 3, AffidavitId = 1 } };
            var requestUrl = $"api/ReasonByAffidavit/upsert";

            var stringContent = TestHelpers.GetStringContent(list);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var ids = JsonConvert.DeserializeObject<List<int>>(jsonResponse);
            Assert.True(ids.Count > 0);

            var rowsAffected = await HardDelete(ids.FirstOrDefault());
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchDocumentAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var item = new ReasonByAffidavit
            {
                ReasonId = 15,
                AffidavitId = 1
            };

            return await Create(item);
        }

        private async Task<int> CreateItem2()
        {
            var item = new ReasonByAffidavit
            {
                ReasonId = 16,
                AffidavitId = 1
            };

            return await Create(item);
        }

        private async Task<int> CreateItem3()
        {
            var item = new ReasonByAffidavit
            {
                ReasonId = 15,
                AffidavitId = 2
            };

            return await Create(item);
        }
    }
}
