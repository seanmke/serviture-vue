﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class LIVE_TESTS : TestBase<Affidavit, AffidavitMapped, AffidavitSearchRequest>, IClassFixture<TestFixture>
    {
        public LIVE_TESTS(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Affidavit.Columns.ToList();
            SearchColumns = AffidavitSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task CreateData_1000()
        {
            for (int i = 0; i < 1000; i++)
            {
                var caseId = await CreateCaseMapped();
                var serveId = await CreateServeMapped(caseId);
                await CreateItemMapped(serveId);
            }
        }

        [Fact]
        public async Task CreateData_100()
        {
            for (int i = 0; i < 100; i++)
            {
                var caseId = await CreateCaseMapped();
                var serveId = await CreateServeMapped(caseId);
                await CreateItemMapped(serveId);
            }
        }

        [Fact]
        public async Task CreateData()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);
            await CreateItemMapped(serveId);
        }

        private async Task<int> CreateItemMapped(int serveId)
        {
            var item = TestHelpers.GetRandomAffidavitMapped(serveId);
            return await CreateMapped(item);
        }
    }
}
