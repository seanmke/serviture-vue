﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class ReasonTests : TestBase<Reason, object, ReasonSearchRequest>, IClassFixture<TestFixture>
    {
        public ReasonTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Reason.Columns.ToList();
            SearchColumns = ReasonSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var id = await CreateItem();
            var deleteItem = new Reason { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchReasonAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var item = new Reason
            {
                Value = "Test Reason",
                SortOrder = 99,
                IsDeleted = false
            };

            return await Create(item);
        }
    }
}