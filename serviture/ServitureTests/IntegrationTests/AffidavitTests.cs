﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class AffidavitTests : TestBase<Affidavit, AffidavitMapped, AffidavitSearchRequest>, IClassFixture<TestFixture>
    {
        public AffidavitTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Affidavit.Columns.ToList();
            SearchColumns = AffidavitSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServe(caseId);
            var id = await CreateItem(serveId);
            await TestCreateBaseAsync(id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);
            var id = await CreateItem(serveId);
            await TestUpdateBaseAsync(id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServe(caseId);
            var id = await CreateItem(serveId);
            var deleteItem = new Affidavit { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);
            var id = await CreateItem(serveId);

            await TestHardDeleteBaseAsync(id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task TestSearchAffidavitAsync()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);
            var id = await CreateItem(serveId);
            await TestSearchAsync(id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task TestSearchTermAsync()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);
            var id = await CreateItemMapped(serveId);
            var affidavit = await GetMapped(id);
            await TestSearchTableAsync(affidavit.CaseName.Value, id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task TestSearchAffidavitCreatedOnDaysBackAsync()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);
            var id = await CreateItem(serveId);
            await TestSearchAsync(id, 30);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------------------- CareOf ------------------------------------

        [Fact]
        public async Task Post_CareOf_New()
        {
            var item = TestHelpers.GetNewField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var itemMapped = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CareOf = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(itemMapped);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.NotEqual(itemMapped.CareOf.Id, newAffidavit.CareOf.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Post_CareOf_Existing()
        {
            var item = await GetRandomField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CareOf = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(item.Id, newAffidavit.CareOf.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_CareOf_New()
        {
            var item = TestHelpers.GetNewField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CareOf = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.CareOf = TestHelpers.GetNewField(FieldType.PersonServed);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.CareOf.Value, updatedAffidavit.CareOf.Value);
            Assert.NotEqual(newAffidavit.CareOf.Id, updatedAffidavit.CareOf.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_CareOf_Existing()
        {
            var item = await GetRandomField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CareOf = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.CareOf = await GetRandomField(FieldType.PersonServed);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedCase = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedCase.Id);
            Assert.NotEqual(originalAffidavit.CareOf.Value, updatedCase.CareOf.Value);
            Assert.NotEqual(originalAffidavit.CareOf.Id, updatedCase.CareOf.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------------------- Relationship ------------------------------------

        [Fact]
        public async Task Post_Relationship_New()
        {
            var item = TestHelpers.GetNewField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var itemMapped = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Relationship = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(itemMapped);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.NotEqual(itemMapped.Relationship.Id, newAffidavit.Relationship.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Post_Relationship_Existing()
        {
            var item = await GetRandomField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Relationship = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(item.Id, newAffidavit.Relationship.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Relationship_New()
        {
            var item = TestHelpers.GetNewField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Relationship = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.Relationship = TestHelpers.GetNewField(FieldType.PersonServed);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.Relationship.Value, updatedAffidavit.Relationship.Value);
            Assert.NotEqual(newAffidavit.Relationship.Id, updatedAffidavit.Relationship.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Relationship_Existing()
        {
            var item = await GetRandomField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Relationship = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.Relationship = await GetRandomField(FieldType.PersonServed);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedCase = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedCase.Id);
            Assert.NotEqual(originalAffidavit.Relationship.Value, updatedCase.Relationship.Value);
            Assert.NotEqual(originalAffidavit.Relationship.Id, updatedCase.Relationship.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- InvoiceNumber ----------------------------

        [Fact]
        public async Task Post_InvoiceNumber()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavitId.ToString(), newAffidavit.InvoiceNumber);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_InvoiceNumber()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.InvoiceNumber = "456";
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.InvoiceNumber, updatedAffidavit.InvoiceNumber);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- IsPaid ----------------------------

        [Fact]
        public async Task Post_IsPaid()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                IsPaid = false,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(originalAffidavit.IsPaid, newAffidavit.IsPaid);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_IsPaid()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                IsPaid = false,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.IsPaid = true;
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.IsPaid, updatedAffidavit.IsPaid);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- IsNotMilitary ----------------------------

        [Fact]
        public async Task Post_IsNotMilitary()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                IsNotMilitary = false,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(originalAffidavit.IsNotMilitary, newAffidavit.IsNotMilitary);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_IsNotMilitary()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                IsNotMilitary = false,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.IsNotMilitary = true;
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.IsNotMilitary, updatedAffidavit.IsNotMilitary);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- IsNotMarried ----------------------------

        [Fact]
        public async Task Post_IsNotMarried()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                IsNotMarried = false,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(originalAffidavit.IsNotMarried, newAffidavit.IsNotMarried);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_IsNotMarried()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                IsNotMarried = false,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.IsNotMarried = true;
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.IsNotMarried, updatedAffidavit.IsNotMarried);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- AffidavitType ----------------------------

        [Fact]
        public async Task Post_AffidavitType()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(originalAffidavit.AffidavitType, newAffidavit.AffidavitType);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_AffidavitType()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = AffidavitType.Personal,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.AffidavitType = AffidavitType.Substitute;
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.AffidavitType, updatedAffidavit.AffidavitType);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- Client ----------------------------

        [Fact]
        public async Task Put_Client_New()
        {
            var item = TestHelpers.GetNewClient();
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Client = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.Client = TestHelpers.GetNewClient();
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.Client.ClientName, updatedAffidavit.Client.ClientName);
            Assert.NotEqual(newAffidavit.Client.Id, updatedAffidavit.Client.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Client_Existing()
        {
            var item = await GetRandomClient();
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Client = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.Client = await GetRandomClient();
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.Client.ClientName, updatedAffidavit.Client.ClientName);
            Assert.NotEqual(originalAffidavit.Client.Id, updatedAffidavit.Client.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- Contact ----------------------------

        [Fact]
        public async Task Put_Contact_New()
        {
            var item = TestHelpers.GetNewField(FieldType.Contact);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Contact = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.Contact = TestHelpers.GetNewField(FieldType.Contact);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.Contact.Value, updatedAffidavit.Contact.Value);
            Assert.NotEqual(newAffidavit.Contact.Id, updatedAffidavit.Contact.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Contact_Existing()
        {
            var item = await GetRandomField(FieldType.Contact);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Contact = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.Contact = await GetRandomField(FieldType.Contact);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.Contact.Value, updatedAffidavit.Contact.Value);
            Assert.NotEqual(originalAffidavit.Contact.Id, updatedAffidavit.Contact.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- FileNum ----------------------------

        [Fact]
        public async Task Put_FileNum_New()
        {
            var item = TestHelpers.GetNewField(FieldType.ClientFileNumber);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                FileNum = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.FileNum = TestHelpers.GetNewField(FieldType.ClientFileNumber);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.FileNum.Value, updatedAffidavit.FileNum.Value);
            Assert.NotEqual(newAffidavit.FileNum.Id, updatedAffidavit.FileNum.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_FileNum_Existing()
        {
            var item = await GetRandomField(FieldType.ClientFileNumber);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                FileNum = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.FileNum = await GetRandomField(FieldType.ClientFileNumber);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.FileNum.Value, updatedAffidavit.FileNum.Value);
            Assert.NotEqual(originalAffidavit.FileNum.Id, updatedAffidavit.FileNum.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- OtherLocation ----------------------------

        [Fact]
        public async Task Put_OtherLocation_New()
        {
            var item = TestHelpers.GetNewField(FieldType.OtherLocation);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                OtherLocation = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.OtherLocation = TestHelpers.GetNewField(FieldType.OtherLocation);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.OtherLocation.Value, updatedAffidavit.OtherLocation.Value);
            Assert.NotEqual(newAffidavit.OtherLocation.Id, updatedAffidavit.OtherLocation.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_OtherLocation_Existing()
        {
            var item = await GetRandomField(FieldType.OtherLocation);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                OtherLocation = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.OtherLocation = await GetRandomField(FieldType.OtherLocation);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.OtherLocation.Value, updatedAffidavit.OtherLocation.Value);
            Assert.NotEqual(originalAffidavit.OtherLocation.Id, updatedAffidavit.OtherLocation.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- CaseNum ----------------------------

        [Fact]
        public async Task Put_CaseNum_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseNumber);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CaseNum = item,

                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.CaseNum = TestHelpers.GetNewField(FieldType.CaseNumber);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.CaseNum.Value, updatedAffidavit.CaseNum.Value);
            Assert.NotEqual(newAffidavit.CaseNum.Id, updatedAffidavit.CaseNum.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_CaseNum_Existing()
        {
            var item = await GetRandomField(FieldType.CaseNumber);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CaseNum = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.CaseNum = await GetRandomField(FieldType.CaseNumber);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.CaseNum.Value, updatedAffidavit.CaseNum.Value);
            Assert.NotEqual(originalAffidavit.CaseNum.Id, updatedAffidavit.CaseNum.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- CaseName ----------------------------

        [Fact]
        public async Task Put_CaseName_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseName);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CaseName = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.CaseName = TestHelpers.GetNewField(FieldType.CaseName);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.CaseName.Value, updatedAffidavit.CaseName.Value);
            Assert.NotEqual(newAffidavit.CaseName.Id, updatedAffidavit.CaseName.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_CaseName_Existing()
        {
            var item = await GetRandomField(FieldType.CaseName);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CaseName = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.CaseName = await GetRandomField(FieldType.CaseName);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.CaseName.Value, updatedAffidavit.CaseName.Value);
            Assert.NotEqual(originalAffidavit.CaseName.Id, updatedAffidavit.CaseName.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- Defendant ----------------------------

        [Fact]
        public async Task Put_Defendant_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseName);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Defendant = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.Defendant = TestHelpers.GetNewField(FieldType.CaseName);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.Defendant.Value, updatedAffidavit.Defendant.Value);
            Assert.NotEqual(newAffidavit.Defendant.Id, updatedAffidavit.Defendant.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Defendant_Existing()
        {
            var item = await GetRandomField(FieldType.CaseName);
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                Defendant = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.Defendant = await GetRandomField(FieldType.CaseName);
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.Defendant.Value, updatedAffidavit.Defendant.Value);
            Assert.NotEqual(originalAffidavit.Defendant.Id, updatedAffidavit.Defendant.Id);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- CourtDate ----------------------------

        [Fact]
        public async Task Put_CourtDate()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                CourtDate = "01/01/2021",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.CourtDate = "02/01/2021";
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.CourtDate, updatedAffidavit.CourtDate);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- State ----------------------------

        [Fact]
        public async Task Put_State()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                State = "Wisconsin",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.State = "Illinois";
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.State, updatedAffidavit.State);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- County ----------------------------

        [Fact]
        public async Task Put_County()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var originalAffidavit = new AffidavitMapped
            {
                ServeId = serveId,
                InvoiceNumber = TestHelpers.GetRandomString(),
                CompanyId = 1,
                AffidavitType = (AffidavitType)new Random().Next(4),
                County = "Milwaukee",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newAffidavitId = await CreateMapped(originalAffidavit);
            var newAffidavit = await GetMapped(newAffidavitId);

            newAffidavit.County = "Manitowoc";
            await UpdateMapped(newAffidavitId, newAffidavit);

            var updatedAffidavit = await GetMapped(newAffidavitId);

            Assert.Equal(newAffidavit.Id, updatedAffidavit.Id);
            Assert.NotEqual(originalAffidavit.County, updatedAffidavit.County);

            var rowsAffected = await HardDelete(newAffidavitId);
            Assert.Equal(1, rowsAffected);
        }

        // --------------------- Workflow Tests --------------------------------------------

        [Fact]
        public async Task Post_Affidavit()
        {
            var newCaseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(newCaseId);
            var affidavitId = await CreateItemMapped(serveId);

            var serveMapped = await GetServeMapped(serveId);
            var affidavitMapped = await GetMapped(affidavitId);

            var doesEqual = affidavitMapped.CompareServe(serveMapped);
            Assert.True(doesEqual);
            Assert.Equal((int)ServeStatus.Complete, serveMapped.ServeStatus);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Post_Affidavit_Sub()
        {
            var newCaseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(newCaseId);
            var item = TestHelpers.GetRandomSubstituteAffidavitMapped(serveId);
             var affidavitId = await CreateMapped(item);

            var serveMapped = await GetServeMapped(serveId);
            var affidavitMapped = await GetMapped(affidavitId);

            var doesEqual = affidavitMapped.CompareServe(serveMapped);
            Assert.True(doesEqual);
            Assert.Equal((int)ServeStatus.Complete, serveMapped.ServeStatus);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        //The field search is expensive.  Might need to look into doing that only once, or prepopulating 
        //and making the id constant.

        //[Fact]
        //public async Task Post_Affidavit_Corp_Officer()
        //{
        //    var newCaseId = await CreateCaseMapped();
        //    var serveId = await CreateServeMapped(newCaseId);
        //    var item = TestHelpers.GetRandomCorpAffidavitMapped(serveId);
        //    item.Relationship = await GetOfficerRelationship();
        //    var affidavitId = await CreateMapped(item);

        //    var serveMapped = await GetServeMapped(serveId);
        //    var affidavitMapped = await GetMapped(affidavitId);

        //    var doesEqual = affidavitMapped.CompareServe(serveMapped);
        //    Assert.True(doesEqual);
        //    Assert.Equal((int)ServeStatus.Complete, serveMapped.ServeStatus);

        //    var rowsAffected2 = await HardDelete("case", newCaseId);
        //    Assert.Equal(1, rowsAffected2);
        //}

        //[Fact]
        //public async Task Post_Affidavit_Corp_Person()
        //{
        //    var newCaseId = await CreateCaseMapped();
        //    var serveId = await CreateServeMapped(newCaseId);
        //    var item = TestHelpers.GetRandomCorpAffidavitMapped(serveId);
        //    item.Relationship = await GetPersonRelationship();
        //    var affidavitId = await CreateMapped(item);

        //    var serveMapped = await GetServeMapped(serveId);
        //    var affidavitMapped = await GetMapped(affidavitId);

        //    var doesEqual = affidavitMapped.CompareServe(serveMapped);
        //    Assert.True(doesEqual);
        //    Assert.Equal((int)ServeStatus.Complete, serveMapped.ServeStatus);

        //    var rowsAffected2 = await HardDelete("case", newCaseId);
        //    Assert.Equal(1, rowsAffected2);
        //}

        [Fact]
        public async Task Put_Addresses()
        {
            var newCaseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(newCaseId);
            var affidavitId = await CreateItemMapped(serveId);
            var affidavitMapped = await GetMapped(affidavitId);

            var address = new Address
            {
                StreetAddress = TestHelpers.GetRandomString()
            };

            affidavitMapped.Addresses = new List<Address> { address };

            await UpdateMapped(affidavitId, affidavitMapped);

            var updatedAffidavitMapped = await GetMapped(affidavitId);

            Assert.Equal(address.StreetAddress, updatedAffidavitMapped.Addresses.FirstOrDefault().StreetAddress);
            Assert.Single(updatedAffidavitMapped.Addresses);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Attempts()
        {
            var newCaseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(newCaseId);
            var affidavitId = await CreateItemMapped(serveId);
            var affidavitMapped = await GetMapped(affidavitId);

            var attempt = new AttemptMapped
            {
                DateServed = "03-03-2021",
                IsSuccess = false,
                OtherInfo = TestHelpers.GetNewField(FieldType.AttemptOther)
            };

            affidavitMapped.Attempts = new List<AttemptMapped> { attempt };

            await UpdateMapped(affidavitId, affidavitMapped);

            var updatedAffidavitMapped = await GetMapped(affidavitId);

            Assert.Equal(attempt.OtherInfo.Value, updatedAffidavitMapped.Attempts.FirstOrDefault().OtherInfo.Value);
            Assert.Single(updatedAffidavitMapped.Attempts);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_DocsByServe()
        {
            var newCaseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(newCaseId);
            var affidavitId = await CreateItemMapped(serveId);
            var affidavitMapped = await GetMapped(affidavitId);

            var docByServe = await GetRandomDocMapped();

            affidavitMapped.Documents = docByServe;

            await UpdateMapped(affidavitId, affidavitMapped);

            var updatedAffidavitMapped = await GetMapped(affidavitId);

            Assert.Equal(docByServe.FirstOrDefault().DocumentId, updatedAffidavitMapped.Documents.FirstOrDefault().DocumentId);
            if (docByServe.FirstOrDefault().DocumentText != null)
            {
                Assert.Equal(docByServe.FirstOrDefault().DocumentText.Value, updatedAffidavitMapped.Documents.FirstOrDefault().DocumentText.Value);
            }
            Assert.Single(updatedAffidavitMapped.Documents);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Reasons()
        {
            var newCaseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(newCaseId);
            var affidavitId = await CreateItemMapped(serveId);
            var affidavitMapped = await GetMapped(affidavitId);

            var reason = await GetRandomReason();

            affidavitMapped.Reasons = new List<ReasonByAffidavitMapped> { new ReasonByAffidavitMapped(reason) };

            await UpdateMapped(affidavitId, affidavitMapped);

            var updatedAffidavitMapped = await GetMapped(affidavitId);

            Assert.Equal(reason.ReasonId, updatedAffidavitMapped.Reasons.FirstOrDefault().ReasonId);
            Assert.Single(updatedAffidavitMapped.Reasons);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        private async Task<int> CreateItem(int serveId)
        {
            var item = TestHelpers.GetRandomAffidavit(serveId);
            return await Create(item);
        }

        private async Task<int> CreateItemMapped(int serveId)
        {
            var item = TestHelpers.GetRandomAffidavitMapped(serveId);
            return await CreateMapped(item);
        }
    }
}
