﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class ContractorUserTests : TestBase<ContractorUser, object, ContractorUserSearchRequest>, IClassFixture<TestFixture>
    {
        public ContractorUserTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Serviture.Models.ContractorUser.Columns.ToList();
            SearchColumns = ContractorUserSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var id = await CreateItem();
            var deleteItem = new ContractorUser { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchContractorUserAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var item = new ContractorUser
            {
                Name = TestHelpers.GetRandomString(),
                ContractorId = 1,
                IsDeleted = false
            };

            return await Create(item);
        }
    }
}
