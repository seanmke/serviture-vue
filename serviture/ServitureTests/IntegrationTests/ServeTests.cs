﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class ServeTests : TestBase<Serve, ServeMapped, ServeSearchRequest>, IClassFixture<TestFixture>
    {
        public ServeTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Serve.Columns.ToList();
            SearchColumns = ServeSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var id = await CreateItem();
            var deleteItem = new Serve { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchServeAsync()
        {
            var caseId = await CreateCaseMapped();
            var id = await CreateServe(caseId);
            await TestSearchAsync(id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task TestSearchTermAsync()
        {
            var caseId = await CreateCaseMapped();
            var serveId = await CreateServeMapped(caseId);

            var newServe = await GetMapped(serveId);

            await TestSearchTableAsync(newServe.Entity.Value, serveId);

            var rowsAffected = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------------------- Entity ------------------------------------

        [Fact]
        public async Task Post_Entity_New()
        {
            var item = TestHelpers.GetNewField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();

            var itemMapped = new ServeMapped
            {
                CaseId = caseId,
                Entity = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(itemMapped);
            var newServe = await GetMapped(newServeId);

            var doesEqual = itemMapped.CompareServe(newServe);
            Assert.True(doesEqual);
            Assert.NotEqual(itemMapped.Entity.Id, newServe.Entity.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Post_Entity_Existing()
        {
            var item = await GetRandomField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                Entity = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            var doesEqual = originalServe.CompareServe(newServe);
            Assert.True(doesEqual);
            Assert.Equal(item.Id, newServe.Entity.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Entity_New()
        {
            var item = TestHelpers.GetNewField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();

            var itemMapped = new ServeMapped
            {
                CaseId = caseId,
                Entity = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(itemMapped);
            var newServe = await GetMapped(newServeId);

            newServe.Entity = TestHelpers.GetNewField(FieldType.PersonServed);
            await UpdateMapped(newServeId, newServe);

            var updatedServe = await GetMapped(newServeId);

            Assert.Equal(newServe.Id, updatedServe.Id);
            Assert.NotEqual(itemMapped.Entity.Value, updatedServe.Entity.Value);
            Assert.NotEqual(newServe.Entity.Id, updatedServe.Entity.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Entity_Existing()
        {
            var item = await GetRandomField(FieldType.PersonServed);
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                Entity = item,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            newServe.Entity = await GetRandomField(FieldType.PersonServed);
            await UpdateMapped(newServeId, newServe);

            var updatedCase = await GetMapped(newServeId);

            Assert.Equal(newServe.Id, updatedCase.Id);
            Assert.NotEqual(originalServe.Entity.Value, updatedCase.Entity.Value);
            Assert.NotEqual(originalServe.Entity.Id, updatedCase.Entity.Id);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- NotaryState ----------------------------

        [Fact]
        public async Task Post_NotaryState()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                NotaryState = "Wisconsin",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            var doesEqual = originalServe.CompareServe(newServe);
            Assert.True(doesEqual);
            Assert.Equal(originalServe.NotaryState, newServe.NotaryState);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_NotaryState()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                NotaryState = "Wisconsin",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            newServe.NotaryState = "Illinois";
            await UpdateMapped(newServeId, newServe);

            var updatedServe = await GetMapped(newServeId);

            Assert.Equal(newServe.Id, updatedServe.Id);
            Assert.NotEqual(originalServe.NotaryState, updatedServe.NotaryState);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- NotaryCounty ----------------------------

        [Fact]
        public async Task Post_NotaryCounty()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                NotaryCounty = "Milwaukee",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            var doesEqual = originalServe.CompareServe(newServe);
            Assert.True(doesEqual);
            Assert.Equal(originalServe.NotaryCounty, newServe.NotaryCounty);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_NotaryCounty()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                NotaryCounty = "Milwaukee",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            newServe.NotaryCounty = "Waukesha";
            await UpdateMapped(newServeId, newServe);

            var updatedServe = await GetMapped(newServeId);

            Assert.Equal(newServe.Id, updatedServe.Id);
            Assert.NotEqual(originalServe.NotaryCounty, updatedServe.NotaryCounty);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- Notes ----------------------------

        [Fact]
        public async Task Post_Notes()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                Notes = "Some notes go here",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            var doesEqual = originalServe.CompareServe(newServe);
            Assert.True(doesEqual);
            Assert.Equal(originalServe.Notes, newServe.Notes);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Notes()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                Notes = "Some notes go here",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            newServe.Notes = "Some new notes go here";
            await UpdateMapped(newServeId, newServe);

            var updatedServe = await GetMapped(newServeId);

            Assert.Equal(newServe.Id, updatedServe.Id);
            Assert.NotEqual(originalServe.Notes, updatedServe.Notes);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- SigState ----------------------------

        [Fact]
        public async Task Post_SigState()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                SigState = "Wisconsin",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            var doesEqual = originalServe.CompareServe(newServe);
            Assert.True(doesEqual);
            Assert.Equal(originalServe.SigState, newServe.SigState);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_SigState()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                SigState = "Wisconsin",
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            newServe.SigState = "Illinois";
            await UpdateMapped(newServeId, newServe);

            var updatedServe = await GetMapped(newServeId);

            Assert.Equal(newServe.Id, updatedServe.Id);
            Assert.NotEqual(originalServe.SigState, updatedServe.SigState);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- Charge ----------------------------

        [Fact]
        public async Task Post_Charge()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                Charge = 10,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            var doesEqual = originalServe.CompareServe(newServe);
            Assert.True(doesEqual);
            Assert.Equal(originalServe.Charge, newServe.Charge);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Charge()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                Charge = 10,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            newServe.Charge = 20;
            await UpdateMapped(newServeId, newServe);

            var updatedServe = await GetMapped(newServeId);

            Assert.Equal(newServe.Id, updatedServe.Id);
            Assert.NotEqual(originalServe.Charge, updatedServe.Charge);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- DueDate ----------------------------

        [Fact]
        public async Task Post_DueDate()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                DueDate = DateTime.Now.ToShortDateString(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            var doesEqual = originalServe.CompareServe(newServe);
            Assert.True(doesEqual);
            Assert.Equal(originalServe.DueDate, newServe.DueDate);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_DueDate()
        {
            var caseId = await CreateCaseMapped();

            var originalServe = new ServeMapped
            {
                CaseId = caseId,
                DueDate = DateTime.Now.ToShortDateString(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newServeId = await CreateMapped(originalServe);
            var newServe = await GetMapped(newServeId);

            newServe.DueDate = DateTime.Now.AddDays(1).ToShortDateString();
            await UpdateMapped(newServeId, newServe);

            var updatedServe = await GetMapped(newServeId);

            Assert.Equal(newServe.Id, updatedServe.Id);
            Assert.NotEqual(originalServe.DueDate, updatedServe.DueDate);

            var rowsAffected2 = await HardDelete("case", caseId);
            Assert.Equal(1, rowsAffected2);
        }

        // ------------------- Case Served ----------------------------

        [Fact]
        public async Task Post_CaseServed()
        {
            var newCaseId = await CreateCaseMapped();

            var searchRequest = new ServeSearchRequest { CaseId = newCaseId, PageIndex = 0, PageSize = 100 };
            var searchResponse = await Search(searchRequest);
            var serves = searchResponse.Results;

            foreach(var serve in serves)
            {
                var serveMapped = await GetMapped(serve.Id);
                Assert.NotNull(serveMapped.Entity);
                Assert.True(serveMapped.Entity.Id > 0);
                Assert.True(serveMapped.Documents.Count > 0);
                Assert.NotNull(serveMapped.ProcessServer);
                Assert.NotEqual(serveMapped.Notes, string.Empty);
            }

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Addresses()
        {
            var newCaseId = await CreateCaseMapped();

            var searchRequest = new ServeSearchRequest { CaseId = newCaseId, PageIndex = 0, PageSize = 100 };
            var searchResponse = await Search(searchRequest);
            var serve = searchResponse.Results.FirstOrDefault();

            var serveMapped = await GetMapped(serve.Id);
            var address = new Address
            {
                ServeId = serve.Id,
                StreetAddress = TestHelpers.GetRandomString()
            };

            serveMapped.Addresses.Add(address);

            await UpdateMapped(serve.Id, serveMapped);

            var updatedCase = await GetMapped(serve.Id);

            Assert.Equal(address.StreetAddress, updatedCase.Addresses.FirstOrDefault().StreetAddress);
            Assert.Single(updatedCase.Addresses);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_Attempts()
        {
            var newCaseId = await CreateCaseMapped();

            var searchRequest = new ServeSearchRequest { CaseId = newCaseId, PageIndex = 0, PageSize = 100 };
            var searchResponse = await Search(searchRequest);
            var serve = searchResponse.Results.FirstOrDefault();

            var serveMapped = await GetMapped(serve.Id);
            var attempt = new AttemptMapped
            {
                ServeId = serve.Id,
                DateServed = "03-03-2021",
                IsSuccess = false,
                OtherInfo = TestHelpers.GetNewField(FieldType.AttemptOther)
            };

            serveMapped.Attempts.Add(attempt);

            await UpdateMapped(serve.Id, serveMapped);

            var updatedCase = await GetMapped(serve.Id);

            Assert.Equal(attempt.OtherInfo.Value, updatedCase.Attempts.FirstOrDefault().OtherInfo.Value);
            Assert.Single(updatedCase.Attempts);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        [Fact]
        public async Task Put_DocsByServe()
        {
            var newCaseId = await CreateCaseMapped();

            var searchRequest = new ServeSearchRequest { CaseId = newCaseId, PageIndex = 0, PageSize = 100 };
            var searchResponse = await Search(searchRequest);
            var serve = searchResponse.Results.FirstOrDefault();

            var serveMapped = await GetMapped(serve.Id);
            var docByServe = await GetRandomDocMapped();
            docByServe.FirstOrDefault().ServeId = serve.Id;

            serveMapped.Documents = docByServe;

            await UpdateMapped(serve.Id, serveMapped);

            var updatedServe = await GetMapped(serve.Id);

            Assert.Equal(docByServe.FirstOrDefault().DocumentId, updatedServe.Documents.FirstOrDefault().DocumentId);
            if(docByServe.FirstOrDefault().DocumentText != null)
            {
                Assert.Equal(docByServe.FirstOrDefault().DocumentText.Value, updatedServe.Documents.FirstOrDefault().DocumentText.Value);
            }
            Assert.Single(updatedServe.Documents);

            var rowsAffected2 = await HardDelete("case", newCaseId);
            Assert.Equal(1, rowsAffected2);
        }

        private async Task<int> CreateItem()
        {
            var item = TestHelpers.GetRandomServe(1);

            return await Create(item);
        }
    }
}
