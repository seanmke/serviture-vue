﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class CaseTests : TestBase<Case, CaseMapped, CaseSearchRequest>, IClassFixture<TestFixture>
    {
        public CaseTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = Case.Columns.ToList();
            SearchColumns = CaseSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var id = await CreateItem();
            var deleteItem = new Case { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchCaseAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        // ------------------- Client ----------------------------

        [Fact]
        public async Task Post_Client_New()
        {
            var item = TestHelpers.GetNewClient();
            var originalCase = new CaseMapped
            {
                Client = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.NotEqual(originalCase.Client.Id, newCase.Client.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Post_Client_Existing()
        {
            var item = await GetRandomClient();
            var originalCase = new CaseMapped
            {
                Client = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(item.Id, newCase.Client.Id); 
            
            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Client_New()
        {
            var item = TestHelpers.GetNewClient();
            var originalCase = new CaseMapped
            {
                Client = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.Client = TestHelpers.GetNewClient();
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.Client.ClientName, updatedCase.Client.ClientName);
            Assert.NotEqual(newCase.Client.Id, updatedCase.Client.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Client_Existing()
        {
            var item = await GetRandomClient();
            var originalCase = new CaseMapped
            {
                Client = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.Client = await GetRandomClient();
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.Client.ClientName, updatedCase.Client.ClientName);
            Assert.NotEqual(originalCase.Client.Id, updatedCase.Client.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- Contact ----------------------------

        [Fact]
        public async Task Post_Contact_New()
        {
            var item = TestHelpers.GetNewField(FieldType.Contact);
            var originalCase = new CaseMapped
            {
                Contact = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.NotEqual(originalCase.Contact.Id, newCase.Contact.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Post_Contact_Existing()
        {
            var item = await GetRandomField(FieldType.Contact);
            var originalCase = new CaseMapped
            {
                Contact = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(item.Id, newCase.Contact.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Contact_New()
        {
            var item = TestHelpers.GetNewField(FieldType.Contact);
            var originalCase = new CaseMapped
            {
                Contact = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.Contact = TestHelpers.GetNewField(FieldType.Contact);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.Contact.Value, updatedCase.Contact.Value);
            Assert.NotEqual(newCase.Contact.Id, updatedCase.Contact.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Contact_Existing()
        {
            var item = await GetRandomField(FieldType.Contact);
            var originalCase = new CaseMapped
            {
                Contact = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.Contact = await GetRandomField(FieldType.Contact);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.Contact.Value, updatedCase.Contact.Value);
            Assert.NotEqual(originalCase.Contact.Id, updatedCase.Contact.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- ClientFileNumber ----------------------------

        [Fact]
        public async Task Post_ClientFileNumber_New()
        {
            var item = TestHelpers.GetNewField(FieldType.ClientFileNumber);
            var originalCase = new CaseMapped
            {
                ClientFileNumber = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.NotEqual(originalCase.ClientFileNumber.Id, newCase.ClientFileNumber.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Post_ClientFileNumber_Existing()
        {
            var item = await GetRandomField(FieldType.ClientFileNumber);
            var originalCase = new CaseMapped
            {
                ClientFileNumber = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(item.Id, newCase.ClientFileNumber.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_ClientFileNumber_New()
        {
            var item = TestHelpers.GetNewField(FieldType.ClientFileNumber);
            var originalCase = new CaseMapped
            {
                ClientFileNumber = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.ClientFileNumber = TestHelpers.GetNewField(FieldType.ClientFileNumber);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.ClientFileNumber.Value, updatedCase.ClientFileNumber.Value);
            Assert.NotEqual(newCase.ClientFileNumber.Id, updatedCase.ClientFileNumber.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_ClientFileNumber_Existing()
        {
            var item = await GetRandomField(FieldType.ClientFileNumber);
            var originalCase = new CaseMapped
            {
                ClientFileNumber = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.ClientFileNumber = await GetRandomField(FieldType.ClientFileNumber);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.ClientFileNumber.Value, updatedCase.ClientFileNumber.Value);
            Assert.NotEqual(originalCase.ClientFileNumber.Id, updatedCase.ClientFileNumber.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- OtherLocation ----------------------------

        [Fact]
        public async Task Post_OtherLocation_New()
        {
            var item = TestHelpers.GetNewField(FieldType.OtherLocation);
            var originalCase = new CaseMapped
            {
                OtherLocation = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.NotEqual(originalCase.OtherLocation.Id, newCase.OtherLocation.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Post_OtherLocation_Existing()
        {
            var item = await GetRandomField(FieldType.OtherLocation);
            var originalCase = new CaseMapped
            {
                OtherLocation = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(item.Id, newCase.OtherLocation.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_OtherLocation_New()
        {
            var item = TestHelpers.GetNewField(FieldType.OtherLocation);
            var originalCase = new CaseMapped
            {
                OtherLocation = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.OtherLocation = TestHelpers.GetNewField(FieldType.OtherLocation);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.OtherLocation.Value, updatedCase.OtherLocation.Value);
            Assert.NotEqual(newCase.OtherLocation.Id, updatedCase.OtherLocation.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_OtherLocation_Existing()
        {
            var item = await GetRandomField(FieldType.OtherLocation);
            var originalCase = new CaseMapped
            {
                OtherLocation = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.OtherLocation = await GetRandomField(FieldType.OtherLocation);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.OtherLocation.Value, updatedCase.OtherLocation.Value);
            Assert.NotEqual(originalCase.OtherLocation.Id, updatedCase.OtherLocation.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- CaseNumber ----------------------------

        [Fact]
        public async Task Post_CaseNumber_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseNumber);
            var originalCase = new CaseMapped
            {
                CaseNumber = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.NotEqual(originalCase.CaseNumber.Id, newCase.CaseNumber.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Post_CaseNumber_Existing()
        {
            var item = await GetRandomField(FieldType.CaseNumber);
            var originalCase = new CaseMapped
            {
                CaseNumber = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(item.Id, newCase.CaseNumber.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_CaseNumber_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseNumber);
            var originalCase = new CaseMapped
            {
                CaseNumber = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.CaseNumber = TestHelpers.GetNewField(FieldType.CaseNumber);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.CaseNumber.Value, updatedCase.CaseNumber.Value);
            Assert.NotEqual(newCase.CaseNumber.Id, updatedCase.CaseNumber.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_CaseNumber_Existing()
        {
            var item = await GetRandomField(FieldType.CaseNumber);
            var originalCase = new CaseMapped
            {
                CaseNumber = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.CaseNumber = await GetRandomField(FieldType.CaseNumber);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.CaseNumber.Value, updatedCase.CaseNumber.Value);
            Assert.NotEqual(originalCase.CaseNumber.Id, updatedCase.CaseNumber.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- CaseName ----------------------------

        [Fact]
        public async Task Post_CaseName_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseName);
            var originalCase = new CaseMapped
            {
                CaseName = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.NotEqual(originalCase.CaseName.Id, newCase.CaseName.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Post_CaseName_Existing()
        {
            var item = await GetRandomField(FieldType.CaseName);
            var originalCase = new CaseMapped
            {
                CaseName = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(item.Id, newCase.CaseName.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_CaseName_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseName);
            var originalCase = new CaseMapped
            {
                CaseName = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.CaseName = TestHelpers.GetNewField(FieldType.CaseName);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.CaseName.Value, updatedCase.CaseName.Value);
            Assert.NotEqual(newCase.CaseName.Id, updatedCase.CaseName.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_CaseName_Existing()
        {
            var item = await GetRandomField(FieldType.CaseName);
            var originalCase = new CaseMapped
            {
                CaseName = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.CaseName = await GetRandomField(FieldType.CaseName);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.CaseName.Value, updatedCase.CaseName.Value);
            Assert.NotEqual(originalCase.CaseName.Id, updatedCase.CaseName.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- Defendant ----------------------------

        [Fact]
        public async Task Post_Defendant_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseName);
            var originalCase = new CaseMapped
            {
                Defendant = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.NotEqual(originalCase.Defendant.Id, newCase.Defendant.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Post_Defendant_Existing()
        {
            var item = await GetRandomField(FieldType.CaseName);
            var originalCase = new CaseMapped
            {
                Defendant = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(item.Id, newCase.Defendant.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Defendant_New()
        {
            var item = TestHelpers.GetNewField(FieldType.CaseName);
            var originalCase = new CaseMapped
            {
                Defendant = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.Defendant = TestHelpers.GetNewField(FieldType.CaseName);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.Defendant.Value, updatedCase.Defendant.Value);
            Assert.NotEqual(newCase.Defendant.Id, updatedCase.Defendant.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_Defendant_Existing()
        {
            var item = await GetRandomField(FieldType.CaseName);
            var originalCase = new CaseMapped
            {
                Defendant = item,
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.Defendant = await GetRandomField(FieldType.CaseName);
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.Defendant.Value, updatedCase.Defendant.Value);
            Assert.NotEqual(originalCase.Defendant.Id, updatedCase.Defendant.Id);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- CourtDate ----------------------------

        [Fact]
        public async Task Post_CourtDate()
        {
            var originalCase = new CaseMapped
            {
                CourtDate = "1/1/2021",
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(originalCase.CourtDate, newCase.CourtDate);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_CourtDate()
        {
            var originalCase = new CaseMapped
            {
                CourtDate = "01/01/2021",
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.CourtDate = "02/01/2021";
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.CourtDate, updatedCase.CourtDate);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- State ----------------------------

        [Fact]
        public async Task Post_State()
        {
            var originalCase = new CaseMapped
            {
                State = "Wisconsin",
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(originalCase.State, newCase.State);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_State()
        {
            var originalCase = new CaseMapped
            {
                State = "Wisconsin",
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.State = "Illinois";
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.State, updatedCase.State);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- County ----------------------------

        [Fact]
        public async Task Post_County()
        {
            var originalCase = new CaseMapped
            {
                County = "Milwaukee",
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(originalCase.County, newCase.County);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        [Fact]
        public async Task Put_County()
        {
            var originalCase = new CaseMapped
            {
                County = "Milwaukee",
                CaseServeMappeds = new List<CaseServeMapped>(),
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            newCase.County = "Manitowoc";
            await UpdateMapped(newCaseId, newCase);

            var updatedCase = await GetMapped(newCaseId);

            Assert.Equal(newCase.Id, updatedCase.Id);
            Assert.NotEqual(originalCase.County, updatedCase.County);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- Case Served ----------------------------

        [Fact]
        public async Task Post_CaseServed()
        {
            var numServes = new Random().Next(1, 4);
            var caseServeList = new List<CaseServeMapped>();

            for(int i = 0; i < numServes; i++) {
                var caseServeMapped = new CaseServeMapped
                {
                    Documents = await GetRandomDocMapped(),
                    Entity = TestHelpers.GetNewField(FieldType.PersonServed),
                    Notes = TestHelpers.GetRandomString(),
                    Server = await GetRandomUser(),
                    CreatedOn = DateTime.Now,
                    CreatedBy = 1
                };

                caseServeList.Add(caseServeMapped);
            }            

            var originalCase = new CaseMapped
            {
                CaseServeMappeds = caseServeList,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            var newCaseId = await CreateMapped(originalCase);
            var newCase = await GetMapped(newCaseId);

            var doesEqual = originalCase.CompareCase(newCase);
            Assert.True(doesEqual);
            Assert.Equal(originalCase.County, newCase.County);

            var rowsAffected = await HardDelete(newCaseId);
            Assert.Equal(1, rowsAffected);
        }

        // ------------------- Private Methods ----------------------------

        private async Task<int> CreateItem()
        {
            var item = TestHelpers.GetRandomCase();
            return await Create(item);
        }
    }
}
