﻿namespace ServitureTests.IntegrationTests
{
    using Newtonsoft.Json;
    using Serviture.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class TestBase<TEntity, TEntityMapped, TSearchRequest>
    {
        protected HttpClient Client;
        protected readonly ITestOutputHelper TestOutput;
        protected List<string> Columns { get; set; }
        protected List<string> SearchColumns { get; set; }

        public TestBase(TestFixture fixture, ITestOutputHelper testOutput)
        {
            Client = fixture.Client;
            TestOutput = testOutput;
        }

        public async Task TestCreateBaseAsync(int id)
        {
            Assert.NotEqual(0, id);

            var rowsAffected = await HardDelete(id);
            Assert.Equal(1, rowsAffected);
        }

        public async Task TestUpdateBaseAsync(int id)
        {
            var dict = await GetUpdateDictionary(id);

            foreach (var entry in dict)
            {
                Assert.NotEqual(entry.Key, entry.Value);
            }

            var rowsAffected = await HardDelete(id);
            Assert.Equal(1, rowsAffected);
        }

        public async Task TestDeleteBaseAsync(int id, TEntity deleteItem)
        {
            var rowsAffected = await Delete(deleteItem);
            var item = await Get(id);

            var isDeleted = typeof(TEntity).GetProperty("IsDeleted").GetValue(item);

            Assert.True((bool)isDeleted);
            Assert.Equal(1, rowsAffected);

            rowsAffected = await HardDelete(id);
            Assert.Equal(1, rowsAffected);
        }

        public async Task TestHardDeleteBaseAsync(int id)
        {
            var rowsAffected = await HardDelete(id);
            var item = await Get(id);

            Assert.Equal(1, rowsAffected);
            Assert.Null(item);
        }

        public async Task TestSearchAsync(int id, int daysBack = 0)
        {
            var item = await Get(id);

            foreach (var col in SearchColumns)
            {
                
                var searchRequest = Activator.CreateInstance<TSearchRequest>();

                var pageIndex = 0;
                object searchBy = new object(); 

                if (col.Equals("Id"))
                {
                    searchBy = new List<int> { id };
                } 
                else if(col.Equals("CreatedOn"))
                {
                    searchBy = typeof(TEntity).GetProperty(col).GetValue(item);
                    if(daysBack > 0)
                    {
                        typeof(TSearchRequest).GetProperty("DaysBack").SetValue(searchRequest, daysBack);
                    } 
                    else
                    {
                        typeof(TSearchRequest).GetProperty("SearchDate").SetValue(searchRequest, DateTime.Today.ToShortDateString());
                    }
                }
                else
                {
                    searchBy = typeof(TEntity).GetProperty(col).GetValue(item);
                }

                typeof(TSearchRequest).GetProperty(col).SetValue(searchRequest, searchBy);
                typeof(TSearchRequest).GetProperty("PageSize").SetValue(searchRequest, 100);
                typeof(TSearchRequest).GetProperty("PageIndex").SetValue(searchRequest, pageIndex);

                var hasMoreResults = false;
                var searchResults = new List<TEntity>();

                do
                {
                    var searchResponse = await Search(searchRequest);
                    if (searchResponse.Results != null)
                    {
                        searchResults.AddRange(searchResponse.Results);
                    }
                    hasMoreResults = searchResponse.HasMoreResults;
                    typeof(TSearchRequest).GetProperty("PageIndex").SetValue(searchRequest, ++pageIndex);
                } while (hasMoreResults);

                if(col.Equals("CreatedOn") && daysBack > 0)
                {
                    var greaterThanDaysBack = searchResults.Where(x => (DateTime)typeof(TEntity).GetProperty("CreatedOn").GetValue(x) > DateTime.Today.AddDays(daysBack));
                    Assert.True(greaterThanDaysBack.Count() == 0);
                } 
                //Put in else because if its just created it wont be in the daysBack query
                else
                {
                    var matchingItems = searchResults.Where(x => (int)typeof(TEntity).GetProperty("Id").GetValue(x) == id);
                    Assert.True(matchingItems.Count() > 0);
                }
            }

            var rowsAffected = await HardDelete(id);
            Assert.Equal(1, rowsAffected);
        }

        public async Task TestSearchMappedAsync(int id)
        {
            var item = await GetMapped(id);

            foreach (var col in SearchColumns)
            {

                var searchRequest = Activator.CreateInstance<TSearchRequest>();

                var pageIndex = 0;

                if (col.Equals("Id"))
                {
                    var searchBy = new List<int> { id };
                    typeof(TSearchRequest).GetProperty(col).SetValue(searchRequest, searchBy);
                }
                else
                {
                    var searchBy = typeof(TEntity).GetProperty(col).GetValue(item);
                    typeof(TSearchRequest).GetProperty(col).SetValue(searchRequest, searchBy);
                }

                typeof(TSearchRequest).GetProperty("PageSize").SetValue(searchRequest, 100);
                typeof(TSearchRequest).GetProperty("PageIndex").SetValue(searchRequest, pageIndex);

                var hasMoreResults = false;
                var searchResults = new List<TEntity>();

                do
                {
                    var searchResponse = await Search(searchRequest);
                    if (searchResponse.Results != null)
                    {
                        searchResults.AddRange(searchResponse.Results);
                    }
                    hasMoreResults = searchResponse.HasMoreResults;
                    typeof(TSearchRequest).GetProperty("PageIndex").SetValue(searchRequest, ++pageIndex);
                } while (hasMoreResults);

                var matchingItems = searchResults.Where(x => (int)typeof(TEntity).GetProperty("Id").GetValue(x) == id);

                Assert.True(matchingItems.Count() > 0);
            }

            var rowsAffected = await HardDelete(id);
            Assert.Equal(1, rowsAffected);
        }

        public async Task TestSearchTableAsync(string searchTerm, int id)
        {
            var item = await Get(id);
            var pageIndex = 0;

            var searchRequest = Activator.CreateInstance<TSearchRequest>();
            typeof(TSearchRequest).GetProperty("SearchTerm").SetValue(searchRequest, searchTerm);
            typeof(TSearchRequest).GetProperty("PageSize").SetValue(searchRequest, 100);
            typeof(TSearchRequest).GetProperty("PageIndex").SetValue(searchRequest, pageIndex);

            var hasMoreResults = false;
            var searchResults = new List<TEntity>();

            do
            {
                var searchResponse = await Search(searchRequest);
                if (searchResponse.Results != null)
                {
                    searchResults.AddRange(searchResponse.Results);
                }
                hasMoreResults = searchResponse.HasMoreResults;
                typeof(TSearchRequest).GetProperty("PageIndex").SetValue(searchRequest, ++pageIndex);
            } while (hasMoreResults);

            var matchingItems = searchResults.Where(x => (int)typeof(TEntity).GetProperty("Id").GetValue(x) == id);

            Assert.True(matchingItems.Count() > 0);

            var rowsAffected = await HardDelete(id);
            Assert.Equal(1, rowsAffected);
        }

        public async Task<int> Create(TEntity item)
        {
            var requestUrl = $"api/{typeof(TEntity).Name}";
            
            var stringContent = TestHelpers.GetStringContent(item);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var newId = JsonConvert.DeserializeObject<int>(jsonResponse);
            return newId;
        }

        public async Task<int> CreateMapped(TEntityMapped item)
        {
            var requestUrl = $"api/{typeof(TEntity).Name}/mapped";

            var stringContent = TestHelpers.GetStringContent(item);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var newId = JsonConvert.DeserializeObject<int>(jsonResponse);
            return newId;
        }

        public async Task<TEntity> Get(int id)
        {
            var request = $"/api/{typeof(TEntity).Name}/{id}";

            var response = await Client.GetAsync(request);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var item = JsonConvert.DeserializeObject<TEntity>(jsonResponse);
            return item;
        }

        public async Task<TEntityMapped> GetMapped(int id)
        {
            var request = $"/api/{typeof(TEntity).Name}/mapped/{id}";

            var response = await Client.GetAsync(request);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var item = JsonConvert.DeserializeObject<TEntityMapped>(jsonResponse);
            return item;
        }

        public async Task<int> Update(int id, TEntity updateObject)
        {
            var requestUrl = $"/api/{typeof(TEntity).Name}/{id}";

            var stringContent = TestHelpers.GetStringContent(updateObject);
            var response = await Client.PutAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var rowsAffected = JsonConvert.DeserializeObject<int>(jsonResponse);
            return rowsAffected;
        }

        public async Task<int> UpdateMapped(int id, TEntityMapped updateObject)
        {
            var requestUrl = $"/api/{typeof(TEntity).Name}/mapped/{id}";

            var stringContent = TestHelpers.GetStringContent(updateObject);
            var response = await Client.PutAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var rowsAffected = JsonConvert.DeserializeObject<int>(jsonResponse);
            return rowsAffected;
        }

        public async Task<Dictionary<object,object>> GetUpdateDictionary(int newId)
        {
            var dict = new Dictionary<object, object>();

            var originalItem = await Get(newId);
            var item = await Get(newId);

            foreach (var col in Columns)
            {
                if (!col.Equals("CreatedOn"))
                {
                    var itemUpdate = UpdateColumn(item, col);
                    var rowAffected = await Update(newId, itemUpdate);

                    var updatedItem = await Get(newId);

                    var original = typeof(TEntity).GetProperty(col).GetValue(originalItem);
                    var updated = typeof(TEntity).GetProperty(col).GetValue(updatedItem);

                    //key cannot be null
                    dict[updated] = original;
                }                
            }

            return dict;
        }

        public async Task<int> Delete(TEntity deleteObject)
        {
            var requestUrl = $"/api/{typeof(TEntity).Name}/delete";

            var stringContent = TestHelpers.GetStringContent(deleteObject);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var rowsAffected = JsonConvert.DeserializeObject<int>(jsonResponse);
            return rowsAffected;
        }

        public async Task<int> HardDelete(int id)
        {
            var request = $"/api/{typeof(TEntity).Name}/{id}";

            var response = await Client.DeleteAsync(request);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var rowsAffected = JsonConvert.DeserializeObject<int>(jsonResponse);
            return rowsAffected;
        }

        public async Task<SearchResponse<TEntity>> Search(TSearchRequest searchRequest)
        {
            var requestUrl = $"/api/{typeof(TEntity).Name}/search";

            var stringContent = TestHelpers.GetStringContent(searchRequest);

            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var searchResponse = JsonConvert.DeserializeObject<SearchResponse<TEntity>>(jsonResponse);

            return searchResponse;
        }

        public async Task<SearchResponse<TEntityMapped>> SearchMapped(TSearchRequest searchRequest)
        {
            var requestUrl = $"/api/{typeof(TEntity).Name}/search";

            var stringContent = TestHelpers.GetStringContent(searchRequest);

            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var searchResponse = JsonConvert.DeserializeObject<SearchResponse<TEntityMapped>>(jsonResponse);

            return searchResponse;
        }

        public async Task<Field> GetRandomField(FieldType fieldType)
        {
            var requestUrl = $"api/Field/random";

            var stringContent = TestHelpers.GetStringContent(fieldType);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var field = JsonConvert.DeserializeObject<Field>(jsonResponse);
            return field;
        }

        public async Task<Client> GetRandomClient()
        {
            var requestUrl = $"api/Client/random";

            var stringContent = TestHelpers.GetStringContent(string.Empty);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var client = JsonConvert.DeserializeObject<Client>(jsonResponse);
            return client;
        }

        private TEntity UpdateColumn(TEntity item, string col)
        {
            var colObj = typeof(TEntity).GetProperty(col).GetValue(item);
            var colType = typeof(TEntity).GetProperty(col).PropertyType;


            if(colType == typeof(string))
            {
                typeof(TEntity).GetProperty(col).SetValue(item, "updated");
                return item;
            } 
            else if (colType == typeof(Int32) || colType == typeof(Int32?))
            {
                if(colObj == null)
                {
                    typeof(TEntity).GetProperty(col).SetValue(item, 1);
                } 
                else
                {
                    typeof(TEntity).GetProperty(col).SetValue(item, (int)colObj + 1);
                }

                return item;
            }
            else if (colType == typeof(bool) || colType == typeof(bool?))
            {
                if (colObj == null)
                {
                    typeof(TEntity).GetProperty(col).SetValue(item, false);
                }
                else
                {
                    typeof(TEntity).GetProperty(col).SetValue(item, !(bool)colObj);
                }

                return item;
            }
            else if (colType == typeof(DateTime) || colType == typeof(DateTime?))
            {
                typeof(TEntity).GetProperty(col).SetValue(item, DateTime.Now);
                return item;
            }
            else if (colType == typeof(DateTimeOffset) || colType == typeof(DateTimeOffset?))
            {
                typeof(TEntity).GetProperty(col).SetValue(item, DateTimeOffset.Now);
                return item;
            }
            else if (colType == typeof(decimal) || colType == typeof(decimal?))
            {
                if (colObj == null)
                {
                    typeof(TEntity).GetProperty(col).SetValue(item, (decimal)1);
                }
                else
                {
                    typeof(TEntity).GetProperty(col).SetValue(item, (decimal)colObj + 1);
                }

                return item;
            }
            else
            {
                return default;
            }
        }

        protected async Task<int> CreateCase()
        {
            var item = TestHelpers.GetRandomCase();
            var requestUrl = $"api/case";

            var stringContent = TestHelpers.GetStringContent(item);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var newId = JsonConvert.DeserializeObject<int>(jsonResponse);
            return newId;
        }

        protected async Task<int> CreateCaseMapped()
        {
            var item = await GetRandomCaseMapped();
            var requestUrl = $"api/case/mapped";

            var stringContent = TestHelpers.GetStringContent(item);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var newId = JsonConvert.DeserializeObject<int>(jsonResponse);
            return newId;
        }

        protected async Task<int> CreateServe(int caseId)
        {
            var item = TestHelpers.GetRandomServe(caseId);
            var requestUrl = $"api/serve";

            var stringContent = TestHelpers.GetStringContent(item);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var newId = JsonConvert.DeserializeObject<int>(jsonResponse);
            return newId;
        }

        protected async Task<int> CreateServeMapped(int caseId)
        {
            var item = TestHelpers.GetRandomServeMapped(caseId);

            var dbsMappedList = new List<DocumentByServeMapped>();

            for (int i = 0; i < new Random().Next(4); i++)
            {
                dbsMappedList.AddRange(await GetRandomDocMapped());
            }

            item.Documents = dbsMappedList;

            var requestUrl = $"api/serve/mapped";

            var stringContent = TestHelpers.GetStringContent(item);
            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var newId = JsonConvert.DeserializeObject<int>(jsonResponse);
            return newId;
        }

        protected async Task<ServeMapped> GetServeMapped(int serveId)
        {
            var request = $"/api/serve/mapped/{serveId}";

            var response = await Client.GetAsync(request);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var item = JsonConvert.DeserializeObject<ServeMapped>(jsonResponse);
            return item;
        }

        protected async Task<int> HardDelete(string entityName, int id)
        {
            var request = $"/api/{entityName}/{id}";

            var response = await Client.DeleteAsync(request);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var rowsAffected = JsonConvert.DeserializeObject<int>(jsonResponse);
            return rowsAffected;
        }

        protected async Task<User> GetRandomUser()
        {
            var requestUrl = $"api/user";

            var response = await Client.GetAsync(requestUrl);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var item = JsonConvert.DeserializeObject<List<User>>(jsonResponse);
            var num = new Random().Next(item.Count);

            return item[num];
        }

        protected async Task<List<DocumentByServeMapped>> GetRandomDocMapped()
        {
            var requestUrl = $"api/document";

            var response = await Client.GetAsync(requestUrl);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var docs = JsonConvert.DeserializeObject<List<Document>>(jsonResponse);
            var num = new Random().Next(docs.Count);
            var doc = docs[num];

            var docMapped = new DocumentByServeMapped
            {
                DocumentId = doc.Id,
                DocumentText = doc.HasText ? TestHelpers.GetNewField(FieldType.DocOther) : null
            };

            var docMappedList = new List<DocumentByServeMapped> { docMapped };
            return docMappedList;
        }

        protected async Task<ReasonByAffidavit> GetRandomReason()
        {
            var requestUrl = $"api/reason";

            var response = await Client.GetAsync(requestUrl);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var reasons = JsonConvert.DeserializeObject<List<Reason>>(jsonResponse);
            var num = new Random().Next(reasons.Count);
            var reason = reasons[num];

            var reasonByAffidavitList = new ReasonByAffidavit { ReasonId = reason.Id };
            return reasonByAffidavitList;
        }

        protected async Task <CaseMapped> GetRandomCaseMapped()
        {
            var numServes = new Random().Next(1, 4);
            var caseServeList = new List<CaseServeMapped>();

            for (int i = 0; i < numServes; i++)
            {
                var caseServeMapped = new CaseServeMapped
                {
                    Documents = await GetRandomDocMapped(),
                    Entity = TestHelpers.GetNewField(FieldType.PersonServed),
                    Notes = TestHelpers.GetRandomString(),
                    DueDate = DateTimeOffset.Now.Date.ToShortDateString(),
                    Server = await GetRandomUser(),
                    CreatedOn = DateTime.Now,
                    CreatedBy = 1
                };

                caseServeList.Add(caseServeMapped);
            }

            var item = new CaseMapped
            {
                CaseName = TestHelpers.GetNewField(FieldType.CaseName),
                CaseNumber = TestHelpers.GetNewField(FieldType.CaseNumber),
                Client = TestHelpers.GetNewClient(),
                Contact = TestHelpers.GetNewField(FieldType.Contact),
                CourtDate = DateTime.Now.ToString(),
                ClientFileNumber = TestHelpers.GetNewField(FieldType.ClientFileNumber),
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                CaseServeMappeds = caseServeList
            };

            var randLoc = new Random().Next(2);

            if(randLoc == 1)
            {
                item.County = "Milwaukee";
                item.State = "Wisconsin";
            } 
            else
            {
                item.OtherLocation = TestHelpers.GetNewField(FieldType.OtherLocation);
            }

            var randSingleCase = new Random().Next(2);

            if(randSingleCase == 1)
            {
                item.Defendant = TestHelpers.GetNewField(FieldType.CaseName);
            }

            return item;
        }

        public async Task<Field> GetOfficerRelationship()
        {
            var searchRequest = new FieldSearchRequest { Value = "Officer", FieldType = (int)FieldType.Relationship, PageIndex = 0, PageSize = 1 };
            var requestUrl = $"/api/Field/search";

            var stringContent = TestHelpers.GetStringContent(searchRequest);

            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var searchResponse = JsonConvert.DeserializeObject<SearchResponse<Field>>(jsonResponse);

            return searchResponse.Results.FirstOrDefault();
        }

        public async Task<Field> GetPersonRelationship()
        {
            var searchRequest = new FieldSearchRequest { Value = "Person", FieldType = (int)FieldType.Relationship, PageIndex = 0, PageSize = 1 };
            var requestUrl = $"/api/Field/search";

            var stringContent = TestHelpers.GetStringContent(searchRequest);

            var response = await Client.PostAsync(requestUrl, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            var searchResponse = JsonConvert.DeserializeObject<SearchResponse<Field>>(jsonResponse);

            return searchResponse.Results.FirstOrDefault();
        }
    }
}
