﻿namespace ServitureTests.IntegrationTests
{
    using Serviture.Models;
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Xunit;
    using Xunit.Abstractions;

    public class UserTests : TestBase<User, object, UserSearchRequest>, IClassFixture<TestFixture>
    {
        public UserTests(TestFixture fixture, ITestOutputHelper testOutput) : base(fixture, testOutput)
        {
            Columns = User.Columns.ToList();
            SearchColumns = UserSearchRequest.Columns.ToList();
        }

        [Fact]
        public async Task TestCreateAsync()
        {
            var id = await CreateItem();
            await TestCreateBaseAsync(id);
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            var id = await CreateItem();
            await TestUpdateBaseAsync(id);
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            var id = await CreateItem();
            var deleteItem = new User { Id = id };

            await TestDeleteBaseAsync(id, deleteItem);
        }

        [Fact]
        public async Task TestHardDeleteAsync()
        {
            var id = await CreateItem();
            await TestHardDeleteBaseAsync(id);
        }

        [Fact]
        public async Task TestSearchUserAsync()
        {
            var id = await CreateItem();
            await TestSearchAsync(id);
        }

        private async Task<int> CreateItem()
        {
            var user = new User
            {
                CanEdit = true,
                CompanyId = 1,
                Email = "test@test.com",
                EntityName = "Entity Name",
                FirstName = "First",
                Password = "password",
                LastName = "Last",
                IsAdmin = true,
                IsDeleted = false,
                Token = "token",
                UserName = "Username",
                UserType = (int)UserType.Serviture,
                CreatedBy = 1,
                CreatedOn = DateTime.Now
            };

            return await Create(user);
        }
    }
}
