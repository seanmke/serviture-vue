﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Serviture.Data;
    using Serviture.Models;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]")]
    [ApiController]
    public class FieldController : ControllerBase
    {
        public IFieldData FieldData { get; }
        readonly ILogger<FieldController> _logger;

        public FieldController(IFieldData fieldData, ILogger<FieldController> logger)
        {
            FieldData = fieldData ?? throw new ArgumentNullException(nameof(fieldData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        
        [HttpGet("{id}")]
        public async Task<Field> Get(int id)
        {
            return await FieldData.GetField(id);
        }

        [HttpPost]
        public async Task<int> Post([FromBody] Field field)
        {
            return await FieldData.CreateField(field);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] Field field)
        {
            return await FieldData.UpdateField(id, field);
        }

        [HttpPost("delete")]
        public async Task<int> DeleteField ([FromBody] Field field)
        {
            return await FieldData.DeleteField(field.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            return await FieldData.HardDeleteField(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Field>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<Field>> SearchFields([FromBody]FieldSearchRequest searchRequest)
        {
            return await FieldData.SearchFields(searchRequest);
        }

        [HttpPost("random")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<Field> GetRandomField([FromBody]FieldType constraint)
        {
            return await FieldData.GetRandomRecord(constraint);
        }
    }
}
