﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Serviture.Data;
    using Serviture.Models;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        public IAddressData AddressData { get; }
        readonly ILogger<AddressController> _logger;

        public AddressController(IAddressData addressData, ILogger<AddressController> logger)
        {
            AddressData = addressData ?? throw new ArgumentNullException(nameof(addressData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("{id}")]
        public async Task<Address> Get(int id)
        {
            return await AddressData.GetAddress(id);
        }

        [HttpPost]
        public async Task<int> Post([FromBody] Address address)
        {
            return await AddressData.CreateAddress(address);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] Address address)
        {
            return await AddressData.UpdateAddress(id, address);
        }

        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            return await AddressData.HardDeleteAddress(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Address>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<Address>> SearchAddresses([FromBody]AddressSearchRequest searchRequest)
        {
            return await AddressData.SearchAddresses(searchRequest);
        }
    }
}
