﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class QuickAttemptController : ControllerBase
    {
        public IQuickAttemptData QuickAttemptData { get; }
        readonly ILogger<QuickAttemptController> _logger;

        public QuickAttemptController(IQuickAttemptData quickAttemptData, ILogger<QuickAttemptController> logger)
        {
            QuickAttemptData = quickAttemptData ?? throw new ArgumentNullException(nameof(quickAttemptData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("{id}")]
        public async Task<QuickAttempt> Get(int id)
        {
            return await QuickAttemptData.GetQuickAttempt(id);
        }

        [HttpGet]
        public async Task<List<QuickAttempt>> Get()
        {
            return await QuickAttemptData.GetQuickAttempts();
        }

        [HttpPost]
        public async Task<int> Post([FromBody] QuickAttempt quickAttempt)
        {
            return await QuickAttemptData.CreateQuickAttempt(quickAttempt);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] QuickAttempt quickAttempt)
        {
            return await QuickAttemptData.UpdateQuickAttempt(id, quickAttempt);
        }

        [HttpPost("delete")]
        public async Task<int> Delete([FromBody] QuickAttempt quickAttempt)
        {
            return await QuickAttemptData.DeleteQuickAttempt(quickAttempt.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await QuickAttemptData.HardDeleteQuickAttempt(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Field>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<QuickAttempt>> SearchQuickAttempts([FromBody]QuickAttemptSearchRequest searchRequest)
        {
            return await QuickAttemptData.SearchQuickAttempts(searchRequest);
        }
    }
}