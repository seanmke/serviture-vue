﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class ContractorUserController : ControllerBase
    {
        public IContractorUserData ContractorUserData { get; }
        readonly ILogger<ContractorUserController> _logger;

        public ContractorUserController(IContractorUserData contractorUserData, ILogger<ContractorUserController> logger)
        {
            ContractorUserData = contractorUserData ?? throw new ArgumentNullException(nameof(contractorUserData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("{id}")]
        public async Task<ContractorUser> Get(int id)
        {
            return await ContractorUserData.GetContractorUser(id);
        }

        [HttpPost]
        public async Task<int> Post([FromBody] ContractorUser contractorUser)
        {
            return await ContractorUserData.CreateContractorUser(contractorUser);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] ContractorUser contractorUser)
        {
            return await ContractorUserData.UpdateContractorUser(id, contractorUser);
        }

        [HttpPost("upsert/{contractorId}")]
        public async Task<List<int>> Upsert(int contractorId, [FromBody] List<ContractorUser> items)
        {
            var ids = new List<int>();
            try
            {
                var searchResponse = await SearchContractorUsers(new ContractorUserSearchRequest { ContractorId = contractorId, PageIndex = 0, PageSize = 50 });

                foreach (var item in items)
                {
                    var userInDb = searchResponse.Results.Where(cu => cu.Name.Equals(item.Name)).FirstOrDefault();

                    //Upsert user doesnt exist in DB, so create new
                    if (userInDb == null)
                    {
                        var contractorUser = new ContractorUser() { ContractorId = contractorId, Name = item.Name, IsDeleted = false };
                        var id = await ContractorUserData.CreateContractorUser(contractorUser);
                        ids.Add(id);
                    }
                    else
                    {
                        //If they exist but are deleted, flip the flag
                        if (userInDb.IsDeleted)
                        {
                            userInDb.IsDeleted = false;
                            await ContractorUserData.UpdateContractorUser(userInDb.Id, userInDb);
                        }
                    }
                }

                //Now check if there are any users in the db, that are no longer in the upserted user list
                foreach(var dbUser in searchResponse.Results)
                {
                    var userInUpsert = items.Where(i => i.Name.Equals(dbUser.Name)).FirstOrDefault();

                    if(userInUpsert == null)
                    {
                        dbUser.IsDeleted = true;
                        await ContractorUserData.UpdateContractorUser(dbUser.Id, dbUser);
                    }
                }

                return ids;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ids;
            }
        }

        [HttpPost("delete")]
        public async Task<int> Delete([FromBody] ContractorUser contractorUser)
        {
            return await ContractorUserData.DeleteContractorUser(contractorUser.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await ContractorUserData.HardDeleteContractorUser(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<ContractorUser>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<ContractorUser>> SearchContractorUsers([FromBody]ContractorUserSearchRequest searchRequest)
        {
            return await ContractorUserData.SearchContractorUsers(searchRequest);
        }
    }
}