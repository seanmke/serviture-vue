﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Serviture.Data;
    using Serviture.Models;
    using Microsoft.Extensions.Logging;
    using Microsoft.AspNetCore.Authorization;

    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public IUserData UserData { get; }
        readonly ILogger<UserController> _logger;

        public UserController(IUserData userData, ILogger<UserController> logger)
        {
            UserData = userData ?? throw new ArgumentNullException(nameof(userData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]

        public async Task<IActionResult> Authenticate([FromBody]AuthenticateModel model)
        {
            try
            {
                _logger.LogInformation($"Attempting log in for {model.Username}");
                var user = await UserData.Authenticate(model);

                if (user == null)
                    return BadRequest(new { message = "Username or password is incorrect" });


                user.LastLoggedIn = DateTime.UtcNow;
                user.Password = null;
                await UserData.UpdateUser(user.Id, user);

                return Ok(user);
            } 
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
            
        }

        [HttpGet("{id}")]
        public async Task<User> GetUser(int id)
        {
            return await UserData.GetUser(id);
        }

        [HttpGet]
        public async Task<List<User>> GetUsers()
        {
            return await UserData.GetUsers();
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<User>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<User>> SearchUsers([FromBody]UserSearchRequest searchRequest)
        {
            return await UserData.SearchUsers(searchRequest);
        }

        [HttpPost]
        public async Task<int> Post([FromBody] User user)
        {
            return await UserData.CreateUser(user);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] User user)
        {
            return await UserData.UpdateUser(id, user);
        }

        [HttpPut("lastaction/{id}")]
        public async Task<int> PutAction(int id)
        {
            if(id > 0)
            {
                var user = await UserData.GetUser(id);
                if(user != null)
                {
                    user.LastActionOn = DateTime.UtcNow;
                    user.Password = null;
                    return await UserData.UpdateUser(id, user);
                }
            }

            return 0;
        }

        [HttpPost("delete")]
        public async Task<int> DeleteUser([FromBody] User user)
        {
            return await UserData.DeleteUser(user.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            return await UserData.HardDeleteUser(id);
        }
    }
}