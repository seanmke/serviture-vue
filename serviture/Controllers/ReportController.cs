﻿namespace Serviture.Controllers
{
    using DinkToPdf;
    using DinkToPdf.Contracts;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Serviture.Data;
    using Serviture.Helpers;
    using Serviture.Models;
    using Serviture.Pages.Templates;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        public IReportData ReportData { get; }

        public IRazorToStringRenderer RazorToStringRenderer { get; }

        public ICSAHelper CSAHelper { get; }

        private readonly IConverter _converter;

        public ReportController(IReportData reportData, ICSAHelper csaHelper, IRazorToStringRenderer razorToStringRenderer, IConverter converter)
        {
            ReportData = reportData ?? throw new ArgumentNullException(nameof(reportData));
            CSAHelper = csaHelper ?? throw new ArgumentNullException(nameof(csaHelper));
            RazorToStringRenderer = razorToStringRenderer ?? throw new ArgumentNullException(nameof(razorToStringRenderer));
            _converter = converter ?? throw new ArgumentNullException(nameof(converter));
        }

        [HttpPost("billingreport")]
        [ProducesResponseType(typeof(BillingReport), StatusCodes.Status200OK)]
        public async Task<BillingReport> RunBillingReport([FromBody]BillingReportSearchRequest searchRequest)
        {
            return await ReportData.RunBillingReport(searchRequest);
        }

        [HttpPost("serveattemptsummary")]
        [ProducesResponseType(typeof(List<ReportServeAttemptSummary>), StatusCodes.Status200OK)]
        public async Task<List<ReportServeAttemptSummary>> RunServeAttemptSummaryReport([FromBody] ReportServeAttemptSummaryRequest searchRequest)
        {
            return await ReportData.RunServeAttemptSummaryReport(searchRequest);
        }

        [HttpGet("attemptreport/{serveId}")]
        public async Task<IActionResult> CreateAttemptReport(int serveId)
        {
            try
            {
                var attemptReports = await CSAHelper.GetAttemptReport(serveId);
                var model = new PrintAttemptsAndFilesModel(attemptReports);

                var htmlString = await RazorToStringRenderer.RenderPartialToStringAsync("Pages/Templates/PrintAttemptsAndFiles.cshtml", model);

                var doc = new HtmlToPdfDocument()
                {
                    GlobalSettings = {
                        PaperSize = PaperKind.A4,
                        Orientation = Orientation.Portrait,
                        DocumentTitle = $"Attempts Report {DateTime.Now}"
                    },
                    Objects = { new ObjectSettings() { HtmlContent = htmlString } }
                };

                var pdfFile = _converter.Convert(doc);

                return File(pdfFile, "application/pdf");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
