﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DinkToPdf;
    using DinkToPdf.Contracts;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Helpers;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class AffidavitController : ControllerBase
    {
        private IWebHostEnvironment _env;
        public IRazorToStringRenderer RazorToStringRenderer { get; }
        public ICSAHelper CSAHelper { get; }
        public IAffidavitData AffidavitData { get; }
        readonly ILogger<AffidavitController> _logger;
        private readonly IConverter _converter;

        public AffidavitController(IAffidavitData clientData, ILogger<AffidavitController> logger, ICSAHelper csaHelper, IRazorToStringRenderer razorToStringRenderer, IConverter converter, IWebHostEnvironment env)
        {
            AffidavitData = clientData ?? throw new ArgumentNullException(nameof(clientData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            CSAHelper = csaHelper ?? throw new ArgumentNullException(nameof(csaHelper));
            RazorToStringRenderer = razorToStringRenderer ?? throw new ArgumentNullException(nameof(razorToStringRenderer));
            _converter = converter ?? throw new ArgumentNullException(nameof(converter));
            _env = env ?? throw new ArgumentNullException(nameof(env));
        }

        [HttpGet("{id}")]
        public async Task<Affidavit> Get(int id)
        {
            var affidavit = await AffidavitData.GetAffidavit(id);
            return affidavit;
        }

        [HttpGet("mapped/{id}")]
        public async Task<AffidavitMapped> GetMapped(int id)
        {
            var affidavitMapped = await CSAHelper.GetAffidavitMapped(id);
            return affidavitMapped;
        }

        [HttpPost]
        public async Task<int> Post([FromBody] Affidavit item)
        {
            var affidavitId = await AffidavitData.CreateAffidavit(item);
            return affidavitId;
        }

        /// <summary>
        /// The only time you would ever be able to create an affidavit is
        /// when completing a serve.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost("mapped")]
        public async Task<int> PostMapped([FromBody] AffidavitMapped item)
        {
            //Still add a check to make sure swagger or something else cant fuck with the data

            //Should we be checking tokens here?

            var serveMapped = await CSAHelper.GetServeMapped(item.ServeId.Value);
            item.SetServeMapped(serveMapped);

            var affidavit = await CSAHelper.GetAffidavitFromAffidavitMapped(item);
            var affidavitId = await AffidavitData.CreateAffidavit(affidavit);
            
            // Add the ReasonsByAffidavit.  We don't need to upsert the 
            // Documents/Addresses/Attempts because they cant be edited
            // between the serve being completed and the affidavit being 
            // created.
            if (item.Reasons != null && item.Reasons.Count > 0)
            {
                item.Reasons.ForEach(rba => rba.AffidavitId = affidavitId);
                await CSAHelper.UpsertReasonByAffidavit(item.Reasons);
            }

            // Mark Serve as complete so the serve isnt returned from searching
            await CSAHelper.UpdateServeAsComplete(item.ServeId.Value);

            return affidavitId;
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] Affidavit item)
        {
            return await AffidavitData.UpdateAffidavit(id, item);
        }

        [HttpPut("mapped/{id}")]
        public async Task<int> PutMapped(int id, [FromBody] AffidavitMapped item)
        {
            var affidavit = await CSAHelper.GetAffidavitFromAffidavitMapped(item);
            await CSAHelper.UpsertByAffidavitMapped(item, id);

            return await AffidavitData.UpdateAffidavit(id, affidavit);
        }

        [HttpPost("delete")]
        public async Task<int> Delete([FromBody] Affidavit client)
        {
            return await AffidavitData.DeleteAffidavit(client.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            //Update to handle the ByServe stuff
            return await CSAHelper.HardDeleteAffidavit(id);
        }

        [HttpPost("search")]
        public async Task<SearchResponse<AffidavitMapped>> SearchAffidavits([FromBody]AffidavitSearchRequest searchRequest)
        {
            var searchResponse = new SearchResponse<AffidavitMapped>();
            var mappedAffidavits = new List<AffidavitMapped>();

            if (searchRequest.SearchUser != null)
            {
                searchRequest.ServerId = searchRequest.SearchUser > 0 ? searchRequest.SearchUser : null;
            }

            var affidavits = new SearchResponse<Affidavit>();

            searchRequest.OrderBy = "CreatedOn desc";
            affidavits = await AffidavitData.SearchAffidavits(searchRequest);

            if(affidavits != null)
            {
                foreach (var affidavit in affidavits.Results)
                {
                    var mappedAffidavit = await CSAHelper.GetAffidavitMappedFromAffidavit(affidavit);
                    mappedAffidavits.Add(mappedAffidavit);
                }

                searchResponse.HasMoreResults = affidavits.HasMoreResults;
                searchResponse.TotalResults = affidavits.TotalResults;
                searchResponse.Results = mappedAffidavits;
            }

            return searchResponse;
        }

        [HttpGet("report/{id}")]
        public async Task<IActionResult> CreatePdf(int id, bool isPrintVersion)
        {
            try
            {
                var affidavit = await CSAHelper.GetAffidavitMapped(id);
                var model = new AffidavitTemplateModel(affidavit);
                model.IsPrintVersion = isPrintVersion;

                var htmlString = await RazorToStringRenderer.RenderPartialToStringAsync("Pages/Templates/AffidavitTemplate.cshtml", model);

                if (isPrintVersion)
                {                    
                    var ccModel = new AffidavitTemplateClientCopyModel(affidavit);
                    ccModel.MultiPage = true;
                    htmlString += await RazorToStringRenderer.RenderPartialToStringAsync("Pages/Templates/AffidavitTemplateClientCopy.cshtml", ccModel);
                    var scModel = new AffidavitTemplateServerCopyModel(affidavit);
                    htmlString += await RazorToStringRenderer.RenderPartialToStringAsync("Pages/Templates/AffidavitTemplateServerCopy.cshtml", scModel);
                }

                var doc = new HtmlToPdfDocument()
                {
                    GlobalSettings = {
                        PaperSize = PaperKind.A4,
                        Orientation = Orientation.Portrait,
                        DocumentTitle = $"Affidavit {affidavit.InvoiceNumber}"
                    },
                    Objects = { new ObjectSettings() { HtmlContent = htmlString }}
                };

                var pdfFile = _converter.Convert(doc);

                return File(pdfFile, "application/pdf");
            } 
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }            
        }

        [HttpGet("clientcopy/{id}")]
        public async Task<IActionResult> CreateClientPdf(int id)
        {
            try
            {
                var affidavit = await CSAHelper.GetAffidavitMapped(id);
                var model = new AffidavitTemplateClientCopyModel(affidavit);
                model.MultiPage = false;
                var htmlString = await RazorToStringRenderer.RenderPartialToStringAsync("Pages/Templates/AffidavitTemplateClientCopy.cshtml", model);

                var doc = new HtmlToPdfDocument()
                {
                    GlobalSettings = {
                        PaperSize = PaperKind.A4,
                        Orientation = Orientation.Portrait,
                        DocumentTitle = $"Affidavit {affidavit.InvoiceNumber}"
                    },
                    Objects = { new ObjectSettings() { HtmlContent = htmlString } }
                };

                var pdfFile = _converter.Convert(doc);

                return File(pdfFile, "application/pdf");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}