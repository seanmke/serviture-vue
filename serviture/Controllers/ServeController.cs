﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Serviture.Data;
    using Serviture.Helpers;
    using Serviture.Models;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]")]
    [ApiController]
    public class ServeController : ControllerBase
    {
        public IServeData ServeData { get; }
        public ICSAHelper CSAHelper { get; }
        public IFieldHelper FieldHelper { get; }
        readonly ILogger<ServeController> _logger;

        public ServeController(IServeData serveData, ICSAHelper csaHelper, IFieldHelper fieldHelper, ILogger<ServeController> logger)
        {
            ServeData = serveData ?? throw new ArgumentNullException(nameof(serveData));
            CSAHelper = csaHelper ?? throw new ArgumentNullException(nameof(csaHelper));
            FieldHelper = fieldHelper ?? throw new ArgumentNullException(nameof(fieldHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("{id}")]
        public async Task<Serve> Get(int id)
        {
            return await ServeData.GetServe(id);
        }

        [HttpGet("mapped/{id}")]
        public async Task<ServeMapped> GetMapped(int id)
        {
            var serveMapped = await CSAHelper.GetServeMapped(id);
            return serveMapped;
        }

        [HttpPost]
        public async Task<int> Post([FromBody] Serve serve)
        {
            return await ServeData.CreateServe(serve);
        }

        [HttpPost("mapped")]
        public async Task<int> PostMapped([FromBody] ServeMapped serveMapped)
        {
            var serve = await CSAHelper.GetServeFromServeMapped(serveMapped);
            var serveId = await ServeData.CreateServe(serve);

            await CSAHelper.UpsertAddresses(serveMapped.Addresses, serveId);
            await CSAHelper.UpsertDocumentsByServeMapped(serveMapped.Documents, serveId);
            await CSAHelper.UpsertAttempts(serveMapped.Attempts, serveId);

            return serveId;
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] Serve serve)
        {
            return await ServeData.UpdateServe(id, serve);
        }

        [HttpPut("mapped/{id}")]
        public async Task<int> PutMapped(int id, [FromBody] ServeMapped serveMapped)
        {
            var serve = await CSAHelper.GetServeFromServeMapped(serveMapped);

            await CSAHelper.UpsertAddresses(serveMapped.Addresses, id);
            await CSAHelper.UpsertDocumentsByServeMapped(serveMapped.Documents, id);
            await CSAHelper.UpsertAttempts(serveMapped.Attempts, id);

            return await ServeData.UpdateServe(id, serve);
        }

        [HttpPost("delete")]
        public async Task<int> DeleteServe([FromBody] Serve serve)
        {
            return await ServeData.DeleteServe(serve.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            return await CSAHelper.HardDeleteServe(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<ServeMapped>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<ServeMapped>> SearchServes([FromBody]ServeSearchRequest searchRequest)
        {
            var searchResponse = new SearchResponse<ServeMapped>();
            var mappedServes = new List<ServeMapped>();

            if(searchRequest.SearchUser != null)
            {
                searchRequest.ServerId = searchRequest.SearchUser > 0 ? searchRequest.SearchUser : null;
            }

            var serves = new SearchResponse<Serve>();

            searchRequest.ServeStatus = (int)ServeStatus.InProcess;

            searchRequest.OrderBy = "CreatedOn desc";
            serves = await ServeData.SearchServes(searchRequest);

            if (serves != null)
            {
                foreach(var serve in serves.Results)
                {
                    var mappedServe = await CSAHelper.GetServeMappedFromServe(serve);
                    mappedServes.Add(mappedServe);
                }

                searchResponse.HasMoreResults = serves.HasMoreResults;
                searchResponse.TotalResults = serves.TotalResults;
                searchResponse.Results = mappedServes;
            }
            

            return searchResponse;
        }
    }
}
