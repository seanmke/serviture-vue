﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class ReasonController : ControllerBase
    {
        public IReasonData ReasonData { get; }
        readonly ILogger<ReasonController> _logger;

        public ReasonController(IReasonData reasonData, ILogger<ReasonController> logger)
        {
            ReasonData = reasonData ?? throw new ArgumentNullException(nameof(reasonData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("{id}")]
        public async Task<Reason> Get(int id)
        {
            return await ReasonData.GetReason(id);
        }

        [HttpGet]
        public async Task<List<Reason>> Get()
        {
            return await ReasonData.GetReasons();
        }

        [HttpPost]
        public async Task<int> Post([FromBody] Reason reason)
        {
            return await ReasonData.CreateReason(reason);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] Reason reason)
        {
            return await ReasonData.UpdateReason(id, reason);
        }

        [HttpPost("delete")]
        public async Task<int> Delete([FromBody] Reason reason)
        {
            return await ReasonData.DeleteReason(reason.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await ReasonData.HardDeleteReason(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Field>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<Reason>> SearchReasons([FromBody]ReasonSearchRequest searchRequest)
        {
            return await ReasonData.SearchReasons(searchRequest);
        }
    }
}