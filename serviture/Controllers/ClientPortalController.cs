﻿namespace Serviture.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class ClientPortalController : ControllerBase
    {
        public IClientPortalData ClientPortalData { get; }
        readonly ILogger<AffidavitController> _logger;

        public ClientPortalController(IClientPortalData clientPortalData, ILogger<AffidavitController> logger)
        {
            ClientPortalData = clientPortalData ?? throw new ArgumentNullException(nameof(clientPortalData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        

        [HttpPost("search")]
        public async Task<SearchResponse<ClientPortalRow>> SearchClientPortal([FromBody]ClientPortalSearchRequest searchRequest)
        {
            var searchResponse = await ClientPortalData.SearchClientPortal(searchRequest);
            return searchResponse;
        }
    }
}