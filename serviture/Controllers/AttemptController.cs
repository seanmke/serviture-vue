﻿namespace Serviture.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Helpers;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class AttemptController : ControllerBase
    {
        public IDocumentData DocumentData { get; }
        public IAttemptData AttemptData { get; }
        public IFieldHelper FieldHelper { get; }

        readonly ILogger<AttemptController> _logger;

        public AttemptController(IDocumentData documentData, IAttemptData attemptData, IFieldHelper fieldHelper, ILogger<AttemptController> logger)
        {
            DocumentData = documentData ?? throw new ArgumentNullException(nameof(documentData));
            AttemptData = attemptData ?? throw new ArgumentNullException(nameof(attemptData));
            FieldHelper = fieldHelper ?? throw new ArgumentNullException(nameof(fieldHelper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        public async Task<int> Post([FromBody] Attempt item)
        {
            return await AttemptData.CreateAttempt(item);
        }

        [HttpPost("mapped")]
        public async Task<int> PostMapped([FromBody] AttemptMapped item)
        {
            var attempt = new Attempt(item);

            if (item.OtherInfo != null && item.OtherInfo.Id == 0)
            {
                await FieldHelper.CreateField(item.OtherInfo);
            }

            return await AttemptData.CreateAttempt(attempt);
        }

        [HttpGet("{id}")]
        public async Task<Attempt> Get(int id)
        {
            return await AttemptData.GetAttempt(id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await AttemptData.HardDeleteAttempt(id);
        }

        [HttpPost("search")]
        public async Task<SearchResponse<Attempt>> SearchAttempts([FromBody]AttemptSearchRequest searchRequest)
        {
            return await AttemptData.SearchAttempts(searchRequest);
        }
    }
}