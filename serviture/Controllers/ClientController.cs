﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        public IClientData ClientData { get; }
        readonly ILogger<ClientController> _logger;

        public ClientController(IClientData clientData, ILogger<ClientController> logger)
        {
            ClientData = clientData ?? throw new ArgumentNullException(nameof(clientData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("{id}")]
        public async Task<Client> Get(int id)
        {
            return await ClientData.GetClient(id);
        }

        [HttpGet]
        public async Task<List<Client>> Get()
        {
            return await ClientData.GetClients();
        }

        [HttpPost]
        public async Task<int> Post([FromBody] Client client)
        {
            return await ClientData.CreateClient(client);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] Client client)
        {
            return await ClientData.UpdateClient(id, client);
        }

        [HttpPost("delete")]
        public async Task<int> Delete([FromBody] Client client)
        {
            return await ClientData.DeleteClient(client.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await ClientData.HardDeleteClient(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Client>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<Client>> SearchFields([FromBody]ClientSearchRequest searchRequest)
        {
            return await ClientData.SearchClients(searchRequest);
        }

        [HttpPost("random")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<Client> GetRandomClient()
        {
            return await ClientData.GetRandomRecord();
        }
    }
}