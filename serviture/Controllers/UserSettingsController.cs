﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Serviture.Data;
    using Serviture.Models;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    [Route("api/[controller]")]
    [ApiController]
    public class UserSettingsController : ControllerBase
    {
        public IUserSettingsData UserSettingsData { get; }
        readonly ILogger<UserSettingsController> _logger;

        public UserSettingsController(IUserSettingsData userSettingsData, ILogger<UserSettingsController> logger)
        {
            UserSettingsData = userSettingsData ?? throw new ArgumentNullException(nameof(userSettingsData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("{id}")]
        public async Task<UserSettings> Get(int id)
        {
            return await UserSettingsData.GetUserSettings(id);
        }

        [HttpGet("userid/{id}")]
        public async Task<UserSettings> GetByUser(int id)
        {
            var dbuSearchResponse = await SearchUserSettingss(new UserSettingsSearchRequest { UserId = id, PageIndex = 0, PageSize = 1 });
            return dbuSearchResponse.Results.ToList().FirstOrDefault();
        }

        [HttpPost("upsert")]
        public async Task<int> Upsert([FromBody] UserSettings item)
        {
            var id = 0;

            try
            {
                var searchResponse = await SearchUserSettingss(new UserSettingsSearchRequest { UserId = item.UserId, PageIndex = 0, PageSize = 1 });
                var oldSetting = searchResponse.Results.ToList().FirstOrDefault();

                if(oldSetting != null)
                {
                    await Delete(oldSetting.Id);
                }

                id = await UserSettingsData.CreateUserSettings(item);

                return id;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return id;
            }

        }

        [HttpPost]
        public async Task<int> Post([FromBody] UserSettings userSettings)
        {
            return await UserSettingsData.CreateUserSettings(userSettings);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] UserSettings userSettings)
        {
            return await UserSettingsData.UpdateUserSettings(id, userSettings);
        }

        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            return await UserSettingsData.HardDeleteUserSettings(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<UserSettings>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<UserSettings>> SearchUserSettingss([FromBody] UserSettingsSearchRequest searchRequest)
        {
            return await UserSettingsData.SearchUserSettingss(searchRequest);
        }
    }
}
