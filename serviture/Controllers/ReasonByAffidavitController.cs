﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class ReasonByAffidavitController : ControllerBase
    {
        public IReasonData ReasonData { get; }
        public IReasonByAffidavitData ReasonByAffidavitData { get; }

        readonly ILogger<ReasonByAffidavitController> _logger;

        public ReasonByAffidavitController(IReasonData reasonData, IReasonByAffidavitData reasonByAffidavitData, ILogger<ReasonByAffidavitController> logger)
        {
            ReasonData = reasonData ?? throw new ArgumentNullException(nameof(reasonData));
            ReasonByAffidavitData = reasonByAffidavitData ?? throw new ArgumentNullException(nameof(reasonByAffidavitData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("affidavitid/{id}")]
        public async Task<List<Reason>> GetByAffidavit(int id)
        {
            var rbaSearchResponse = await SearchReasonByAffidavits(new ReasonByAffidavitSearchRequest { AffidavitId = id, PageIndex = 0, PageSize = 50 });
            var ids = rbaSearchResponse.Results.Select(rba => rba.ReasonId).ToList();

            var reasonSearchResponse = await ReasonData.SearchReasons(new ReasonSearchRequest { Id = ids, PageIndex = 0, PageSize = 50 });
            return reasonSearchResponse.Results.ToList();
        }

        [HttpPost("upsert")]
        public async Task<List<int>> Post([FromBody] List<ReasonByAffidavit> items)
        {
            var ids = new List<int>();

            try
            {
                var searchResponse = await SearchReasonByAffidavits(new ReasonByAffidavitSearchRequest { AffidavitId = items.FirstOrDefault().AffidavitId, PageIndex = 0, PageSize = 50 });

                foreach (var dbu in searchResponse.Results)
                {
                    await HardDelete(dbu.Id);
                }

                foreach (var item in items)
                {
                    var id = await ReasonByAffidavitData.CreateReasonByAffidavit(item);
                    ids.Add(id);
                }

                return ids;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ids;
            }

        }

        [HttpPost]
        public async Task<int> Post([FromBody] ReasonByAffidavit item)
        {
            return await ReasonByAffidavitData.CreateReasonByAffidavit(item);
        }

        [HttpGet("{id}")]
        public async Task<ReasonByAffidavit> Get(int id)
        {
            return await ReasonByAffidavitData.GetReasonByAffidavit(id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await ReasonByAffidavitData.HardDeleteReasonByAffidavit(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Field>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<ReasonByAffidavit>> SearchReasonByAffidavits([FromBody]ReasonByAffidavitSearchRequest searchRequest)
        {
            return await ReasonByAffidavitData.SearchReasonByAffidavits(searchRequest);
        }
    }
}