﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Helpers;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class CaseController : ControllerBase
    {
        public ICaseData CaseData { get; }
        readonly ILogger<CaseController> _logger;
        public IFieldHelper FieldHelper { get; }
        public ICSAHelper CSAHelper { get;  }

        public CaseController(ICaseData caseData, ILogger<CaseController> logger, IFieldHelper fieldHelper, ICSAHelper csaHelper)
        {
            CaseData = caseData ?? throw new ArgumentNullException(nameof(caseData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            FieldHelper = fieldHelper ?? throw new ArgumentNullException(nameof(fieldHelper));
            CSAHelper = csaHelper ?? throw new ArgumentNullException(nameof(csaHelper));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Case))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var aCase = await CaseData.GetCase(id);
                return Ok(aCase);
            } 
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("mapped/{id}")]
        public async Task<CaseMapped> GetMapped(int id)
        {
            var caseMapped = await CSAHelper.GetCaseMapped(id);
            caseMapped.CaseServeMappeds = new List<CaseServeMapped>();

            var serves = await CSAHelper.GetServesByCaseId(id);
            foreach(var serve in serves)
            {
                var caseServeMapped = await CSAHelper.GetCaseServeMappedFromServe(serve);
                caseMapped.CaseServeMappeds.Add(caseServeMapped);
            }

            return caseMapped;
        }

        [HttpPost]
        public async Task<int> Post([FromBody]Case item)
        {
            return await CaseData.CreateCase(item);
        }

        [HttpPost("mapped")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PostMapped([FromBody] CaseMapped item)
        {
            try
            {
                var aCase = await CSAHelper.GetCaseFromCaseMapped(item);

                var caseId = await CaseData.CreateCase(aCase);

                foreach(var caseServe in item.CaseServeMappeds)
                {
                    var serve = await CSAHelper.GetServeFromCaseServeMapped(caseServe);
                    serve.CaseId = caseId;
                    var serveId = await CSAHelper.CreateServe(serve);

                    caseServe.Documents.ForEach(doc => doc.ServeId = serveId);

                    await CSAHelper.UpsertDocumentsByServeMapped(caseServe.Documents);
                }

                return Ok(caseId);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }            
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] Case item)
        {
            return await CaseData.UpdateCase(id, item);
        }

        [HttpPut("mapped/{id}")]
        public async Task<int> Put(int id, [FromBody] CaseMapped item)
        {
            var aCase = await CSAHelper.GetCaseFromCaseMapped(item);
            var rowsUpdated = await CaseData.UpdateCase(id, aCase);

            foreach (var caseServe in item.CaseServeMappeds)
            {
                var serve = await CSAHelper.GetServeFromCaseServeMapped(caseServe);

                if(serve.ServeStatus != (int)ServeStatus.Complete)
                {
                    serve.CaseId = id;
                    if (caseServe.Id > 0)
                    {
                        await CSAHelper.UpdateServe(caseServe.Id, serve);
                    }
                    else
                    {
                        await CSAHelper.CreateServe(serve);
                    }
                }                
            }

            return rowsUpdated;
        }

        [HttpPost("delete")]
        public async Task<int> DeleteCase([FromBody] Case item)
        {
            return await CaseData.DeleteCase(item.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            return await CSAHelper.HardDeleteCase(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Case>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<CaseMapped>> SearchCases([FromBody]CaseSearchRequest searchRequest)
        {
            var searchResponse = new SearchResponse<CaseMapped>();
            var mappedCases = new List<CaseMapped>();

            searchRequest.OrderBy = "CreatedOn desc";
            var cases = await CaseData.SearchCases(searchRequest);

            foreach (var aCase in cases.Results)
            {
                var mappedCase = await CSAHelper.GetCaseMappedFromCase(aCase);
                mappedCases.Add(mappedCase);
            }

            searchResponse.HasMoreResults = cases.HasMoreResults;
            searchResponse.TotalResults = cases.TotalResults;
            searchResponse.Results = mappedCases;

            return searchResponse;
        }        
    }
}