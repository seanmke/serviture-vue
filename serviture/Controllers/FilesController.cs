﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;
    using Serviture.Services;

    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        public IFilesData FilesData { get; }
        private readonly IBlobService BlobService;
        readonly ILogger<FilesController> _logger;
        private readonly IConfiguration Configuration;

        public FilesController(IFilesData filesData, IBlobService blobService, ILogger<FilesController> logger, IConfiguration configuration)
        {
            FilesData = filesData ?? throw new ArgumentNullException(nameof(filesData));
            BlobService = blobService ?? throw new ArgumentNullException(nameof(blobService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        [HttpPost("upload/{serveId}")]
        public async Task<IActionResult> UploadFiles([FromRoute]int serveId, [FromForm]IFormCollection files)
        {
            var fileIds = new List<int>();

            foreach (var file in files.Files)
            {
                byte[] fileBytes = new byte[] { 0x20 };

                if (file.Length > 0)
                {
                    var fileName = $"{serveId} {file.FileName}";

                    var newUpload = new FileUpload
                    {
                        ServeId = serveId,
                        Size = (int)file.Length,
                        FileData = fileBytes,
                        Filename = fileName,
                        FileType = Path.GetExtension(file.FileName).Substring(1),
                        UploadedBy = 1,
                        UploadedOn = DateTime.UtcNow
                    };

                    var fileId = await FilesData.CreateFiles(newUpload);

                    var filePrefix = Configuration["AppSettings:BlobFilePrefix"] ?? string.Empty;

                    newUpload.Id = fileId;
                    var updatedFileName = $"{filePrefix}{serveId}_{fileId}.{newUpload.FileType}";
                    updatedFileName = updatedFileName.Replace("image/", string.Empty);
                    newUpload.Filename = updatedFileName;

                    await FilesData.UpdateFile(fileId, newUpload);

                    var fileStream = file.OpenReadStream();
                    var blobInfo = await BlobService.Upload(updatedFileName, fileStream);

                    fileIds.Add(fileId);
                }
            }

            return Ok(fileIds);
        }

        [HttpPost("picture/{serveId}")]
        public async Task<IActionResult> UploadPicture([FromRoute]int serveId, [FromForm]IFormCollection formData)
        {
            var fileId = 0;

            var file = formData.Files.FirstOrDefault();

            formData.TryGetValue("coords", out var coords);
            formData.TryGetValue("time", out var timestamp);

            byte[] fileBytes = new byte[] { 0x20 };

            if (file.Length > 0)
            {
                var newUpload = new FileUpload
                {
                    ServeId = serveId,
                    Size = (int)file.Length,
                    FileData = fileBytes,
                    Filename = $"image{timestamp}",
                    FileType = "png",
                    Metadata = $"{coords}, {timestamp}",
                    UploadedBy = 1,
                    UploadedOn = DateTime.UtcNow
                };

                fileId = await FilesData.CreateFiles(newUpload);

                var filePrefix = Configuration["AppSettings:BlobFilePrefix"] ?? string.Empty;

                newUpload.Id = fileId;
                var updatedFileName = $"{filePrefix}{serveId}_{fileId}.{newUpload.FileType}";
                updatedFileName = updatedFileName.Replace("image/", string.Empty);
                newUpload.Filename = updatedFileName;

                await FilesData.UpdateFile(fileId, newUpload);

                var fileStream = file.OpenReadStream();
                var blobInfo = await BlobService.Upload(updatedFileName, fileStream);
            }

            return Ok(fileId);
        }

        [HttpGet("{id}")]
        public async Task<FileUpload> Get(int id)
        {
            var fileInfo = await FilesData.GetFile(id);
            var fileData = await BlobService.Get(fileInfo.Filename);
            fileInfo.FileData = fileData;
            return fileInfo;
        }

        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            return await FilesData.HardDeleteFiles(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<FileUpload>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<FileUpload>> SearchFiles([FromBody]FilesSearchRequest searchRequest)
        {
            var files = await FilesData.SearchFiles(searchRequest);
            return files;
        }

        [HttpPost("searchblobs")]
        [ProducesResponseType(typeof(List<FileUpload>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<FileUpload>> SearchFilesWithBlobs([FromBody] FilesSearchRequest searchRequest)
        {
            var files = await FilesData.SearchFiles(searchRequest);

            foreach (var f in files.Results)
            {
                var fileData = await BlobService.Get(f.Filename);
                f.FileData = fileData;
            }

            return files;
        }
    }
}