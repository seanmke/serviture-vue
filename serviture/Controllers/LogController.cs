﻿namespace Serviture.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Models;
    using System;

    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        readonly ILogger<AddressController> _logger;

        public LogController(ILogger<AddressController> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        public void Post([FromBody] LogMessage logMessage)
        {
            _logger.LogInformation(logMessage.Message);
        }
    }
}
