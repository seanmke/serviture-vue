﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class DocumentByUserController : ControllerBase
    {
        public IDocumentData DocumentData { get; }
        public IDocumentByUserData DocumentByUserData { get; }

        readonly ILogger<DocumentByUserController> _logger;

        public DocumentByUserController(IDocumentData documentData, IDocumentByUserData documentByUserData, ILogger<DocumentByUserController> logger)
        {
            DocumentData = documentData ?? throw new ArgumentNullException(nameof(documentData));
            DocumentByUserData = documentByUserData ?? throw new ArgumentNullException(nameof(documentByUserData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("userid/{id}")]
        public async Task<List<Document>> GetByUser(int id)
        {
            var dbuSearchResponse = await SearchDocumentByUsers(new DocumentByUserSearchRequest { UserId = id, PageIndex = 0, PageSize = 50 });
            var ids = dbuSearchResponse.Results.Select(dbu => dbu.DocumentId).ToList();

            var docSearchResponse = await DocumentData.SearchDocuments(new DocumentSearchRequest { Id = ids, PageIndex = 0, PageSize = 50 });
            return docSearchResponse.Results.ToList();
        }

        [HttpPost("upsert")]
        public async Task<List<int>> Post([FromBody] List<DocumentByUser> items)
        {
            var ids = new List<int>();

            try
            {
                var searchResponse = await SearchDocumentByUsers(new DocumentByUserSearchRequest { UserId = items.FirstOrDefault().UserId, PageIndex = 0, PageSize = 50 });

                foreach (var dbu in searchResponse.Results)
                {
                    await HardDelete(dbu.Id);
                }

                foreach (var item in items)
                {
                    var id = await DocumentByUserData.CreateDocumentByUser(item);
                    ids.Add(id);
                }

                return ids;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                return ids;
            }
            
        }

        [HttpPost]
        public async Task<int> Post([FromBody] DocumentByUser item)
        {
            return await DocumentByUserData.CreateDocumentByUser(item);
        }

        [HttpGet("{id}")]
        public async Task<DocumentByUser> Get(int id)
        {
            return await DocumentByUserData.GetDocumentByUser(id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await DocumentByUserData.HardDeleteDocumentByUser(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Field>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<DocumentByUser>> SearchDocumentByUsers([FromBody]DocumentByUserSearchRequest searchRequest)
        {
            return await DocumentByUserData.SearchDocumentByUsers(searchRequest);
        }
    }
}