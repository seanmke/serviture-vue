﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class DocumentByServeController : ControllerBase
    {
        public IDocumentData DocumentData { get; }
        public IDocumentByServeData DocumentByServeData { get; }

        readonly ILogger<DocumentByServeController> _logger;

        public DocumentByServeController(IDocumentData documentData, IDocumentByServeData documentByServeData, ILogger<DocumentByServeController> logger)
        {
            DocumentData = documentData ?? throw new ArgumentNullException(nameof(documentData));
            DocumentByServeData = documentByServeData ?? throw new ArgumentNullException(nameof(documentByServeData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("serveid/{id}")]
        public async Task<List<Document>> GetByServe(int serveId)
        {
            var dbuSearchResponse = await SearchDocumentByServes(new DocumentByServeSearchRequest { ServeId = serveId, PageIndex = 0, PageSize = 50 });
            var ids = dbuSearchResponse.Results.Select(dbu => dbu.DocumentId).ToList();

            var docSearchResponse = await DocumentData.SearchDocuments(new DocumentSearchRequest { Id = ids, PageIndex = 0, PageSize = 50 });
            return docSearchResponse.Results.ToList();
        }

        [HttpPost("upsert")]
        public async Task<List<int>> Post([FromBody] List<DocumentByServe> items)
        {
            var ids = new List<int>();

            try
            {
                var searchResponse = await SearchDocumentByServes(new DocumentByServeSearchRequest { ServeId = items.FirstOrDefault().ServeId, PageIndex = 0, PageSize = 50 });

                foreach (var dbu in searchResponse.Results)
                {
                    await HardDelete(dbu.Id);
                }

                foreach (var item in items)
                {
                    var id = await DocumentByServeData.CreateDocumentByServe(item);
                    ids.Add(id);
                }

                return ids;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ids;
            }

        }

        [HttpPost]
        public async Task<int> Post([FromBody] DocumentByServe item)
        {
            return await DocumentByServeData.CreateDocumentByServe(item);
        }

        [HttpGet("{id}")]
        public async Task<DocumentByServe> Get(int id)
        {
            return await DocumentByServeData.GetDocumentByServe(id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await DocumentByServeData.HardDeleteDocumentByServe(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Field>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<DocumentByServe>> SearchDocumentByServes([FromBody]DocumentByServeSearchRequest searchRequest)
        {
            return await DocumentByServeData.SearchDocumentByServes(searchRequest);
        }
    }
}