﻿namespace Serviture.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;

    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        public IDocumentData DocumentData { get; }
        public IDocumentByUserData DocumentByUserData { get; }
        readonly ILogger<DocumentController> _logger;

        public DocumentController(IDocumentData documentData, IDocumentByUserData documentByUserData, ILogger<DocumentController> logger)
        {
            DocumentData = documentData ?? throw new ArgumentNullException(nameof(documentData));
            DocumentByUserData = documentByUserData ?? throw new ArgumentNullException(nameof(documentByUserData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("{id}")]
        public async Task<Document> Get(int id)
        {
            return await DocumentData.GetDocument(id);
        }

        [HttpGet]
        public async Task<List<Document>> Get()
        {
            return await DocumentData.GetDocuments();
        }

        [HttpPost]
        public async Task<int> Post([FromBody] Document document)
        {
            return await DocumentData.CreateDocument(document);
        }

        [HttpPut("{id}")]
        public async Task<int> Put(int id, [FromBody] Document document)
        {
            return await DocumentData.UpdateDocument(id, document);
        }

        [HttpPost("delete")]
        public async Task<int> Delete([FromBody] Document document)
        {
            return await DocumentData.DeleteDocument(document.Id);
        }

        [HttpDelete("{id}")]
        public async Task<int> HardDelete(int id)
        {
            return await DocumentData.HardDeleteDocument(id);
        }

        [HttpPost("search")]
        [ProducesResponseType(typeof(List<Field>), StatusCodes.Status200OK)]
        public async Task<SearchResponse<Document>> SearchDocuments([FromBody]DocumentSearchRequest searchRequest)
        {
            return await DocumentData.SearchDocuments(searchRequest);
        }
    }
}