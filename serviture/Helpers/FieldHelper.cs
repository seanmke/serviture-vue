﻿namespace Serviture.Helpers
{
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class FieldHelper : IFieldHelper
    {
        public IFieldData FieldData { get; }
        public IClientData ClientData { get; }

        readonly ILogger<FieldHelper> _logger;

        public FieldHelper(IFieldData fieldData, IClientData clientData, ILogger<FieldHelper> logger)
        {
            FieldData = fieldData ?? throw new ArgumentNullException(nameof(fieldData));
            ClientData = clientData ?? throw new ArgumentNullException(nameof(clientData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<int> CreateField(Field field)
        {
            return await FieldData.CreateField(field);
        }

        public async Task<Field> GetField(int fieldId)
        {
            return await FieldData.GetField(fieldId);
        }

        public async Task<Field> SearchField(string value)
        {
            var result = await FieldData.SearchFields(new FieldSearchRequest { Value = value, PageIndex = 0, PageSize = 10 });
            return result.Results.FirstOrDefault();
        }

        public async Task<int> CreateClient(Client client)
        {
            return await ClientData.CreateClient(client);
        }

        public async Task<Client> GetClient(int clientId)
        {
            return await ClientData.GetClient(clientId);
        }

        public async Task<Client> GetRandomClient(int clientId)
        {
            return await ClientData.GetClient(clientId);
        }

        public async Task<Field> GetRandomField(FieldType fieldType)
        {
            return await FieldData.GetRandomRecord(fieldType);
        }
    }
}
