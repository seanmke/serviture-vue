﻿namespace Serviture.Helpers
{
    using System.Threading.Tasks;

    public interface IRazorToStringRenderer
    {
        Task<string> RenderPartialToStringAsync<TModel>(string partialName, TModel model);
    }
}
