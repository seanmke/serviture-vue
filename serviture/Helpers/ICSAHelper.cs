﻿namespace Serviture.Helpers
{
    using Serviture.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICSAHelper
    {
        Task<CaseMapped> GetCaseMapped(int caseId);

        Task<Case> GetCaseFromCaseMapped(CaseMapped caseMapped);

        Task<int> CreateServe(Serve serve);

        Task<Serve> GetServe(int serveId);

        Task<ServeMapped> GetServeMapped(int serveId);

        Task<ServeMapped> GetServeMappedFromServe(Serve serve);

        Task<AffidavitMapped> GetAffidavitMapped(int affidavitId);

        Task<Serve> GetServeFromServeMapped(ServeMapped serveMapped);

        Task<AffidavitMapped> GetAffidavitMappedFromAffidavit(Affidavit affidavit);

        Task<int> UpdateServe(int serveId, Serve serve);

        Task<int> UpdateServeAsComplete(int serveId);

        Task<List<Serve>> GetServesByCaseId(int caseId);

        Task UpsertByAffidavitMapped(AffidavitMapped affidavitMapped, int? affidavitId = null);

        Task<List<int>> UpsertDocumentsByServeMapped(List<DocumentByServeMapped> documentByServes, int? serveId = null);

        Task<List<int>> UpsertAddresses(List<Address> addresses, int? serveId = null);

        Task<List<int>> UpsertAttempts(List<AttemptMapped> attempts, int? serveId = null);

        Task<List<int>> UpsertReasonByAffidavit(List<ReasonByAffidavitMapped> reasonByAffidavits, int? affidavit = null);

        Task<CaseServeMapped> GetCaseServeMappedFromServe(Serve serve);

        Task<Serve> GetServeFromCaseServeMapped(CaseServeMapped caseServeMapped);

        Task<Affidavit> GetAffidavitFromAffidavitMapped(AffidavitMapped affidavitMapped);

        Task<CaseMapped> GetCaseMappedFromCase(Case aCase);

        Task<int> HardDeleteCase(int caseId);

        Task<int> HardDeleteServe(int serveId);

        Task<int> HardDeleteAffidavit(int affidavitId);

        Task<AttemptReport> GetAttemptReport(int serveId);
    }
}
