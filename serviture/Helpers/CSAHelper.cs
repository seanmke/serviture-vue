﻿namespace Serviture.Helpers
{
    using Microsoft.Extensions.Logging;
    using Serviture.Data;
    using Serviture.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CSAHelper : ICSAHelper
    {
        readonly ILogger<CSAHelper> _logger;
        public IServeData ServeData { get; }
        public ICaseData CaseData { get; }
        public IAffidavitData AffidavitData { get; }
        public IDocumentByServeData DocumentByServeData { get; }
        public IAddressData AddressData { get; }
        public IAttemptData AttemptData { get; set; }
        public IReasonByAffidavitData ReasonByAffidavitData { get; set; }
        public IFieldHelper FieldHelper { get; }
        public IUserData UserData { get; }
        public IFilesData FilesData { get; }
        public IReasonData ReasonData { get; }
        public IDocumentData DocumentData { get; }
        public IContractorUserData ContractorUserData { get; }

        public CSAHelper(IServeData serveData, ICaseData caseData, IAffidavitData affidavitData, IDocumentByServeData documentByServeData, 
            IAddressData addressByServeData, ILogger<CSAHelper> logger, IFieldHelper fieldHelper, IUserData userData, IAttemptData attemptData, 
            IReasonByAffidavitData reasonByAffidavitData, IFilesData filesData, IReasonData reasonData, IDocumentData documentData,
            IContractorUserData contractorUserData)
        {
            ServeData = serveData ?? throw new ArgumentNullException(nameof(serveData));
            CaseData = caseData ?? throw new ArgumentNullException(nameof(caseData));
            AffidavitData = affidavitData ?? throw new ArgumentNullException(nameof(affidavitData));
            DocumentByServeData = documentByServeData ?? throw new ArgumentNullException(nameof(documentByServeData));
            AddressData = addressByServeData ?? throw new ArgumentNullException(nameof(addressByServeData));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            FieldHelper = fieldHelper ?? throw new ArgumentNullException(nameof(fieldHelper));
            UserData = userData ?? throw new ArgumentNullException(nameof(userData));
            AttemptData = attemptData ?? throw new ArgumentNullException(nameof(attemptData));
            ReasonByAffidavitData = reasonByAffidavitData ?? throw new ArgumentNullException(nameof(reasonByAffidavitData));
            FilesData = filesData ?? throw new ArgumentNullException(nameof(filesData));
            ReasonData = reasonData ?? throw new ArgumentNullException(nameof(reasonData));
            DocumentData = documentData ?? throw new ArgumentNullException(nameof(documentData));
            ContractorUserData = contractorUserData ?? throw new ArgumentNullException(nameof(contractorUserData));
        }

        public async Task<CaseMapped> GetCaseMapped(int caseId)
        {
            var aCase = await CaseData.GetCase(caseId);
            var caseMapped = await GetCaseMappedFromCase(aCase);

            return caseMapped;
        }

        public async Task<Case> GetCaseFromCaseMapped(CaseMapped caseMapped)
        {
            var aCase = new Case();

            int? clientId = null;

            // Client
            if(caseMapped.Client != null)
            {
                if (caseMapped.Client.Id == 0)
                {
                    clientId = await FieldHelper.CreateClient(caseMapped.Client);
                }
                else
                {
                    clientId = caseMapped.Client.Id;
                }
            }            

            aCase.ClientId = clientId;

            // Client File Number

            int? fileNumId = null;

            if (caseMapped.ClientFileNumber != null)
            {
                if (caseMapped.ClientFileNumber.Id == 0)
                {

                    fileNumId = await FieldHelper.CreateField(caseMapped.ClientFileNumber);
                }
                else
                {
                    fileNumId = caseMapped.ClientFileNumber.Id;
                }
            }

            aCase.FileNumId = fileNumId;

            // Case Name

            int? caseNameId = null;

            if (caseMapped.CaseName != null)
            {
                if (caseMapped.CaseName.Id == 0)
                {
                    caseNameId = await FieldHelper.CreateField(caseMapped.CaseName);
                }
                else
                {
                    caseNameId = caseMapped.CaseName.Id;
                }
            }

            aCase.CaseNameId = caseNameId;

            // Case Number

            int? caseNumberId = null;

            if (caseMapped.CaseNumber != null)
            {
                if (caseMapped.CaseNumber.Id == 0)
                {
                    caseNumberId = await FieldHelper.CreateField(caseMapped.CaseNumber);
                }
                else
                {
                    caseNumberId = caseMapped.CaseNumber.Id;
                }
            }

            aCase.CaseNumId = caseNumberId;

            // Contact

            int? contactId = null;

            if (caseMapped.Contact != null)
            {
                if (caseMapped.Contact.Id == 0)
                {
                    contactId = await FieldHelper.CreateField(caseMapped.Contact);
                }
                else
                {
                    contactId = caseMapped.Contact.Id;
                }
            }

            aCase.ContactId = contactId;

            // Defendant

            int? defendantId = null;

            if (caseMapped.Defendant != null)
            {
                if (caseMapped.Defendant.Id == 0)
                {
                    defendantId = await FieldHelper.CreateField(caseMapped.Defendant);
                }
                else
                {
                    defendantId = caseMapped.Defendant.Id;
                }
            }

            aCase.DefendantId = defendantId;

            // Other Location

            int? otherLocationId = null;

            if (caseMapped.OtherLocation != null)
            {
                if (caseMapped.OtherLocation.Id == 0)
                {

                    otherLocationId = await FieldHelper.CreateField(caseMapped.OtherLocation);
                }
                else
                {
                    otherLocationId = caseMapped.OtherLocation.Id;
                }
            }

            aCase.OtherLocationId = otherLocationId;

            // Other Fields

            aCase.State = caseMapped.State;
            aCase.County = caseMapped.County;

            DateTime? courtDate = null;

            if (!string.IsNullOrWhiteSpace(caseMapped.CourtDate))
            {
                courtDate = DateTime.Parse(caseMapped.CourtDate);
            }

            aCase.CourtDate = courtDate;

            aCase.CreatedBy = caseMapped.CreatedBy;
            aCase.CreatedOn = caseMapped.CreatedOn;

            return aCase;
        }

        public async Task<int> CreateServe(Serve serve)
        {
            return await ServeData.CreateServe(serve);
        }

        public async Task<Serve> GetServe(int serveId)
        {
            return await ServeData.GetServe(serveId);
        }

        public async Task<ServeMapped> GetServeMapped(int serveId)
        {
            var serve = await ServeData.GetServe(serveId);
            var caseMapped = await GetCaseMapped(serve.CaseId);

            var serveMapped = await GetServeMappedFromCaseAndServe(caseMapped, serve);
            var addresses = await GetAddressesByServe(serveId);
            var documents = await GetDocumentByServe(serveId);
            var attempts = await GetAttemptsByServe(serveId);

            serveMapped.Addresses = addresses;
            serveMapped.Documents = documents;
            serveMapped.Attempts = attempts;
            serveMapped.Files = await GetFilesByServe(serveId);

            return serveMapped;
        }

        public async Task<AffidavitMapped> GetAffidavitMapped(int affidavitId)
        {
            var affidavit = await AffidavitData.GetAffidavit(affidavitId);

            if(affidavit == null)
            {
                return null;
            }

            var affidavitMapped = await GetAffidavitMappedFromAffidavit(affidavit);

            return affidavitMapped;
        }

        public async Task<ServeMapped> GetServeMappedFromServe(Serve serve)
        {
            var serveId = serve.Id;
            var caseMapped = await GetCaseMapped(serve.CaseId);

            var serveMapped = await GetServeMappedFromCaseAndServe(caseMapped, serve);
            serveMapped.Attempts = await GetAttemptsByServe(serveId);
            //serveMapped.Files = await GetFilesByServe(serveId); //This was slowing everything down and I think not being used at all

            //TODO Gets the test passing, but is this correct?
            serveMapped.Id = serve.Id;

            return serveMapped;
        }

        public async Task<int> UpdateServe(int serveId, Serve serve)
        {
            return await ServeData.UpdateServe(serveId, serve);
        }

        public async Task<int> UpdateServeAsComplete(int serveId)
        {
            var serve = await ServeData.GetServe(serveId);
            serve.ServeStatus = (int)ServeStatus.Complete;
            return await ServeData.UpdateServe(serveId, serve);
        }

        public async Task<List<Serve>> GetServesByCaseId(int caseId)
        {
            var searchRequest = new ServeSearchRequest
            {
                CaseId = caseId,
                PageIndex = 0,
                PageSize = 50,
                ServeStatus = (int)ServeStatus.InProcess,
                IsDeleted = false
            };

            var searchResponse = await ServeData.SearchServes(searchRequest);
            return searchResponse.Results.ToList();
        }

        public async Task UpsertByAffidavitMapped(AffidavitMapped affidavitMapped, int? affidavitId = null)
        {
            await UpsertDocumentsByServeMapped(affidavitMapped.Documents, affidavitMapped.ServeId);
            await UpsertAddresses(affidavitMapped.Addresses, affidavitMapped.ServeId);
            await UpsertAttempts(affidavitMapped.Attempts, affidavitMapped.ServeId);

            await UpsertReasonByAffidavit(affidavitMapped.Reasons, affidavitId);
        }

        public async Task<List<int>> UpsertDocumentsByServeMapped(List<DocumentByServeMapped> documentByServes, int? serveId = null)
        {
            var ids = new List<int>();

            try
            {
                var tServeId = serveId ?? documentByServes.FirstOrDefault().ServeId;
                var searchResponse = await DocumentByServeData.SearchDocumentByServes(new DocumentByServeSearchRequest { ServeId = tServeId, PageIndex = 0, PageSize = 50 });

                foreach (var dbu in searchResponse.Results)
                {
                    await DocumentByServeData.HardDeleteDocumentByServe(dbu.Id);
                }

                foreach (var item in documentByServes)
                {
                    if (item.DocumentText != null && item.DocumentText.Id == 0)
                    {
                        await FieldHelper.CreateField(item.DocumentText);
                    }

                    item.ServeId = tServeId;
                    var docByServe = new DocumentByServe(item);
                    var id = await DocumentByServeData.CreateDocumentByServe(docByServe);
                    ids.Add(id);
                }

                return ids;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ids;
            }
        }

        public async Task<List<int>> UpsertReasonByAffidavit(List<ReasonByAffidavitMapped> reasonByAffidavits, int? affidavitId = null)
        {
            var ids = new List<int>();

            try
            {
                var tAffidavitId = affidavitId ?? reasonByAffidavits.FirstOrDefault().AffidavitId;
                var searchResponse = await ReasonByAffidavitData.SearchReasonByAffidavits(new ReasonByAffidavitSearchRequest { AffidavitId = tAffidavitId, PageIndex = 0, PageSize = 50 });

                foreach (var dbu in searchResponse.Results)
                {
                    await ReasonByAffidavitData.HardDeleteReasonByAffidavit(dbu.Id);
                }

                foreach (var item in reasonByAffidavits)
                {
                    var rba = new ReasonByAffidavit(item);
                    rba.AffidavitId = tAffidavitId;
                    var id = await ReasonByAffidavitData.CreateReasonByAffidavit(rba);
                    ids.Add(id);
                }

                return ids;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ids;
            }
        }

        public async Task<List<int>> UpsertAddresses(List<Address> addresses, int? serveId = null)
        {
            var ids = new List<int>();

            try
            {
                var tServeId = serveId ?? addresses.FirstOrDefault().ServeId;
                var searchResponse = await AddressData.SearchAddresses(new AddressSearchRequest { ServeId = tServeId, PageIndex = 0, PageSize = 50 });

                foreach (var sba in searchResponse.Results)
                {
                    await AddressData.HardDeleteAddress(sba.Id);
                }

                foreach (var item in addresses)
                {
                    item.ServeId = tServeId;
                    var id = await AddressData.CreateAddress(item);
                    ids.Add(id);
                }

                return ids;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ids;
            }
        }

        public async Task<List<int>> UpsertAttempts(List<AttemptMapped> attempts, int? serveId = null)
        {
            var ids = new List<int>();

            try
            {
                var tServeId = serveId ?? attempts.FirstOrDefault().ServeId;
                var searchResponse = await AttemptData.SearchAttempts(new AttemptSearchRequest { ServeId = tServeId, PageIndex = 0, PageSize = 50 });

                foreach (var attempt in searchResponse.Results)
                {
                    await AttemptData.HardDeleteAttempt(attempt.Id);
                }

                foreach (var item in attempts)
                {
                    if (item.OtherInfo != null && item.OtherInfo.Id == 0)
                    {
                        await FieldHelper.CreateField(item.OtherInfo);
                    }

                    item.ServeId = tServeId;
                    var attempt = new Attempt(item);
                    var id = await AttemptData.CreateAttempt(attempt);
                    ids.Add(id);
                }

                return ids;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return ids;
            }
        }

        public async Task<Serve> GetServeFromCaseServeMapped(CaseServeMapped caseServeMapped)
        {
            var serve = new Serve(caseServeMapped);

            int? entityId = null;

            if (caseServeMapped.Entity != null)
            {
                if (caseServeMapped.Entity.Id == 0)
                {
                    entityId = await FieldHelper.CreateField(caseServeMapped.Entity);
                }
                else
                {
                    entityId = caseServeMapped.Entity.Id;
                }
            }

            serve.EntityId = entityId;

            return serve;
        }

        public async Task<CaseServeMapped> GetCaseServeMappedFromServe(Serve serve)
        {
            var caseServeMapped = new CaseServeMapped();

            caseServeMapped.Id = serve.Id;
            caseServeMapped.Notes = serve.Notes;
            caseServeMapped.DueDate = serve.DueDate?.Date.ToShortDateString();
            caseServeMapped.ServeStatus = serve.ServeStatus;

            if (serve.EntityId != null)
            {
                var entity = await FieldHelper.GetField(serve.EntityId.Value);
                caseServeMapped.Entity = entity;
            }

            if (serve.ServerId != null)
            {
                var user = await UserData.GetUser(serve.ServerId.Value);
                caseServeMapped.Server = user;
            }

            return caseServeMapped;
        }

        public async Task<Serve> GetServeFromServeMapped(ServeMapped serveMapped)
        {
            var serve = new Serve(serveMapped);

            //EntityId

            int? entityId = null;

            if (serveMapped.Entity != null)
            {
                if (serveMapped.Entity.Id == 0)
                {

                    entityId = await FieldHelper.CreateField(serveMapped.Entity);
                }
                else
                {
                    entityId = serveMapped.Entity.Id;
                }
            }

            serve.EntityId = entityId;

            int? contractorUserId = null;

            if(serveMapped.ContractorUser != null)
            {
                if (serveMapped.ContractorUser.Id == 0)
                {
                    contractorUserId = await ContractorUserData.CreateContractorUser(serveMapped.ContractorUser);
                }
                else
                {
                    contractorUserId = serveMapped.ContractorUser.Id;
                }
            }

            serve.ContractorUserId = contractorUserId;

            return serve;
        }

        public async Task<Affidavit> GetAffidavitFromAffidavitMapped(AffidavitMapped affidavitMapped)
        {
            var affidavit = new Affidavit(affidavitMapped);

            int? entityId = null;

            if (affidavitMapped.Entity != null)
            {
                if (affidavitMapped.Entity.Id == 0)
                {

                    entityId = await FieldHelper.CreateField(affidavitMapped.Entity);
                }
                else
                {
                    entityId = affidavitMapped.Entity.Id;
                }
            }

            affidavit.EntityId = entityId;

            int? contractorUserId = null;

            if (affidavitMapped.ContractorUser != null)
            {
                if (affidavitMapped.ContractorUser.Id == 0)
                {
                    contractorUserId = await ContractorUserData.CreateContractorUser(affidavitMapped.ContractorUser);
                }
                else
                {
                    contractorUserId = affidavitMapped.ContractorUser.Id;
                }
            }

            affidavit.ContractorUserId = contractorUserId;

            int? careOfId = null;

            if (affidavitMapped.CareOf != null)
            {
                if (affidavitMapped.CareOf.Id == 0)
                {

                    careOfId = await FieldHelper.CreateField(affidavitMapped.CareOf);
                }
                else
                {
                    careOfId = affidavitMapped.CareOf.Id;
                }
            }

            affidavit.CareOfId = careOfId;

            int? relationshipId = null;

            if (affidavitMapped.Relationship != null)
            {
                if (affidavitMapped.Relationship.Id == 0)
                {

                    relationshipId = await FieldHelper.CreateField(affidavitMapped.Relationship);
                }
                else
                {
                    relationshipId = affidavitMapped.Relationship.Id;
                }
            }

            affidavit.RelationshipId = relationshipId;

            int? clientId = null;

            // Client
            if (affidavitMapped.Client != null)
            {
                if (affidavitMapped.Client.Id == 0)
                {
                    clientId = await FieldHelper.CreateClient(affidavitMapped.Client);
                }
                else
                {
                    clientId = affidavitMapped.Client.Id;
                }
            }

            affidavit.ClientId = clientId;

            // Client File Number

            int? fileNumId = null;

            if (affidavitMapped.FileNum != null)
            {
                if (affidavitMapped.FileNum.Id == 0)
                {

                    fileNumId = await FieldHelper.CreateField(affidavitMapped.FileNum);
                }
                else
                {
                    fileNumId = affidavitMapped.FileNum.Id;
                }
            }

            affidavit.FileNumId = fileNumId;

            // Case Name

            int? caseNameId = null;

            if (affidavitMapped.CaseName != null)
            {
                if (affidavitMapped.CaseName.Id == 0)
                {
                    caseNameId = await FieldHelper.CreateField(affidavitMapped.CaseName);
                }
                else
                {
                    caseNameId = affidavitMapped.CaseName.Id;
                }
            }

            affidavit.CaseNameId = caseNameId;

            // Case Number

            int? caseNumberId = null;

            if (affidavitMapped.CaseNum != null)
            {
                if (affidavitMapped.CaseNum.Id == 0)
                {
                    caseNumberId = await FieldHelper.CreateField(affidavitMapped.CaseNum);
                }
                else
                {
                    caseNumberId = affidavitMapped.CaseNum.Id;
                }
            }

            affidavit.CaseNumId = caseNumberId;

            // Contact

            int? contactId = null;

            if (affidavitMapped.Contact != null)
            {
                if (affidavitMapped.Contact.Id == 0)
                {
                    contactId = await FieldHelper.CreateField(affidavitMapped.Contact);
                }
                else
                {
                    contactId = affidavitMapped.Contact.Id;
                }
            }

            affidavit.ContactId = contactId;

            // Defendant

            int? defendantId = null;

            if (affidavitMapped.Defendant != null)
            {
                if (affidavitMapped.Defendant.Id == 0)
                {
                    defendantId = await FieldHelper.CreateField(affidavitMapped.Defendant);
                }
                else
                {
                    defendantId = affidavitMapped.Defendant.Id;
                }
            }

            affidavit.DefendantId = defendantId;

            // Other Location

            int? otherLocationId = null;

            if (affidavitMapped.OtherLocation != null)
            {
                if (affidavitMapped.OtherLocation.Id == 0)
                {

                    otherLocationId = await FieldHelper.CreateField(affidavitMapped.OtherLocation);
                }
                else
                {
                    otherLocationId = affidavitMapped.OtherLocation.Id;
                }
            }

            affidavit.OtherLocationId = otherLocationId;

            return affidavit;
        }

        public async Task<CaseMapped> GetCaseMappedFromCase(Case aCase)
        {
            var caseMapped = new CaseMapped();

            if (aCase != null)
            {
                if (aCase.ClientId != null)
                {
                    var client = await FieldHelper.GetClient(aCase.ClientId.Value);
                    caseMapped.Client = client;
                }

                if (aCase.CaseNameId != null)
                {
                    var field = await FieldHelper.GetField(aCase.CaseNameId.Value);
                    caseMapped.CaseName = field;
                }

                if (aCase.CaseNumId != null)
                {
                    var field = await FieldHelper.GetField(aCase.CaseNumId.Value);
                    caseMapped.CaseNumber = field;
                }

                if (aCase.ContactId != null)
                {
                    var field = await FieldHelper.GetField(aCase.ContactId.Value);
                    caseMapped.Contact = field;
                }

                if (aCase.DefendantId != null)
                {
                    var field = await FieldHelper.GetField(aCase.DefendantId.Value);
                    caseMapped.Defendant = field;
                }

                if (aCase.FileNumId != null)
                {
                    var field = await FieldHelper.GetField(aCase.FileNumId.Value);
                    caseMapped.ClientFileNumber = field;
                }

                if (aCase.OtherLocationId != null)
                {
                    var field = await FieldHelper.GetField(aCase.OtherLocationId.Value);
                    caseMapped.OtherLocation = field;
                }

                caseMapped.Id = aCase.Id;
                caseMapped.County = aCase.County;
                caseMapped.State = aCase.State;
                caseMapped.CourtDate = aCase.CourtDate?.ToShortDateString();
                caseMapped.CreatedOn = aCase.CreatedOn;
            }

            return caseMapped;
        }

        public async Task<int> HardDeleteCase(int caseId)
        {
            var serves = await GetServesByCaseId(caseId);
            foreach(var serve in serves)
            {
                await HardDeleteServe(serve.Id);
            }

            return await CaseData.HardDeleteCase(caseId);
        }

        public async Task<int> HardDeleteServe(int serveId)
        {
            var addresses = await GetAddressesByServe(serveId);
            foreach(var address in addresses)
            {
                await AddressData.HardDeleteAddress(address.Id);
            }
            
            var attempts = await GetAttemptsByServe(serveId);
            foreach(var attempt in attempts)
            {
                await AttemptData.HardDeleteAttempt(attempt.Id);
            }

            var documents = await GetDocumentByServe(serveId);
            foreach(var doc in documents)
            {
                await DocumentByServeData.HardDeleteDocumentByServe(doc.Id);
            }

            var files = await GetFilesByServe(serveId);
            foreach(var file in files)
            {
                await FilesData.HardDeleteFiles(file.Id);
            }

            return await ServeData.HardDeleteServe(serveId);
        }

        public async Task<int> HardDeleteAffidavit(int affidavitId)
        {
            var rbas = await GetReasonsByAffidavit(affidavitId);
            foreach(var rba in rbas)
            {
                await ReasonByAffidavitData.HardDeleteReasonByAffidavit(rba.Id);
            }

            return await AffidavitData.HardDeleteAffidavit(affidavitId);
        }

        public async Task<AffidavitMapped> GetAffidavitMappedFromAffidavit(Affidavit affidavit)
        {
            var affidavitMapped = new AffidavitMapped(affidavit);

            if (affidavit.EntityId != null)
            {
                var entity = await FieldHelper.GetField(affidavit.EntityId.Value);
                affidavitMapped.Entity = entity;
            }

            if (affidavit.ContractorUserId != null)
            {
                var contractorUser = await ContractorUserData.GetContractorUser(affidavit.ContractorUserId.Value);
                affidavitMapped.ContractorUser = contractorUser;
            }

            if (affidavit.CareOfId != null)
            {
                var careOf = await FieldHelper.GetField(affidavit.CareOfId.Value);
                affidavitMapped.CareOf = careOf;
            }

            if (affidavit.RelationshipId != null)
            {
                var relationship = await FieldHelper.GetField(affidavit.RelationshipId.Value);
                affidavitMapped.Relationship = relationship;
            }

            if (affidavit.ClientId != null)
            {
                var client = await FieldHelper.GetClient(affidavit.ClientId.Value);
                affidavitMapped.Client = client;
            }

            if (affidavit.CaseNameId != null)
            {
                var field = await FieldHelper.GetField(affidavit.CaseNameId.Value);
                affidavitMapped.CaseName = field;
            }

            if (affidavit.CaseNumId != null)
            {
                var field = await FieldHelper.GetField(affidavit.CaseNumId.Value);
                affidavitMapped.CaseNum = field;
            }

            if (affidavit.ContactId != null)
            {
                var field = await FieldHelper.GetField(affidavit.ContactId.Value);
                affidavitMapped.Contact = field;
            }

            if (affidavit.DefendantId != null)
            {
                var field = await FieldHelper.GetField(affidavit.DefendantId.Value);
                affidavitMapped.Defendant = field;
            }

            if (affidavit.FileNumId != null)
            {
                var field = await FieldHelper.GetField(affidavit.FileNumId.Value);
                affidavitMapped.FileNum = field;
            }

            if (affidavit.OtherLocationId != null)
            {
                var field = await FieldHelper.GetField(affidavit.OtherLocationId.Value);
                affidavitMapped.OtherLocation = field;
            }

            var user = await UserData.GetUser(affidavit.ServerId);
            affidavitMapped.Server = user;

            if (affidavit.ServeId != null)
            {
                affidavitMapped.Addresses = await GetAddressesByServe(affidavit.ServeId.Value);
                affidavitMapped.Documents = await GetDocumentByServe(affidavit.ServeId.Value);
                affidavitMapped.Attempts = await GetAttemptsByServe(affidavit.ServeId.Value);
                affidavitMapped.Files = await GetFilesByServe(affidavit.ServeId.Value);
            }

            affidavitMapped.Reasons = await GetReasonsByAffidavit(affidavit.Id);

            return affidavitMapped;
        }

        public async Task<AttemptReport> GetAttemptReport(int serveId)
        {
            var attemptReport = new AttemptReport();

            attemptReport.Attempts = await GetAttemptsByServe(serveId);
            attemptReport.Files = await GetFilesByServe(serveId);

            return attemptReport;
        }

        private async Task<ServeMapped> GetServeMappedFromCaseAndServe(CaseMapped caseMapped, Serve serve)
        {
            var serveMapped = new ServeMapped();
            serveMapped.SetCaseMapped(caseMapped);
            serveMapped.SetVariables(serve);

            serveMapped.CaseId = serve.CaseId;

            if (serve.EntityId != null)
            {
                var entity = await FieldHelper.GetField(serve.EntityId.Value);
                serveMapped.Entity = entity;
            }

            if (serve.ServerId != null)
            {
                var user = await UserData.GetUser(serve.ServerId.Value);
                serveMapped.ProcessServer = user;
            }

            if (serve.ContractorUserId != null)
            {
                var contractorUser = await ContractorUserData.GetContractorUser(serve.ContractorUserId.Value);
                serveMapped.ContractorUser = contractorUser;
            }

            return serveMapped;
        }

        private async Task<List<Address>> GetAddressesByServe(int serveId)
        {
            var addresses = await AddressData.SearchAddresses(new AddressSearchRequest { ServeId = serveId, PageIndex = 0, PageSize = 50 });
            return addresses.Results.ToList();
        }

        private async Task<List<DocumentByServeMapped>> GetDocumentByServe(int serveId)
        {
            var mappedDocs = new List<DocumentByServeMapped>();
            var documentByServes = await DocumentByServeData.SearchDocumentByServes(new DocumentByServeSearchRequest { ServeId = serveId, PageIndex = 0, PageSize = 50 });
            var documents = await DocumentData.GetAllDocuments();

            foreach(var dbs in documentByServes.Results)
            {
                var doc = documents.Where(d => d.Id == dbs.DocumentId).FirstOrDefault();
                var docString = doc.Name;

                var mapped = new DocumentByServeMapped(dbs);
                if (!string.IsNullOrWhiteSpace(dbs.DocumentText))
                {
                    mapped.DocumentText = new Field { Id = 1, Value = dbs.DocumentText, FieldType = (int)FieldType.DocOther };
                    if (doc.TextBefore)
                    {
                        docString = $"{dbs.DocumentText} {docString}";
                    } else
                    {
                        docString = $"{docString} {dbs.DocumentText}";
                    }
                }

                mapped.DocumentString = docString;
                mappedDocs.Add(mapped);
            }
            return mappedDocs;
        }

        private async Task<List<AttemptMapped>> GetAttemptsByServe(int serveId)
        {
            var mappedAttempts = new List<AttemptMapped>();
            var attempts = await AttemptData.SearchAttempts(new AttemptSearchRequest { ServeId = serveId, PageIndex = 0, PageSize = 50 });

            foreach(var attempt in attempts.Results)
            {
                var mapped = new AttemptMapped(attempt);
                if (!string.IsNullOrWhiteSpace(attempt.OtherInfo))
                {
                    mapped.OtherInfo = new Field { Id = 1, Value = attempt.OtherInfo, FieldType = (int)FieldType.AttemptOther };
                }
                mappedAttempts.Add(mapped);
            }

            return mappedAttempts;
        }

        private async Task<List<FileUpload>> GetFilesByServe(int serveId)
        {
            var files = await FilesData.SearchFiles(new FilesSearchRequest { ServeId = serveId, PageIndex = 0, PageSize = 50 });
            return files.Results.ToList();
        }

        private async Task<List<ReasonByAffidavitMapped>> GetReasonsByAffidavit(int affidavitId)
        {
            var mappedReasons = new List<ReasonByAffidavitMapped>();
            var reasonsByAffidavit = await ReasonByAffidavitData.SearchReasonByAffidavits(new ReasonByAffidavitSearchRequest { AffidavitId = affidavitId, PageIndex = 0, PageSize = 50 });
            var reasons = await ReasonData.GetReasons();

            foreach (var reason in reasonsByAffidavit.Results)
            {
                var mapped = new ReasonByAffidavitMapped(reason);
                mapped.ReasonText = reasons.Where(r => r.Id == reason.ReasonId).FirstOrDefault().Value;
                mappedReasons.Add(mapped);
            }

            return mappedReasons;
        }
    }
}
