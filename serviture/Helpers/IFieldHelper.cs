﻿namespace Serviture.Helpers
{
    using Serviture.Models;
    using System.Threading.Tasks;

    public interface IFieldHelper
    {
        Task<int> CreateField(Field field);

        Task<Field> GetField(int fieldId);

        Task<Field> SearchField(string value);

        Task<int> CreateClient(Client client);

        Task<Client> GetClient(int clientId);

        Task<Field> GetRandomField(FieldType fieldType);
    }
}
