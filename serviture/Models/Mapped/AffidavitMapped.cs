﻿namespace Serviture.Models
{
    using System;
    using System.Collections.Generic;

    public class AffidavitMapped : AuditInfo
    {
        public int Id { get; set; }

        public string InvoiceNumber { get; set; }

        public int CompanyId { get; set; }

        public AffidavitType AffidavitType { get; set; }

        public User Server { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsArchived { get; set; }

        /// <summary>
        /// For Billing
        /// </summary>
        public bool IsPaid { get; set; }

        public bool IsNotMilitary { get; set; }

        public bool IsNotMarried { get; set; }

        public int? ServeId { get; set; }

        public Field Entity { get; set; }

        /// <summary>
        /// Can map differently depending on the Affidavit Type.
        /// Substitute Service -> Substitute Service Upon Name
        /// Company/Corporation -> Served Person
        /// </summary>
        public Field CareOf { get; set; }

        public Field Relationship { get; set; }

        public ContractorUser ContractorUser { get; set; }

        public Client Client { get; set; }

        public Field Contact { get; set; }

        public Field CaseName { get; set; }

        public Field Defendant { get; set; }

        public Field OtherLocation { get; set; }

        public Field CaseNum { get; set; }

        public Field FileNum { get; set; }

        public decimal? Charge { get; set; }

        public string CourtDate { get; set; }

        public string State { get; set; }

        public string County { get; set; }

        public string NotaryState { get; set; }

        public string NotaryCounty { get; set; }

        public string SigState { get; set; }

        public List<ReasonByAffidavitMapped> Reasons { get; set; }

        public List<Address> Addresses { get; set; }

        public List<DocumentByServeMapped> Documents { get; set; }

        public List<AttemptMapped> Attempts { get; set; }

        public List<FileUpload> Files { get; set; }

        public AffidavitMapped() { }

        public AffidavitMapped(Affidavit affidavit)
        {
            Id = affidavit.Id;
            InvoiceNumber = affidavit.InvoiceNumber;
            CompanyId = affidavit.CompanyId;
            AffidavitType = (AffidavitType)affidavit.AffidavitType;
            IsDeleted = affidavit.IsDeleted;
            IsArchived = affidavit.IsArchived;
            IsPaid = affidavit.IsPaid;
            IsNotMarried = affidavit.IsNotMarried;
            IsNotMilitary = affidavit.IsNotMilitary;
            CreatedOn = affidavit.CreatedOn;
            CreatedBy = affidavit.CreatedBy;

            ServeId = affidavit.ServeId;

            Charge = affidavit.Charge;
            CourtDate = affidavit.CourtDate?.ToShortDateString();
            State = affidavit.State;
            County = affidavit.County;
            NotaryState = affidavit.NotaryState;
            NotaryCounty = affidavit.NotaryCounty;
            SigState = affidavit.SigState;
        }

        public void SetServeMapped(ServeMapped serveMapped)
        {
            Entity = serveMapped.Entity;
            Client = serveMapped.Client;
            Contact = serveMapped.Contact;
            CaseName = serveMapped.CaseName;
            Defendant = serveMapped.Defendant;
            OtherLocation = serveMapped.OtherLocation;
            CaseNum = serveMapped.CaseNumber;
            FileNum = serveMapped.ClientFileNumber;

            ServeId = serveMapped.ServeId;
            Server = serveMapped.ProcessServer;
            ContractorUser = serveMapped.ContractorUser;

            Charge = serveMapped.Charge;
            CourtDate = serveMapped.CourtDate;
            State = serveMapped.State;
            County = serveMapped.County;
            NotaryState = serveMapped.NotaryState;
            NotaryCounty = serveMapped.NotaryCounty;
            SigState = serveMapped.SigState;

            Documents = serveMapped.Documents;
            Attempts = serveMapped.Attempts;
            Addresses = serveMapped.Addresses;
        }

        public bool CompareAffidavit(AffidavitMapped newAffidavitMapped)
        {
            try
            {
                var check = (string.IsNullOrWhiteSpace(InvoiceNumber) && string.IsNullOrWhiteSpace(newAffidavitMapped.InvoiceNumber) || InvoiceNumber.Equals(newAffidavitMapped.InvoiceNumber)) &&
                (AffidavitType == newAffidavitMapped.AffidavitType) &&
                (IsPaid == newAffidavitMapped.IsPaid) &&
                (IsNotMarried == newAffidavitMapped.IsNotMarried) &&
                (IsNotMilitary == newAffidavitMapped.IsNotMilitary) &&

                (Entity == null && newAffidavitMapped.Entity == null || Entity.Equals(newAffidavitMapped.Entity)) &&
                (CareOf == null && newAffidavitMapped.CareOf == null || CareOf.Equals(newAffidavitMapped.CareOf)) &&
                (Relationship == null && newAffidavitMapped.Relationship == null || Relationship.Equals(newAffidavitMapped.Relationship)) &&
                (Client == null && newAffidavitMapped.Client == null || Client.Equals(newAffidavitMapped.Client)) &&
                (Contact == null && newAffidavitMapped.Contact == null || Contact.Equals(newAffidavitMapped.Contact)) &&
                (CaseName == null && newAffidavitMapped.CaseName == null || CaseName.Equals(newAffidavitMapped.CaseName)) &&
                (Defendant == null && newAffidavitMapped.Defendant == null || Defendant.Equals(newAffidavitMapped.Defendant)) &&
                (OtherLocation == null && newAffidavitMapped.OtherLocation == null || OtherLocation.Equals(newAffidavitMapped.OtherLocation)) &&
                (CaseNum == null && newAffidavitMapped.CaseNum == null || CaseNum.Equals(newAffidavitMapped.CaseNum)) &&
                (FileNum == null && newAffidavitMapped.FileNum == null || FileNum.Equals(newAffidavitMapped.FileNum)) &&

                (Charge == null && newAffidavitMapped.Charge == null || Charge == newAffidavitMapped.Charge) &&
                (string.IsNullOrWhiteSpace(CourtDate) && string.IsNullOrWhiteSpace(newAffidavitMapped.CourtDate) || CourtDate.Equals(newAffidavitMapped.CourtDate)) &&
                (string.IsNullOrWhiteSpace(County) && string.IsNullOrWhiteSpace(newAffidavitMapped.County) || County.Equals(newAffidavitMapped.County)) &&
                (string.IsNullOrWhiteSpace(State) && string.IsNullOrWhiteSpace(newAffidavitMapped.State) || State.Equals(newAffidavitMapped.State)) &&
                (string.IsNullOrWhiteSpace(NotaryCounty) && string.IsNullOrWhiteSpace(newAffidavitMapped.NotaryCounty) || NotaryCounty.Equals(newAffidavitMapped.NotaryCounty)) &&
                (string.IsNullOrWhiteSpace(NotaryState) && string.IsNullOrWhiteSpace(newAffidavitMapped.NotaryState) || NotaryState.Equals(newAffidavitMapped.NotaryState)) &&
                (string.IsNullOrWhiteSpace(SigState) && string.IsNullOrWhiteSpace(newAffidavitMapped.SigState) || SigState.Equals(newAffidavitMapped.SigState));

                return check;
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }           
        }

        public bool CompareServe(ServeMapped serveMapped)
        {
            try
            {
                var check = (Entity == null && serveMapped.Entity == null || Entity.Equals(serveMapped.Entity)) &&
                (Client == null && serveMapped.Client == null || Client.Equals(serveMapped.Client)) &&
                (Contact == null && serveMapped.Contact == null || Contact.Equals(serveMapped.Contact)) &&
                (CaseName == null && serveMapped.CaseName == null || CaseName.Equals(serveMapped.CaseName)) &&
                (Defendant == null && serveMapped.Defendant == null || Defendant.Equals(serveMapped.Defendant)) &&
                (OtherLocation == null && serveMapped.OtherLocation == null || OtherLocation.Equals(serveMapped.OtherLocation)) &&
                (CaseNum == null && serveMapped.CaseNumber == null || CaseNum.Equals(serveMapped.CaseNumber)) &&
                (FileNum == null && serveMapped.ClientFileNumber == null || FileNum.Equals(serveMapped.ClientFileNumber)) &&

                (Charge == null && serveMapped.Charge == null || Charge == serveMapped.Charge) &&
                (string.IsNullOrWhiteSpace(CourtDate) && string.IsNullOrWhiteSpace(serveMapped.CourtDate) || CourtDate.Equals(serveMapped.CourtDate)) &&
                (string.IsNullOrWhiteSpace(County) && string.IsNullOrWhiteSpace(serveMapped.County) || County.Equals(serveMapped.County)) &&
                (string.IsNullOrWhiteSpace(State) && string.IsNullOrWhiteSpace(serveMapped.State) || State.Equals(serveMapped.State)) &&
                (string.IsNullOrWhiteSpace(NotaryCounty) && string.IsNullOrWhiteSpace(serveMapped.NotaryCounty) || NotaryCounty.Equals(serveMapped.NotaryCounty)) &&
                (string.IsNullOrWhiteSpace(NotaryState) && string.IsNullOrWhiteSpace(serveMapped.NotaryState) || NotaryState.Equals(serveMapped.NotaryState)) &&
                (string.IsNullOrWhiteSpace(SigState) && string.IsNullOrWhiteSpace(serveMapped.SigState) || SigState.Equals(serveMapped.SigState));

                return check;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
    }
}
