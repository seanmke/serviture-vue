﻿namespace Serviture.Models
{
    public class DocumentByServeMapped
    {
        public int Id { get; set; }

        public int ServeId { get; set; }

        public int DocumentId { get; set; }

        public Field DocumentText { get; set; }

        public string DocumentString { get; set; }

        public DocumentByServeMapped() { }

        public DocumentByServeMapped(DocumentByServe documentByServe)
        {
            Id = documentByServe.Id;
            ServeId = documentByServe.ServeId;
            DocumentId = documentByServe.DocumentId;
        }
    }
}
