﻿namespace Serviture.Models
{
    public class ReasonByAffidavitMapped
    {
        public int Id { get; set; }

        public int AffidavitId { get; set; }

        public int ReasonId { get; set; }

        public string ReasonText { get; set; }

        public ReasonByAffidavitMapped() { }

        public ReasonByAffidavitMapped(ReasonByAffidavit reasonByAffidavit)
        {
            Id = reasonByAffidavit.Id;
            AffidavitId = reasonByAffidavit.AffidavitId;
            ReasonId = reasonByAffidavit.ReasonId;
        }
    }
}
