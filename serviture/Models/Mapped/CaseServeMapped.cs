﻿namespace Serviture.Models
{
    using System.Collections.Generic;

    public class CaseServeMapped : AuditInfo
    {
        public int Id { get; set; }

        public Field Entity { get; set; }

        public User Server { get; set; }

        public string Notes { get; set; }

        public string DueDate { get; set; }

        public decimal? Charge { get; set; }

        public int ServeStatus { get; set; }

        public List<DocumentByServeMapped> Documents { get; set; }
    }
}
