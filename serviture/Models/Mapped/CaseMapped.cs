﻿namespace Serviture.Models
{
    using System.Collections.Generic;

    public class CaseMapped : AuditInfo
    {
        public int Id { get; set; }

        public Client Client { get; set; }

        public Field Contact { get; set; }

        public Field CaseName { get; set; }

        public Field Defendant { get; set; }

        public string County { get; set; }

        public string State { get; set; }

        public Field OtherLocation { get; set; }

        public Field CaseNumber { get; set; }

        public Field ClientFileNumber { get; set; }

        public string CourtDate { get; set; }

        public List<CaseServeMapped> CaseServeMappeds { get; set; }

        public bool CompareCase(CaseMapped newCaseMapped)
        {
            return (Client == null && newCaseMapped.Client == null || Client.Equals(newCaseMapped.Client)) &&
                (Contact == null && newCaseMapped.Contact == null || Contact.Equals(newCaseMapped.Contact)) &&
                (CaseName == null && newCaseMapped.CaseName == null || CaseName.Equals(newCaseMapped.CaseName)) &&
                (Defendant == null && newCaseMapped.Defendant == null || Defendant.Equals(newCaseMapped.Defendant)) &&
                (OtherLocation == null && newCaseMapped.OtherLocation == null || OtherLocation.Equals(newCaseMapped.OtherLocation)) &&
                (CaseNumber == null && newCaseMapped.CaseNumber == null || CaseNumber.Equals(newCaseMapped.CaseNumber)) &&
                (ClientFileNumber == null && newCaseMapped.ClientFileNumber == null || ClientFileNumber.Equals(newCaseMapped.ClientFileNumber)) &&
                (string.IsNullOrWhiteSpace(County) && string.IsNullOrWhiteSpace(newCaseMapped.County) || County.Equals(newCaseMapped.County)) &&
                (string.IsNullOrWhiteSpace(State) && string.IsNullOrWhiteSpace(newCaseMapped.State) || State.Equals(newCaseMapped.State)) &&
                (string.IsNullOrWhiteSpace(CourtDate) && string.IsNullOrWhiteSpace(newCaseMapped.CourtDate) || CourtDate.Equals(newCaseMapped.CourtDate));
        }
    }
}
