﻿using System;

namespace Serviture.Models
{
    public class FileInfo
    {
        public int Id { get; set; }

        public int ServeId { get; set; }

        public string Filename { get; set; }

        public int Size { get; set; }

        public int UploadedBy { get; set; }

        public DateTime UploadedOn { get; set; }

        public byte[] FileData { get; set; }

        public string FileType { get; set; }

        public string Metadata { get; set; }
    }
}
