﻿namespace Serviture.Models
{
    using System.Collections.Generic;

    public class ServeMapped : CaseMapped
    {

        public int ServeId { get; set; }

        public int CaseId { get; set; }

        public int ServeStatus { get; set; }

        public Field Entity { get; set; }

        public User ProcessServer { get; set; }

        public ContractorUser ContractorUser { get; set; }

        public decimal? Charge { get; set; }

        public string SigState { get; set; }

        public string NotaryState { get; set; }

        public string NotaryCounty { get; set; }

        public string Notes { get; set; }

        public string DueDate { get; set; }

        public List<Address> Addresses { get; set; }

        public List<DocumentByServeMapped> Documents { get; set; }

        public List<AttemptMapped> Attempts { get; set; }

        public List<FileUpload> Files { get; set; }

        public void SetCaseMapped(CaseMapped caseMapped)
        {
            Client = caseMapped.Client;
            Contact = caseMapped.Contact;
            CaseName = caseMapped.CaseName;
            Defendant = caseMapped.Defendant;
            County = caseMapped.County;
            State = caseMapped.State;
            OtherLocation = caseMapped.OtherLocation;
            CaseNumber = caseMapped.CaseNumber;
            ClientFileNumber = caseMapped.ClientFileNumber;
            CourtDate = caseMapped.CourtDate;
        }

        public void SetVariables(Serve serve)
        {
            ServeId = serve.Id;
            Charge = serve.Charge;
            ServeStatus = serve.ServeStatus;
            SigState = serve.SigState;
            NotaryState = serve.NotaryState;
            NotaryCounty = serve.NotaryCounty;
            Notes = serve.Notes;
            DueDate = serve.DueDate?.Date.ToShortDateString();

            CreatedOn = serve.CreatedOn;
            CreatedBy = serve.CreatedBy;
        }

        public bool CompareServe(ServeMapped newServeMapped)
        {
            return (Entity == null && newServeMapped.Entity == null || Entity.Equals(newServeMapped.Entity)) &&
                (Charge == null && newServeMapped.Charge == null || Charge == newServeMapped.Charge) &&
                (string.IsNullOrWhiteSpace(DueDate) && string.IsNullOrWhiteSpace(newServeMapped.DueDate) || DueDate.Equals(newServeMapped.DueDate)) &&
                (string.IsNullOrWhiteSpace(NotaryCounty) && string.IsNullOrWhiteSpace(newServeMapped.NotaryCounty) || NotaryCounty.Equals(newServeMapped.NotaryCounty)) &&
                (string.IsNullOrWhiteSpace(NotaryState) && string.IsNullOrWhiteSpace(newServeMapped.NotaryState) || NotaryState.Equals(newServeMapped.NotaryState)) &&
                (string.IsNullOrWhiteSpace(Notes) && string.IsNullOrWhiteSpace(newServeMapped.Notes) || Notes.Equals(newServeMapped.Notes)) &&
                (string.IsNullOrWhiteSpace(SigState) && string.IsNullOrWhiteSpace(newServeMapped.SigState) || SigState.Equals(newServeMapped.SigState));
        }
    }
}
