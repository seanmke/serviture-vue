﻿namespace Serviture.Models
{
    public class AttemptMapped
    {
        public int Id { get; set; }

        public string DateServed { get; set; }

        public Field OtherInfo { get; set; }

        public bool IsSuccess { get; set; }

        public int ServeId { get; set; }

        public AttemptMapped() { }

        public AttemptMapped(Attempt attempt)
        {
            if (attempt.DateServed != null)
            {
                DateServed = attempt.DateServed.Value.ToString("g");
            }

            Id = attempt.Id;
            IsSuccess = attempt.IsSuccess;
            ServeId = attempt.ServeId;
        }
    }
}
