﻿namespace Serviture.Models
{
    public class UserSettings
    {
        public static readonly string[] Columns = { "UserId", "NotaryState", "NotaryCounty", "SigState" };

        public int Id { get; set; }

        public int UserId { get; set; }

        public string NotaryState { get; set; }

        public string NotaryCounty { get; set; }

        public string SigState { get; set; }
    }
}
