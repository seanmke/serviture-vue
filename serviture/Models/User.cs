﻿using System;

namespace Serviture.Models
{
    public class User : AuditInfo
    {
        public static readonly string[] Columns = { "CompanyId", "UserType", "IsDeleted", "IsAdmin", "CanEdit", "CreatedBy", "CreatedOn", "Password",
            "UpdatedBy", "UpdatedOn", "FirstName", "LastName", "EntityName", "Email", "UserName", "LastLoggedIn", "LastActionOn" };

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EntityName { get; set; }

        public int UserType { get; set; }

        public string Email { get; set; }

        public int CompanyId { get; set; }

        public string UserName { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? LastLoggedIn { get; set; }

        public DateTime? LastActionOn { get; set; }

        //[JsonIgnore]
        public string Password { get; set; }

        public bool IsAdmin { get; set; }

        public bool CanEdit { get; set; }

        public string Token { get; set; }
    }
}
