﻿namespace Serviture.Models
{
    public class ReasonByAffidavit
    {
        public static readonly string[] Columns = { "AffidavitId", "ReasonId" };

        public int Id { get; set; }

        public int AffidavitId { get; set; }

        public int ReasonId { get; set; }

        public ReasonByAffidavit() { }

        public ReasonByAffidavit(ReasonByAffidavitMapped reasonByAffidavitMapped)
        {
            Id = reasonByAffidavitMapped.Id;
            AffidavitId = reasonByAffidavitMapped.AffidavitId;
            ReasonId = reasonByAffidavitMapped.ReasonId;
        }
    }
}