﻿namespace Serviture.Models
{
    using System;

    public class ServeSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "CaseId", "EntityId", "ServerId", "ServeStatus", "ContractorUserId", "DueDate",
                                                    "IsDeleted" };

        public int CaseId { get; set; }

        public int? EntityId { get; set; }

        public int? ServerId { get; set; }

        public int ServeStatus { get; set; }

        public int? ContractorUserId { get; set; }

        public DateTimeOffset? DueDate { get; set; }

        public override string ToString()
        {
            return $"* SERVE_SEARCH_REQUEST - CaseId {CaseId}; EntityId {EntityId.GetValueOrDefault()}; ServeStatus {ServeStatus}; ServerId { ServerId.GetValueOrDefault()}; ContractorUserId {ContractorUserId.GetValueOrDefault()}; DueDate {DueDate.GetValueOrDefault()}; {base.ToString()}";
        }
    }
}
