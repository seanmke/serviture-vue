﻿namespace Serviture.Models
{
    public class ContractorUserSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "ContractorId", "Name", "IsDeleted" };

        public int ContractorId { get; set; }

        public string Name { get; set; }
    }
}
