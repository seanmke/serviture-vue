﻿namespace Serviture.Models
{
    public class DocumentByUserSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "UserId" };
    }
}
