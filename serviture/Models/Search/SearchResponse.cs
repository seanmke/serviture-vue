﻿namespace Serviture.Models
{
    using System.Collections.Generic;

    public class SearchResponse<T>
    {
        public IEnumerable<T> Results { get; set; }

        public bool HasMoreResults { get; set; }

        public int TotalResults { get; set; }
    }
}
