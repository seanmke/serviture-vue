﻿namespace Serviture.Models
{
    using System;

    public class AffidavitSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "CompanyId", "AffidavitType", "InvoiceNumber", "IsPaid", "ClientId", "ServeId", "ServerId", "IsDeleted", "CreatedOn" };

        public int? AffidavitType { get; set; }

        public string InvoiceNumber { get; set; }
        
        public int? ServeId { get; set; }

        public int? ServerId { get; set; }

        public DateTimeOffset? CreatedOn { get; set; }

        /// <summary>
        /// For Billing
        /// </summary>
        public bool? IsPaid { get; set; }

        public int? ClientId { get; set; }

        /// <summary>
        /// For Billing, searches for affidavits created with the last X days.
        /// We don't search on this property.
        /// </summary>
        public int? DaysBack { get; set; }

        public override string ToString()
        {
            return $"* AFFIDAVIT_SEARCH_REQUEST - AffidavitType {AffidavitType.GetValueOrDefault()}; InvoiceNumber {InvoiceNumber}; ServeId {ServeId.GetValueOrDefault()}; ServerId {ServerId.GetValueOrDefault()}; CreatedOn {CreatedOn.GetValueOrDefault()}; IsPaid {IsPaid.GetValueOrDefault()}; ClientId {ClientId.GetValueOrDefault()}; DaysBack {DaysBack.GetValueOrDefault()}; {base.ToString()}";
        }
    }
}
