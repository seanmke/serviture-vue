﻿namespace Serviture.Models
{
    public class DocumentByServeSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "ServeId" };

        public int ServeId { get; set; }
    }
}
