﻿namespace Serviture.Models
{
    public class ReasonSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "Id", "Value" };

        public string Value { get; set; }
    }
}
