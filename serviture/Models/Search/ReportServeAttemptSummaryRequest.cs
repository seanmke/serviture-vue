﻿namespace Serviture.Models
{
    using System;

    public class ReportServeAttemptSummaryRequest
    {
        public int ClientId { get; set; }

        public DateTimeOffset? StartDate { get; set; }

        public DateTimeOffset? EndDate { get; set; }

        public bool IncludeUnsuccessful { get; set; }
    }
}
