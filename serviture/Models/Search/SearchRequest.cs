﻿namespace Serviture.Models
{
    using System.Collections.Generic;

    public class SearchRequest : UserContext
    {
        public List<int> Id { get; set; }

        public int PageSize { get; set; }

        public int PageIndex { get; set; }

        public string OrderBy { get; set; }

        public bool? IsDeleted { get; set; }

        public string SearchTerm { get; set; }

        public string SearchDate { get; set; }

        public int? SearchUser { get; set; }

        public override string ToString()
        {
            var ids = Id != null ? string.Join(",", Id) : "none";
            var isDeleted = IsDeleted == null ? "null" : IsDeleted.Value.ToString();
            return $"* SEARCH_REQUEST - Id Count {ids}; PageSize {PageSize}; PageIndex {PageIndex}; OrderBy {OrderBy}; IsDeleted {isDeleted}; SearchTerm {SearchTerm}; SearchDate {SearchDate}; SearchUser {SearchUser.GetValueOrDefault()}; {base.ToString()}";
        }
    }
}
