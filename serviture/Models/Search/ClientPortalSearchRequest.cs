﻿namespace Serviture.Models
{
    public class ClientPortalSearchRequest : SearchRequest
    {
        public int ClientId { get; set; }
    }
}
