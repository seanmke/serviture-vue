﻿namespace Serviture.Models
{
    public class AttemptSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "ServeId" };

        public int ServeId { get; set; }
    }
}
