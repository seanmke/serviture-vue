﻿namespace Serviture.Models
{
    public class QuickAttemptSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "Value", "IsDeleted" };

        public string Value { get; set; }
    }
}
