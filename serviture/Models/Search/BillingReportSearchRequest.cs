﻿namespace Serviture.Models
{
    using System;

    public class BillingReportSearchRequest
    {
        public int? ClientId { get; set; }
        public int? ServerId { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string State { get; set; }
        public string County { get; set; }
    }
}
