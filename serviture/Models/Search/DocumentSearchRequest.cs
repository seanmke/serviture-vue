﻿namespace Serviture.Models
{
    public class DocumentSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "Id", "Name", "IsDeleted" };

        public string Name { get; set; }
    }
}
