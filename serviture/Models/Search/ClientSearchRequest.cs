﻿namespace Serviture.Models
{
    public class ClientSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "ClientName", "IsDeleted" };

        public string ClientName { get; set; }
    }
}
