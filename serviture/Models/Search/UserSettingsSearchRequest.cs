﻿namespace Serviture.Models
{
    public class UserSettingsSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "UserId" };
    }
}
