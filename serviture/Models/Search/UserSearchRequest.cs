﻿using System;

namespace Serviture.Models
{
    public class UserSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "FirstName", "LastName", "EntityName", "UserType", "Email", "UserName", "IsDeleted", "LastLoggedIn", "LastActionOn" };

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EntityName { get; set; }

        public int? UserType { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public DateTime LastLoggedIn { get; set; }

        public DateTime LastActionOn { get; set; }
    }
}
