﻿namespace Serviture.Models
{
    public class ReasonByAffidavitSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "AffidavitId" };

        public int AffidavitId { get; set; }
    }
}
