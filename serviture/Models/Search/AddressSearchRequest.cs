﻿namespace Serviture.Models
{
    public class AddressSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "ServeId" };

        public int ServeId { get; set; }
    }
}
