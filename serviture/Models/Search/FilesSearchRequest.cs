﻿namespace Serviture.Models
{
    public class FilesSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "ServeId" };

        public int ServeId { get; set; }
    }
}
