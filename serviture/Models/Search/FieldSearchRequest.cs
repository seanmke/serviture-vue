﻿namespace Serviture.Models
{
    public class FieldSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "Value", "FieldType", "IsDeleted", "IsArchived", "CompanyId" };

        public string Value { get; set; }

        public int? FieldType { get; set; }

        public bool? IsArchived { get; set; }
    }
}
