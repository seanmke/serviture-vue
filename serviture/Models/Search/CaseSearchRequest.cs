﻿namespace Serviture.Models
{
    using System;

    public class CaseSearchRequest : SearchRequest
    {
        public static readonly string[] Columns = { "ClientId", "ContactId", "CaseNameId", "DefendantId", "County", "OtherLocationId", 
                                                    "CaseNumId", "FileNumId", "CourtDate", "State", "IsDeleted", "CreatedOn" };

        public int ClientId { get; set; }

        public int? ContactId { get; set; }

        public int? CaseNameId { get; set; }

        public int? DefendantId { get; set; }

        public string County { get; set; }

        public string State { get; set; }

        public int? OtherLocationId { get; set; }

        public int? CaseNumId { get; set; }

        public int? FileNumId { get; set; }

        public DateTimeOffset? CourtDate { get; set; }

        public DateTimeOffset CreatedOn { get; set; }

        public override string ToString()
        {
            return $"* CASE_SEARCH_REQUEST - ClientId {ClientId}; ContactId {ContactId.GetValueOrDefault()}; CaseNameId {CaseNameId.GetValueOrDefault()}; DefendantId {DefendantId.GetValueOrDefault()}; County {County}; State {State}; OtherLocationId {OtherLocationId.GetValueOrDefault()}; CaseNumId {CaseNumId.GetValueOrDefault()}; FileNumId {FileNumId.GetValueOrDefault()}; CourtDate {CourtDate.GetValueOrDefault()}; CreatedOn {CreatedOn}; {base.ToString()}";
        }
    }
}
