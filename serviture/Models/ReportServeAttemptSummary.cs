﻿namespace Serviture.Models
{
    using System;

    public class ReportServeAttemptSummary
    {
        public string FileNumber { get; set; }

        public DateTime DateServed { get; set; }

        public string ServerName { get; set; }

        public string Gps { get; set; }
    }
}
