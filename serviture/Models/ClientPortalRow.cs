﻿namespace Serviture.Models
{
    using System;

    public class ClientPortalRow
    {
        public string Status { get; set; }

        public string AffidavitType { get; set; }

        public string ClientFileNumber { get; set; }

        public string CaseNumber { get; set; }

        public string Defendant { get; set; }

        public string EntityServed { get; set; }

        public int AttemptCount { get; set; }

        public DateTime? LatestAttempt { get; set; }

        public int FileCount { get; set; }

        public string InvoiceNumber { get; set; }

        public string Server { get; set; }

        public DateTime? ServeCompleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public int CaseId { get; set; }

        public int ServeId { get; set; }

        public int? AffidavitId { get; set; }
    }
}
