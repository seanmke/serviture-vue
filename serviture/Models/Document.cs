﻿namespace Serviture.Models
{
    using System;

    [Serializable]
    public class Document
    {
        public static readonly string[] Columns = { "Name", "HasText", "IsDeleted", "SortOrder", "TextBefore" };

        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public bool HasText { get; set; }

        public int SortOrder { get; set; }

        public bool TextBefore { get; set; }
    }
}
