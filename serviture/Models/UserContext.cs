﻿namespace Serviture.Models
{
    public class UserContext
    {
        public int? UserId { get; set; }

        public bool? IsAdmin { get; set; }

        public bool? IsSuperAdmin { get; set; }

        public int? CompanyId { get; set; }

        public override string ToString()
        {
            return $"*USER CONTEXT - UserId {UserId.GetValueOrDefault()}; IsAdmin {IsAdmin.GetValueOrDefault()}; IsSuperAdmin {IsSuperAdmin.GetValueOrDefault()}; CompanyId {CompanyId.GetValueOrDefault()} *";
        }
    }
}