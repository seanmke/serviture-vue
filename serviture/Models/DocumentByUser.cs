﻿namespace Serviture.Models
{
    public class DocumentByUser
    {
        public static readonly string[] Columns = { "DocumentId", "UserId" };

        public int Id { get; set; }

        public int DocumentId { get; set; }

        public int UserId { get; set; }
    }
}
