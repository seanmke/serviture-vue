﻿namespace Serviture.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class AttemptReport
    {
        public List<AttemptMapped> Attempts { get; set; }

        public List<FileUpload> Files { get; set; }
    }
}
