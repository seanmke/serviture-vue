﻿namespace Serviture.Models
{
    using System;

    public class Case : AuditInfo
    {
        public static readonly string[] Columns = { "ClientId", "ContactId", "CaseNameId", "DefendantId", "County", "OtherLocationId", "CaseNumId", "FileNumId", "CourtDate", "State",
                                                    "IsDeleted", "IsArchived", "CreatedBy", "CreatedOn",  "UpdatedBy", "UpdatedOn" };

        public int Id { get; set; }

        public int? ClientId { get; set; }

        public int? ContactId { get; set; }

        public int? CaseNameId { get; set; }

        public int? DefendantId { get; set; }

        public string County { get; set; }

        public string State { get; set; }

        public int? OtherLocationId { get; set; }

        public int? CaseNumId { get; set; }

        public int? FileNumId { get; set; }

        public DateTime? CourtDate { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsArchived { get; set; }
    }
}