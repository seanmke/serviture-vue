﻿namespace Serviture.Models
{
    using System;

    public class Attempt
    {
        public static readonly string[] Columns = { "DateServed", "OtherInfo", "IsSuccess", "ServeId" };

        public int Id { get; set; }

        public DateTime? DateServed { get; set; }

        public string OtherInfo { get; set; }

        public bool IsSuccess { get; set; }

        public int ServeId { get; set; }

        public Attempt() { }

        public Attempt(AttemptMapped attemptMapped)
        {
            if (!string.IsNullOrWhiteSpace(attemptMapped.DateServed))
            {
                if(DateTime.TryParse(attemptMapped.DateServed, out var mappedDateServed))
                {
                    DateServed = mappedDateServed;
                }
            }

            if(attemptMapped.OtherInfo != null)
            {
                OtherInfo = attemptMapped.OtherInfo.Value;
            }

            Id = attemptMapped.Id;
            IsSuccess = attemptMapped.IsSuccess;
            ServeId = attemptMapped.ServeId;            
        }
    }
}