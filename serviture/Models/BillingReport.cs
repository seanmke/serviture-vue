﻿namespace Serviture.Models
{
    public class BillingReport
    {
        public int PersonalCount { get; set; }
        public int SubstituteCount { get; set; }
        public int ReasonableCount { get; set; }
        public int CorporationCount { get; set; }
        public int OtherCount { get; set; }
        public decimal TotalCharge { get; set; }
        public int TotalServes { get; set; }
    }
}
