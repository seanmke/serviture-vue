﻿namespace Serviture.Models
{
    public class QuickAttempt
    {
        public static readonly string[] Columns = { "Value", "SortOrder", "IsDeleted" };

        public int Id { get; set; }

        public string Value { get; set; }

        public bool IsDeleted { get; set; }
        
        public int SortOrder { get; set; }
    }
}