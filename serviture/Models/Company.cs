﻿namespace Serviture.Models
{
    public class Company
    {
        public static readonly string[] Columns = { "IsDeleted", "Name" };

        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public string Name { get; set; }
    }
}
