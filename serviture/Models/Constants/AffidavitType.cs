﻿namespace Serviture.Models
{
    public enum AffidavitType
    {
        Personal,
        Substitute,
        Reasonable,
        Company,
        Mailing,
        Invoice
    }
}
