﻿namespace Serviture.Models
{
    public static class DataConstants
    {
        public static string Address = "address";
        public static string Affidavit = "affidavit";
        public static string Attempt = "attempt";
        public static string Case = "[case]";
        public static string Client = "client";
        public static string Company = "company";
        public static string ContractorUser = "contractoruser";
        public static string Document = "document";
        public static string DocumentByServe = "documentbyserve";
        public static string DocumentByUser = "documentbyuser";
        public static string Field = "field";
        public static string File = "[file]";
        public const string FileBlob = "fileblob";
        public static string QuickAttempt = "quickattempt";
        public static string Reason = "reason";
        public static string ReasonByAffidavit = "reasonbyaffidavit";
        public static string Serve = "serve";
        public static string User = "users";
        public static string UserSettings = "usersettings";

        public static string OrderByDefault = "Id";
        public static string OrderBySortOrder = "SortOrder";
    }
}
