﻿namespace Serviture.Models
{
    public enum FieldType
    {
        Client,
        Contact,
        County,
        CaseName,
        CaseNumber,
        Address,
        Relationship,
        OtherLocation,
        AttemptOther,
        PersonServed,
        DocOther,
        ClientFileNumber,
        ServiceProvided
    }
}
