﻿namespace Serviture.Models
{
    public enum ServeStatus
    {
        Unknown,
        InProcess,
        Complete
    }
}
