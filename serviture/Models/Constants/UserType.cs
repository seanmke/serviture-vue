﻿namespace Serviture.Models
{
    public enum UserType
    {
        Unknown,
        Serviture,
        Client,
        Contractor
    }
}
