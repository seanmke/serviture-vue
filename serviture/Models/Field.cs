﻿namespace Serviture.Models
{
    using System;

    public class Field
    {
        public static readonly string[] Columns = { "Value", "FieldType", "IsDeleted", "IsArchived", "CompanyId" };

        public int Id { get; set; }

        public string Value { get; set; }

        public int FieldType { get; set; }

        public bool IsDeleted { get; set; }

        public int CompanyId { get; set; }

        public bool IsArchived { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Field field &&
                   Value == field.Value &&
                   FieldType == field.FieldType &&
                   IsDeleted == field.IsDeleted &&
                   CompanyId == field.CompanyId &&
                   IsArchived == field.IsArchived;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Value, FieldType, IsDeleted, CompanyId, IsArchived);
        }
    }
}
