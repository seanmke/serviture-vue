﻿namespace Serviture.Models
{
    public class DocumentByServe
    {
        public static readonly string[] Columns = { "ServeId", "DocumentId", "DocumentText" };

        public int Id { get; set; }

        public int ServeId { get; set; }

        public int DocumentId { get; set; }

        public string DocumentText { get; set; }

        public DocumentByServe() { }

        public DocumentByServe(DocumentByServeMapped documentByServeMapped)
        {
            ServeId = documentByServeMapped.ServeId;
            DocumentId = documentByServeMapped.DocumentId;

            if(documentByServeMapped.DocumentText != null)
            {
                DocumentText = documentByServeMapped.DocumentText.Value;
            }
        }
    }
}