﻿namespace Serviture.Models
{
    using System;

    public class Serve : AuditInfo
    {
        public static readonly string[] Columns = { "CaseId", "EntityId", "ServerId", "ServeStatus", "Charge",
                                                    "NotaryCounty", "NotaryState", "SigState", "Notes", "ContractorUserId", "DueDate",
                                                    "IsDeleted", "CreatedBy", "CreatedOn",  "UpdatedBy", "UpdatedOn" };

        public int Id { get; set; }

        public int CaseId { get; set; }

        public int? EntityId { get; set; }

        public int? ServerId { get; set; }

        public int ServeStatus { get; set; }

        public bool IsDeleted { get; set; }

        public decimal? Charge { get; set; }

        public string NotaryCounty { get; set; }

        public string NotaryState { get; set; }

        public string SigState { get; set; }

        public string Notes { get; set; }

        public int? ContractorUserId { get; set; }

        public DateTime? DueDate { get; set; }

        public Serve() { }

        public Serve(CaseServeMapped caseServeMapped)
        {
            if(caseServeMapped.Server != null)
            {
                ServerId = caseServeMapped.Server.Id;
            }

            DueDate = string.IsNullOrEmpty(caseServeMapped.DueDate) ? null : (DateTime?)DateTime.Parse(caseServeMapped.DueDate);
            Notes = caseServeMapped.Notes;
            ServeStatus = caseServeMapped.ServeStatus;
            Charge = caseServeMapped.Charge;
            CreatedBy = caseServeMapped.CreatedBy;
            CreatedOn = caseServeMapped.CreatedOn;
        }

        public Serve(ServeMapped serveMapped)
        {
            if (serveMapped.ProcessServer != null)
            {
                ServerId = serveMapped.ProcessServer.Id;
            }

            if(serveMapped.ContractorUser != null)
            {
                ContractorUserId = serveMapped.ContractorUser.Id;
            }

            ServeStatus = serveMapped.ServeStatus;
            CaseId = serveMapped.CaseId;
            Charge = serveMapped.Charge;
            NotaryCounty = serveMapped.NotaryCounty;
            NotaryState = serveMapped.NotaryState;
            SigState = serveMapped.SigState;
            Notes = serveMapped.Notes;
            DueDate = string.IsNullOrEmpty(serveMapped.DueDate) ? null : (DateTime?)DateTime.Parse(serveMapped.DueDate);

            CreatedBy = serveMapped.CreatedBy;
            CreatedOn = serveMapped.CreatedOn;
            UpdatedBy = serveMapped.UpdatedBy;
            UpdatedOn = serveMapped.UpdatedOn;
        }
    }
}