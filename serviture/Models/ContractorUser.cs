﻿namespace Serviture.Models
{
    public class ContractorUser
    {
        public static readonly string[] Columns = { "ContractorId", "IsDeleted", "Name" };

        public int Id { get; set; }

        public int ContractorId { get; set; }

        public bool IsDeleted { get; set; }

        public string Name { get; set; }
    }
}
