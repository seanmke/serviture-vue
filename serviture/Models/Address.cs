﻿namespace Serviture.Models
{
    public class Address
    {
        public static readonly string[] Columns = { "ServeId", "StreetAddress" };

        public int Id { get; set; }

        public int ServeId { get; set; }

        public string StreetAddress { get; set; }
    }
}