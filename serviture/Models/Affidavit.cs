﻿namespace Serviture.Models
{
    using System;

    public class Affidavit : AuditInfo
    {
        public static readonly string[] Columns = { "InvoiceNumber", "CompanyId", "AffidavitType", "ServerId", "IsDeleted", "IsArchived", "IsPaid", "IsNotMilitary", "IsNotMarried",
                                                    "ServeId", "EntityId", "CareOfId", "RelationshipId", "ContractorUserId",
                                                    "ClientId", "ContactId", "CaseNameId", "DefendantId", "OtherLocationId",  "CaseNumId", "FileNumId", "Charge", "CourtDate", 
                                                    "State", "County", "NotaryState", "NotaryCounty", "SigState",
                                                    "CreatedBy", "CreatedOn",  "UpdatedBy", "UpdatedOn" };

        public int Id { get; set; }

        public string InvoiceNumber { get; set; }

        public int CompanyId { get; set; }

        public int AffidavitType { get; set; }

        public int ServerId { get; set; }

        public bool IsDeleted { get; set; }       

        public bool IsArchived { get; set; }
        
        /// <summary>
        /// For Billing
        /// </summary>
        public bool IsPaid { get; set; }

        public bool IsNotMilitary { get; set; }

        public bool IsNotMarried { get; set; }

        /// <summary>
        /// The ServeId on an Affidavit is used to 
        /// get the Addresses/Attempts/Documents
        /// </summary>
        public int? ServeId { get; set; }

        public int? EntityId { get; set; }

        /// <summary>
        /// Can map differently depending on the Affidavit Type.
        /// Substitute Service -> Substitute Service Upon Name
        /// Company/Corporation -> Served Person
        /// </summary>
        public int? CareOfId { get; set; }

        public int? RelationshipId { get; set; }

        public int? ContractorUserId { get; set; }

        public int? ClientId { get; set; }

        public int? ContactId { get; set; }

        public int? CaseNameId { get; set; }

        public int? DefendantId { get; set; }

        public int? OtherLocationId { get; set; }

        public int? CaseNumId { get; set; }

        public int? FileNumId { get; set; }

        public decimal? Charge { get; set; }

        public DateTime? CourtDate { get; set; }

        public string State { get; set; }

        public string County { get; set; }

        public string NotaryState { get; set; }

        public string NotaryCounty { get; set; }

        public string SigState { get; set; }

        public Affidavit() { }

        public Affidavit(AffidavitMapped affidavitMapped)
        {
            Id = affidavitMapped.Id;
            ServeId = affidavitMapped.ServeId;
            InvoiceNumber = affidavitMapped.InvoiceNumber;
            CompanyId = affidavitMapped.CompanyId;
            AffidavitType = (int)affidavitMapped.AffidavitType;
            IsDeleted = affidavitMapped.IsDeleted;
            IsArchived = affidavitMapped.IsArchived;
            IsPaid = affidavitMapped.IsPaid;
            IsNotMarried = affidavitMapped.IsNotMarried;
            IsNotMilitary = affidavitMapped.IsNotMilitary;
            CreatedOn = affidavitMapped.CreatedOn;
            CreatedBy = affidavitMapped.CreatedBy;

            Charge = affidavitMapped.Charge;
            CourtDate = string.IsNullOrEmpty(affidavitMapped.CourtDate) ? null : (DateTime?)DateTime.Parse(affidavitMapped.CourtDate);
            State = affidavitMapped.State;
            County = affidavitMapped.County;
            NotaryState = affidavitMapped.NotaryState;
            NotaryCounty = affidavitMapped.NotaryCounty;
            SigState = affidavitMapped.SigState;


            if (affidavitMapped.Server != null)
            {
                ServerId = affidavitMapped.Server.Id;
            }

            if(affidavitMapped.ContractorUser != null)
            {
                ContractorUserId = affidavitMapped.ContractorUser.Id;
            }
        }
    }
}