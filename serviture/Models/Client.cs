﻿namespace Serviture.Models
{
    using System;

    public class Client
    {
        public static readonly string[] Columns = { "CompanyId", "ClientName", "DefaultFee", "IsDeleted", "ShowUnsuccessful" };

        public int Id { get; set; }

        public int CompanyId { get; set; }

        public string ClientName { get; set; }

        public decimal? DefaultFee { get; set; }

        public bool? IsDeleted { get; set; }

        public bool? ShowUnsuccessful { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Client client &&
                   CompanyId == client.CompanyId &&
                   ClientName == client.ClientName &&
                   DefaultFee == client.DefaultFee &&
                   IsDeleted == client.IsDeleted &&
                   ShowUnsuccessful == client.ShowUnsuccessful;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CompanyId, ClientName, DefaultFee, IsDeleted, ShowUnsuccessful);
        }
    }
}