﻿namespace Serviture.Services
{
    using Azure.Storage.Blobs.Models;
    using System.IO;
    using System.Threading.Tasks;

    public interface IBlobService
    {
        Task<byte[]> Get(string fileName);

        Task<BlobContentInfo> Upload(string fileName, Stream stream);
    }
}