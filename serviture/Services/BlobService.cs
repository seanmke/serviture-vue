﻿namespace Serviture.Services
{
    using Azure.Storage.Blobs;
    using Azure.Storage.Blobs.Models;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public class BlobService : IBlobService
    {
        private IConfiguration Configuration { get; set; }

        public BlobService(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task<byte[]> Get(string fileName)
        {
            try
            {
                var blobServiceClient = new BlobServiceClient(Configuration["ConnectionStrings:AzureStorage"]);
                var container = blobServiceClient.GetBlobContainerClient("serve-attempts");
                var blobClient = container.GetBlobClient(fileName);
                var blobResponse = await blobClient.DownloadStreamingAsync();
                var blobStream = blobResponse.Value.Content;

                using (var ms = new MemoryStream())
                {
                    blobStream.CopyTo(ms);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<BlobContentInfo> Upload(string fileName, Stream stream)
        {
            var blobServiceClient = new BlobServiceClient(Configuration["ConnectionStrings:AzureStorage"]);
            var container = blobServiceClient.GetBlobContainerClient("serve-attempts");
            var response = await container.UploadBlobAsync(fileName, stream);
            return response.Value;
        }
    }
}
